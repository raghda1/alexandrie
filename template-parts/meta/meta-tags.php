<?php

if ( !empty( get_the_tag_list() ) ) {
    echo $tags = sprintf(
    '<span class="tags">' .
    esc_html_x( 'Tags %s', 'post Tags', 'alexandrie' ),
    get_the_tag_list( '', esc_html__( ', ', 'alexandrie' ) ) . '</span>'
    );
}
?>