<?php

if ( !post_password_required() && ( comments_open() || get_comments_number() ) ) {
    echo '<span class="comments-link">';
    comments_popup_link( esc_html__( '0 Comment', 'alexandrie' ) ,
               esc_html__( '1 Comment', 'alexandrie' ), esc_html__( '% Comments', 'alexandrie' ) 
    );
    echo '</span>';
}
?>