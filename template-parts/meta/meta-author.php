<?php
$show_avatar=get_theme_mod('blog_archive_show_athour_avatar','1');
if($show_avatar){
echo $byline = sprintf(
 '<span class="author vcard post-author">' . get_avatar( get_the_author_meta( 'ID' ) , 32,'','',array('class'=>'rounded-circle') ) .
 esc_html_x( ' %s', 'post author', 'alexandrie' ), '<a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
);
}else{
    echo $byline = sprintf(
 '<span class="author vcard post-author">' . esc_html_x( 'By %s', 'post author', 'alexandrie' ), '<a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
);
}
?>