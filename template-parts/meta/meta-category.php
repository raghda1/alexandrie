<?php

if ( !empty( get_the_category_list() ) ) {
    echo $cate = sprintf(
    '<span class="category">' .
    esc_html_x( ' %s', 'post Category', 'alexandrie' ),
    get_the_category_list( esc_html__( ', ', 'alexandrie' ) ) . '</span>'
    );
}
?>