<?php 
global $disable_featured_image;
if($disable_featured_image == 0):
if ( has_post_thumbnail() ) : ?>
<div class="post-image <?php echo esc_attr(get_theme_mod('blog_single_thumb_shadow','none')); ?>"><?php the_post_thumbnail('',['class'=>'img-fluid single-post-img']); ?></div>
<?php 
endif; 
endif; 
?>