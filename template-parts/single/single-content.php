<?php if ( !empty( $post->post_content ) ) : ?>
    <div class="entry-content post-content py-5 my-3 ">
        <?php
        the_content();
        if ( get_theme_mod( 'single_posts_show_tags_after_content', '1' ) ) {
            if ( !empty( get_the_tag_list() ) ) {
                echo $tags = sprintf(
                '<div class="tags mt-5"><i class="ti-tag"></i>' .
                esc_html_x( ' %s', 'post Tags', 'alexandrie' ),
                get_the_tag_list( '', esc_html__( ', ', 'alexandrie' ) ) . '</div>'
                );
            }
        }


        wp_link_pages( array(
            'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'alexandrie' ),
            'after' => '</div>',
        ) );
        ?>
    </div>
<?php endif; ?>