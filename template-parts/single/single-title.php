<?php 
global $disable_title;
if($disable_title == 0):
if( ! empty( $post->post_title ) ) : ?>
<h2 class="entry-title post-title "><?php the_title(); ?></h2>
<?php 
endif; 
endif; 
?>
<?php do_action( 'alexandrie_after_content_title' ); ?>