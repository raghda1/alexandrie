

<?php
if ( is_sticky() ) {
    ?>
    <div class="featured">
        <span class="featured-label"><?php _e('featured','alexandrie'); ?></span>
    </div>
<?php }
?>
<?php if ( true == get_theme_mod( 'blog_archive_show_cat_before_title', true ) ) : ?>
    <div class="post-category">
        <?php echo get_the_category_list( esc_html__( ', ', 'alexandrie' ) ); ?>
    </div>
<?php endif; ?>
<?php if ( true == get_theme_mod( 'blog_archive_title_linkable', true ) ) : ?>
    <h3 class="entry-title post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
<?php else : ?>
    <h3 class="entry-title post-title"><?php the_title(); ?></h3>
<?php endif; ?>
<?php do_action( 'alexandrie_after_content_title' ); ?>