<?php if ( true == get_theme_mod( 'blog_archive_thumbnail_linkable', true ) ) : ?>
    <div class="post-image"><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('post-thumbnail', ['class' => 'img-fluid' ] ); ?></a></div>
<?php else : ?>
    <div class="post-image"><?php the_post_thumbnail('post-thumbnail', ['class' => 'img-fluid' ] ); ?></div>
<?php endif; ?>