<?php if ( true == get_theme_mod( 'search_archive_title_linkable', true ) ) : ?>
    <h3 class="entry-title post-title pt-3 pl-3"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
<?php else : ?>
    <h3 class="entry-title post-title pt-3 pl-3"><?php the_title(); ?></h3>
<?php endif; ?>
<?php do_action( 'alexandrie_after_content_title' ); ?>