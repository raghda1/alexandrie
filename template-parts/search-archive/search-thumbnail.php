

<?php if ( true == get_theme_mod( 'search_archive_thumbnail_linkable', true ) ) : ?>
    <div class="post-image"><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('post-thumbnail', ['class' => 'img-fluid' ] ); ?></a></div>
<?php else : ?>
    <div class="post-image"><?php the_post_thumbnail('large', ['class' => 'img-fluid rounded-top' ] ); ?></div>
<?php endif; ?>
        