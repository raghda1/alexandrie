// bundle.config.js 

module.exports = {
    bundle: {
        'plugins': {
            scripts: [
//                'src/js/lib/**/*.js',
//                'node_modules/jquery/dist/jquery.min.js',
                'node_modules/popper.js/dist/umd/popper.min.js',
                'node_modules/bootstrap/dist/js/bootstrap.min.js',
                'assets/src/js/plugins/masonry.pkgd.min.js',
                'assets/src/js/plugins/*.js',

            ],
            options: {
                order: {
                    scripts: [
//                        'node_modules/jquery/dist/jquery.min.js',
                        'node_modules/popper.js/dist/umd/popper.min.js',
                        'node_modules/bootstrap/dist/js/bootstrap.min.js',
                         'assets/src/js/plugins/masonry.pkgd.min.js', 
                         'assets/src/js/plugins/*.js',
//                        'assets/src/js/plugins/*.js'
//                        'src/js/plugins/modernizr.custom.js',
//                        'src/js/plugins/classie.js',
//                        'src/js/plugins/search.js'
                    ]
                },
                rev: false,
                uglify: false,
                maps: false
            }
        },
        'main': {
            scripts: [
                'assets/src/js/main.js'
            ],
            options: {
                uglify: false,
                rev: false,
                maps: false
            }
        },
       'blocks': {
           scripts: [
               'assets/src/js/gutenberg/*.js'
           ],
           options: {
               uglify: false,
               rev: false,
               maps: false
           }
       },
//        'contact': {
//            scripts: [
//                'src/js/contact.js'
//            ],
//            options: {
//                uglify: false,
//                rev: false,
//                maps: false
//            }
//        },
//        'rsvp': {
//            scripts: [
//                'src/js/rsvp.js'
//            ],
//            options: {
//                uglify: false,
//                rev: false,
//                maps: false
//            }
//        }

    }
};
