<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package alexandrie
 */
?>
</div><!-- #content -->
</div><!-- #content -->


<footer id="colophon" class="site-footer" role="contentinfo">

    <?php
    global $disable_footer;
    if ( $disable_footer == 0 ):
        do_action( 'alexandrie_before_footer' );
        $footercol = get_theme_mod( 'footer_layout' );
        if ( $footercol !== "footer-no" ):
            ?>
            <div class="footer-area">
                <div class="<?php echo alexandrie_footer_layout(); ?>">
                    <div class="row">
                        <?php
                        switch ( $footercol ) {
                            case "footer-one":
                                ?>
                                <div class="swp-footer-1 col-md-12 col-xs-12"><?php dynamic_sidebar( 'footer-1' ); ?></div>
                                <?php
                                break;
                            case "footer-two":
                                ?>
                                <div class="swp-footer-1 col-md-6 col-sm-12"><?php dynamic_sidebar( 'footer-1' ); ?></div>
                                <div class="swp-footer-2 col-md-6 col-sm-12"><?php dynamic_sidebar( 'footer-2' ); ?></div>
                                <?php
                                break;
                            case "footer-three":
                                ?>
                                <div class="swp-footer-1 col-md-4 col-sm-12"><?php dynamic_sidebar( 'footer-1' ); ?></div>
                                <div class="swp-footer-2 col-md-4 col-sm-12"><?php dynamic_sidebar( 'footer-2' ); ?></div>
                                <div class="swp-footer-3 col-md-4 col-sm-12"><?php dynamic_sidebar( 'footer-3' ); ?></div>
                                <?php
                                break;
                            case "footer-four":
                                ?>
                                <div class="swp-footer-1 col-md-3 col-sm-12"><?php dynamic_sidebar( 'footer-1' ); ?></div>
                                <div class="swp-footer-2 col-md-3 col-sm-12"><?php dynamic_sidebar( 'footer-2' ); ?></div>
                                <div class="swp-footer-3 col-md-3 col-sm-12"><?php dynamic_sidebar( 'footer-3' ); ?></div>
                                <div class="swp-footer-4 col-md-3 col-sm-12"><?php dynamic_sidebar( 'footer-4' ); ?></div>
                                <?php
                                break;
                            default:
                        }
                        ?>
                    </div>
                </div>
            </div>
            <?php
        endif;
    endif;
    ?>
    <?php if ( get_theme_mod( 'show_footer_social_menu', '1' ) ) { ?>
        <div class=" py-2 footer-social-menu-wrapper">
            <?php
            wp_nav_menu( $args = array(
                'menu_class' => "nav justify-content-center footer-social-menu", // (string) CSS class to use for the ul element which forms the menu. Default 'menu'.
                'depth' => "1", // (int) How many levels of the hierarchy are to be included. 0 means all. Default 0.
                'theme_location' => "footer-social", // (string) Theme location to be used. Must be registered with register_nav_menu() in order to be selectable by the user.
//            'fallback_cb' => false,
            ) );
            ?>


        </div>
    <?php } ?>

    
    <?php
    global $disable_copyright;
    if ( $disable_copyright == 0 ):
        ?>
        <?php do_action( 'alexandrie_after_footer_widgets' ); ?>
        <div class="site-info">
            <div class=" <?php echo esc_attr( alexandrie_copyright_layout() ); ?>">
                <div class="row">
                    <div class="col-md-12">
                        <div class="site-info-content">
                            <div class="site-info-text">
                                <?php echo do_shortcode(get_theme_mod( 'above_copyright', 'Alexandrie' )); ?>
                            </div>
                            <?php
                            if ( get_theme_mod( 'show_footer_sitemap_menu', 'true' ) ) {
                                wp_nav_menu( $args = array(
                                    'menu_class' => "nav justify-content-center footer-menu", // (string) CSS class to use for the ul element which forms the menu. Default 'menu'.
                                    'depth' => "1", // (int) How many levels of the hierarchy are to be included. 0 means all. Default 0.
                                    'theme_location' => "footer-menu", // (string) Theme location to be used. Must be registered with register_nav_menu() in order to be selectable by the user.
                                    'fallback_cb' => false,
                                ) );
                            }
                            ?>
                        </div>

                    </div>
                </div>
            </div>
            <div class="copyright-text">
                <div class=" <?php echo esc_attr( alexandrie_copyright_layout() ); ?>">
                    <div class="row">
                        <div class="col-md-12">
                            <small> <?php echo get_theme_mod( 'footer_copyright', '© My blog. 2018' ); ?></small>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .site-info -->
        <?php do_action( 'alexandrie_after_footer' ); ?>	
    <?php endif; ?>

</footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>
</body>
</html>
