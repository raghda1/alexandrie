<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package alexandrie
 */
get_header();
$col_class = array(
    '1' => 'col-md-12',
    '2' => 'col-md-6',
    '3' => 'col-md-4',
    '4' => 'col-md-3',
);
?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <?php do_action( 'alexandrie_inside_content_container_before' ); ?><!-- Content Container Before Hook -->
        <?php if ( have_posts() ) : ?>
        <div class="row grid">
            <div class="grid-sizer col-xs-1"></div>
            <?php /* Start the Loop */ ?>
            <?php while ( have_posts() ) : the_post(); ?>

                <?php if ( get_post_type() == 'post' ) : 
                    $shadow_class=get_theme_mod( 'blog_archive_post_shadow','shadow-sm');
                    $col_num=get_theme_mod( 'blog_posts_col_num','1');
                ?><article class="grid-item <?php echo esc_attr($col_class[$col_num]);?>">
                    <div id="post-<?php the_ID(); ?>" <?php post_class( ['alexandrie_blog blog_and_archive',$shadow_class] ); ?>>
                        <?php do_action( 'alexandrie_before_content' ); ?>
                        <div class="post-container">
                            <?php
                            $blog_template_parts = get_theme_mod( 'blog_posts_layout', array( 'thumbnail', 'title', 'meta', 'content' ) );

                            if ( !empty( $blog_template_parts ) && is_array( $blog_template_parts ) ) {
                                foreach ( $blog_template_parts as $blog_part ) {
                                    get_template_part( 'template-parts/blog-archive/blog-' . $blog_part );
                                }
                            }
                            ?>
                        </div>
                        <?php do_action( 'alexandrie_after_content' ); ?>

                    </div>
                </article>
                <?php endif; ?>

                <?php if ( get_post_type() != 'post' ) : ?>
                    <?php get_template_part( 'template-parts/content', get_post_format() ); ?>
                <?php endif; ?>

            <?php endwhile; ?>
        </div>
        <div class="row">
            <div class="col-md-12">
            <?php alexandrie_the_posts_navigation(); ?>
            </div>
        </div>
        <?php else : ?>
            <div class="row">
                <div class="col-md-12">
            <?php get_template_part( 'template-parts/content', 'none' ); ?>
                </div>
            </div>
        <?php endif; ?>
        <?php do_action( 'alexandrie_inside_content_container_after' ); ?><!-- Content Container After Hook -->
    </main><!-- #main -->
</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
