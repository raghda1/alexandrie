<?php
/**
 * author.php
 */
get_header();
$col_class = array(
    '1' => 'col-md-12',
    '2' => 'col-md-6',
    '3' => 'col-md-4',
    '4' => 'col-md-3',
);
?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <?php do_action( 'alexandrie_inside_content_container_before' ); ?><!-- Content Container Before Hook -->
        <?php
        $curauth = (isset( $_GET[ 'author_name' ] )) ? get_user_by( 'slug', $author_name ) : get_userdata( intval( $author ) );

//        var_dump( $curauth );
        ?>

        <div class="row">
            <div class="col-md-12">
                <div class="author-card bg-gray-100">
                    <div class="row">
                        <div class="col-md-6 mr-auto ml-auto">
                            <?php echo get_avatar( $curauth->ID, 200, '', '', array( 'class' => 'rounded-circle img-fluid author-img' ) ); ?>
                            <!--<img class="rounded-circle img-fluid author-img" src="../images/alexander-krivitskiy-768344-unsplash.jpg">-->
                        </div>
                        <!--<div class="col-md-6 mr-auto ml-auto"><img class="rounded-circle mr-auto ml-auto d-block shadow-sm" src="https://dummyimage.com/300x300/d4d4d4/fff"></div>-->
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center mt-5">    
                            <h2><?php echo $curauth->nickname; ?></h2>
                            <p><?php echo $curauth->user_description; ?></p>
                            <p><a href="<?php echo $curauth->user_url; ?>"><?php echo $curauth->user_url; ?></a></p>
                            <?php
                            if ( function_exists( 'alexandrie_user_social_links_sc' ) ) {
                                echo do_shortcode( '[alexandrie_author_social id=' . $curauth->ID . ']' );
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <h3 class=" my-4"><?php echo __( 'Posts by ', 'alexandrie' ) . $curauth->nickname; ?>:</h3>


        <!-- The Loop -->
        <div class="row grid h-auto">
            <div class="grid-sizer col-xs-1"></div>
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                    <?php
                    if ( get_post_type() == 'post' ) :
                        $shadow_class = get_theme_mod( 'blog_archive_post_shadow', 'shadow-sm' );
                        $col_num = get_theme_mod( 'blog_posts_col_num', '1' );
                        ?><article class=" <?php echo esc_attr( $col_class[ $col_num ] ); ?>">
                            <div id="post-<?php the_ID(); ?>" <?php post_class( [ 'alexandrie_blog blog_and_archive', $shadow_class ] ); ?>>
                                    <?php do_action( 'alexandrie_before_content' ); ?>
                                <div class="post-container">
                                    <?php
                                    $blog_template_parts = get_theme_mod( 'blog_posts_layout', array( 'thumbnail', 'title', 'meta', 'content' ) );

                                    if ( !empty( $blog_template_parts ) && is_array( $blog_template_parts ) ) {
                                        foreach ( $blog_template_parts as $blog_part ) {
                                            get_template_part( 'template-parts/blog-archive/blog-' . $blog_part );
                                        }
                                    }
                                    ?>
                                </div>
            <?php do_action( 'alexandrie_after_content' ); ?>

                            </div>
                        </article>
        <?php endif; ?>



                    <?php
                endwhile;
            else:
                ?>
                <p><?php _e( 'No posts by this author.', 'alexandrie' ); ?></p>

<?php endif; ?>
        </div>
        <!-- End Loop -->
        <div class="clearfix"></div>

<?php do_action( 'alexandrie_inside_content_container_after' ); ?><!-- Content Container After Hook -->
    </main>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>