<?php

/**
 * alexandrie functions and definitions
 *
 * @package alexandrie
 */
/**
 * Assign the alexandrie version to a var
 */
$theme = wp_get_theme( 'alexandrie' );
$alexandriewp_version = $theme[ 'Version' ];


/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( !isset( $content_width ) ) {
    $content_width = 640; /* pixels */
}

if ( !function_exists( 'alexandrie_setup' ) ) :

    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function alexandrie_setup() {

        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on start, use a find and replace
         * to change 'alexandrie' to the name of your theme in all the template files
         */
        load_theme_textdomain( 'alexandrie', get_template_directory() . '/languages' );


        // Add default posts and comments RSS feed links to head.
        add_theme_support( 'automatic-feed-links' );

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */

        add_theme_support( 'title-tag' );

        // Site Logo
        add_theme_support( 'custom-logo', array(
            'flex-height' => true,
            'flex-width' => true,
            'header-text' => array( 'site-title', 'site-description' ),
        ) );
        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
         */
        add_theme_support( 'post-thumbnails' );

//        add_theme_support( 'post-formats', array( 'link', 'image', 'gallery', 'video', 'audio' ) );
        // This theme uses wp_nav_menu() in one location.
        register_nav_menus( array(
            'primary' => esc_html__( 'Primary Menu', 'alexandrie' ),
        ) );
        register_nav_menus( array(
            'header-social' => esc_html__( 'Header Social Menu', 'alexandrie' ),
        ) );
        register_nav_menus( array(
            'footer-social' => esc_html__( 'Footer Social Menu', 'alexandrie' ),
        ) );
        register_nav_menus( array(
            'footer-menu' => esc_html__( 'Footer Menu', 'alexandrie' ),
        ) );


        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support( 'html5', array(
            'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
        ) );

        /*
         * Gutenberg theme support
         */
        add_theme_support( 'align-wide' );
        add_theme_support( 'responsive-embeds' );
        add_theme_support( 'wp-block-styles' );
        add_theme_support( 'editor-styles' );
        add_editor_style( 'alexandrie-custom-style',  get_template_directory_uri().'/assets/css/styles.css', array( 'wp-blocks','wp-editor' ), '1.01' );
    add_editor_style( 'alexandrie-gutenberg-in-style', get_template_directory_uri().'/assets/css/gutenberg.css', array( 'wp-blocks' ), '1.01' );
//        add_theme_support( 'dark-editor-style' );
    }

endif; // alexandrie_setup
add_action( 'after_setup_theme', 'alexandrie_setup' );

/**
 * Register widget area.
 */
function alexandrie_widgets_init() {
    // Sidebar
    register_sidebar( array(
        'name' => esc_html__( 'Sidebar', 'alexandrie' ),
        'id' => 'sidebar-1',
        'description' => '',
        'before_widget' => '<aside id="%1$s" class="widget shadow-none sidebar-widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title styled-heading-2">',
        'after_title' => '</h3>',
    ) );

    // Footer 1
    register_sidebar( array(
        'name' => esc_html__( 'Footer Widget Area 1', 'alexandrie' ),
        'id' => 'footer-1',
        'description' => '',
        'before_widget' => '<aside id="%1$s" class="widget widget-footer widget-footer-1 %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title styled-heading-2">',
        'after_title' => '</h3>',
    ) );

    // Footer 2
    register_sidebar( array(
        'name' => esc_html__( 'Footer Widget Area 2', 'alexandrie' ),
        'id' => 'footer-2',
        'description' => '',
        'before_widget' => '<aside id="%1$s" class="widget widget-footer widget-footer-2 %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title styled-heading-2">',
        'after_title' => '</h3>',
    ) );

    // Footer 3
    register_sidebar( array(
        'name' => esc_html__( 'Footer Widget Area 3', 'alexandrie' ),
        'id' => 'footer-3',
        'description' => '',
        'before_widget' => '<aside id="%1$s" class="widget widget-footer widget-footer-3 %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title styled-heading-2">',
        'after_title' => '</h3>',
    ) );

    // Footer 4
    register_sidebar( array(
        'name' => esc_html__( 'Footer Widget Area 4', 'alexandrie' ),
        'id' => 'footer-4',
        'description' => '',
        'before_widget' => '<aside id="%1$s" class="widget widget-footer widget-footer-4 %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title styled-heading-2">',
        'after_title' => '</h3>',
    ) );
    // header widget area
    register_sidebar( array(
        'name' => esc_html__( 'Header Widget Area', 'alexandrie' ),
        'id' => 'header-widget-area',
        'description' => '',
        'before_widget' => '<aside id="%1$s" class="widget shadow-none widget-header d-md-flex align-items-center %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title header-widget-title">',
        'after_title' => '</h3>',
    ) );
}

add_action( 'widgets_init', 'alexandrie_widgets_init' );

/*
 * Enqueue scripts and styles.
 */

function alexandrie_scripts() {
    wp_enqueue_style( 'start-style', get_stylesheet_uri() );
    wp_enqueue_style( 'alexandrie-my-custom-style', get_template_directory_uri() . "/assets/css/styles.css" );
//    wp_enqueue_style( 'alexandrie-gutenberg-style', get_template_directory_uri() . "/assets/css/gutenberg.css" );
    wp_enqueue_style( 'themify-icon-font', get_template_directory_uri() . "/assets/fonts/themify-icons/themify-icons.css" );
    wp_enqueue_style( 'themify-icon-font', get_template_directory_uri() . "/assets/fonts/fontello/css/fontello.css" );
    wp_enqueue_script( 'alexandrie-custom-js-plugins', get_template_directory_uri() . "/assets/js/plugins.js", array( 'jquery' ) );
    wp_enqueue_script( 'alexandrie-custom-js', get_template_directory_uri() . "/assets/js/main.js", array( 'jquery', 'alexandrie-custom-js-plugins' ) );
    wp_enqueue_script( 'alexandrie-custom-js', get_template_directory_uri() . "/assets/js/customizer.js", array( 'jquery' ) );

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}

add_action( 'wp_enqueue_scripts', 'alexandrie_scripts' );


/*
 * Include tgma
 */
//include_once( dirname( __FILE__ ) . '/inc/customizer/kirki/kirki.php' );
require_once get_template_directory() . '/inc/tgm_init.php';
/*
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/*
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';
/*
 * One click demo Import settings
 */
require get_template_directory() . '/inc/demo-import-settings.php';

/*
 * Customizer additions.
 */

function alexandrie_theme_include_kirki() {
    if ( class_exists( 'Kirki' ) ) {
//		include 'path-to-include-kirki.php';
//		include 'file-with-panels-sections-fields-etc.php';
       require get_template_directory() . '/inc/customizer.php';
    }
}

add_action( 'after_setup_theme', 'alexandrie_theme_include_kirki' );



/*
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
/* load gutenberg config */
require get_template_directory() . '/inc/gutenberg.php';





/**
 * Activates the welcome page.
 *
 * Adds transient to manage the welcome page.
 */
function alexandrie_welcome_activate() {

    // Transient max age is 60 seconds.
    set_transient( '_welcome_redirect_swp', true, 60 );
}

add_action( "after_switch_theme", "alexandrie_welcome_activate", 10, 2 );

/**
 * Deactivates welcome page
 *
 * Deletes the welcome page transient.
 */
function alexandrie_welcome_deactivate() {

    delete_transient( '_welcome_redirect_swp' );
}

add_action( "switch_theme", "alexandrie_welcome_deactivate", 10, 2 );

/**
 * Welcome page redirect.
 *
 * Only happens once and if the site is not a network or multisite.
 */
function alexandrie_safe_welcome_redirect() {

    // Bail if no activation redirect transient is present.
    if ( !get_transient( '_welcome_redirect_swp' ) ) {

        return;
    }

    // Delete the redirect transient.
    delete_transient( '_welcome_redirect_swp' );

    // Bail if activating from network or bulk sites.
    if ( is_network_admin() || isset( $_GET[ 'activate-multi' ] ) ) {

        return;
    }

    // Redirect to Welcome Page.
    wp_safe_redirect( add_query_arg( array( 'page' => 'startwp' ), admin_url( 'themes.php' ) ) );
}

//add_action( 'admin_init', 'alexandrie_safe_welcome_redirect' );


//add_filter('next_posts_link_attributes', 'posts_link_attributes');
//add_filter('previous_posts_link_attributes', 'posts_link_attributes');
//
//function posts_link_attributes() {
//    return 'class="btn btn-primary btn-oval"';
//}
//function theme_kirki_custom_css() {
//  wp_add_inline_style( 'kirki-styles', get_theme_mod('theme_custom_css') );
//}
//add_action('wp_enqueue_scripts', 'theme_kirki_custom_css', 200);


is_admin() || add_filter( 'dynamic_sidebar_params', 'alexandrie_add_widget_classes' );

/**
 * Add classes for widgets.
 *
 * @param  array $params
 * @return array
 */
function alexandrie_add_widget_classes( $params ) {
//    var_dump( $params );
    if ( $params[ 0 ][ 'id' ] === 'sidebar-1' ) {
//         var_dump($params);
        $shadow_class=get_theme_mod( 'sidebar_shadow','none');
        $widget_class="widget ".$shadow_class;
        $params[ 0 ] = array_replace( $params[ 0 ], array( 'before_widget' => preg_replace( '/shadow[-]*[a-z]*/',$shadow_class, $params[ 0 ][ 'before_widget' ] ) ) );
//        var_dump($params);
    }
    if ( $params[ 0 ][ 'id' ] === 'header-widget-area' ) {
//         var_dump($params);
        $shadow_class=get_theme_mod( 'header_widget_shadow','none');
        $widget_class="widget ".$shadow_class;
        $params[ 0 ] = array_replace( $params[ 0 ], array( 'before_widget' => preg_replace( '/shadow[-]*[a-z]*/',$shadow_class, $params[ 0 ][ 'before_widget' ] ) ) );
    }

    return $params;
}


add_theme_support( 'starter-content', array(
    // Place widgets in the desired locations (such as sidebar or footer).
    // Example widgets: archives, calendar, categories, meta, recent-comments, recent-posts, 
    //                  search, text_business_info, text_about
    'widgets'     => array( 'sidebar-1' => array( 'search', 'categories', 'meta'), ),
    // Specify pages to create, and optionally add custom thumbnails to them.
    // Note: For thumbnails, use attachment symbolic references in {{double-curly-braces}}.
    // Post options: post_type, post_title, post_excerpt, post_name (slug), post_content, 
    //               menu_order, comment_status, thumbnail (featured image ID), and template
    'posts'       => array( 'home', 'about', 'blog' => array( 'thumbnail' => '{{image-cafe}}' ), ),
    // Create custom image attachments used as post thumbnails for pages.
    // Note: You can reference these attachment IDs in the posts section above. Example: {{image-cafe}}
    'attachments' => array( 'image-cafe' => array( 'post_title' => 'Cafe', 'file' => 'assets/images/cafe.jpg' ), ),
    // Assign options defaults, such as front page settings.
    // The 'show_on_front' value can be 'page' to show a specified page, or 'posts' to show your latest posts.
    // Note: Use page ID symbolic references from the posts section above wrapped in {{double-curly-braces}}.
    'options'     => array( 'show_on_front'  => 'page', 'page_on_front' => '{{home}}', 'page_for_posts' => '{{blog}}', ),
    // Set the theme mods.
    'theme_mods'  => array( 'panel_1' => '{{about}}' ),
    // Set up nav menus.
    'nav_menus'   => array( 'top' => array( 'name' => 'Top Menu', 'items' => array( 'link_home', 'page_about', 'page_blog' )), ),
) );


function alexandrie_load_font_icons() {
 
wp_enqueue_style( 'alexandrie-font-icons', get_template_directory_uri() . '/assets/fonts/fontello/css/fontello.css' );
 
}
 
add_action( 'wp_enqueue_scripts', 'alexandrie_load_font_icons' );

function my_styles_method() {
	wp_enqueue_style(
		'alexandrie-menu-style',
		get_template_directory_uri() . '/assets/css/custom_script.css'
	);
        $sticky = get_theme_mod( 'sticky_header' ); 
        
        if($sticky){
            if(is_admin_bar_showing()){
        $custom_css = "
                #visible_scroll_down.header-pin,
                #visible_scroll_up.header-pin {
    position: fixed;
    top: 30px;
    width: 100%;
    background: #fff;
    box-shadow: 1px 2px 18px rgba(0,0,0,0.2);
    z-index: 1000;
        };";
            }else{
                $custom_css = "
                #visible_scroll_down.header-pin,
                #visible_scroll_up.header-pin {
    position: fixed;
    top: 0;
    width: 100%;
    background: #fff;
    box-shadow: 1px 2px 18px rgba(0,0,0,0.2);
    z-index: 1000;
        };";
            }
            }else{
            $custom_css = "";
        }
        wp_add_inline_style( 'alexandrie-menu-style', $custom_css );
}
add_action( 'wp_enqueue_scripts', 'my_styles_method' );



/**
 * Load Font Awesome's CSS from CDN
 *
 * @param  string                $stylesheet_uri Icon type's stylesheet URI.
 * @param  string                $icon_type_id   Icon type's ID.
 * @param  Icon_Picker_Type_Font $icon_type      Icon type's instance.
 *
 * @return string
 */
function myprefix_font_awesome_css_from_cdn( $stylesheet_uri, $icon_type_id, $icon_type ) {
    if ( 'fa' === $icon_type_id ) {
        $stylesheet_uri = sprintf(
            'https://use.fontawesome.com/releases/v5.6.3/css/all.css',
            $icon_type->version
        );
    }

    return $stylesheet_uri;
}
add_filter( 'icon_picker_icon_type_stylesheet_uri', 'myprefix_font_awesome_css_from_cdn', 10, 3 );

//editing post columns add featured iamge column
function alexandrie_custom_columns( $columns ) {
    $columns = array(
        'cb' => '<input type="checkbox" />',
        'title' => 'Title',
        'featured_image' => 'Image',
        'categories'=>'Categories',
        'tags'=>'Tags',
         'comments' => '<span class="vers"><div title="Comments" class="comment-grey-bubble"></div></span>',
        'date' => 'Date',
        
     );
    return $columns;
}
add_filter('manage_posts_columns' , 'alexandrie_custom_columns');

function alexandrie_custom_columns_data( $column, $post_id ) {
    switch ( $column ) {
    case 'featured_image':
        the_post_thumbnail( array(50, 50) );
        break;
    }
}
add_action( 'manage_posts_custom_column' , 'alexandrie_custom_columns_data', 10, 2 ); 
