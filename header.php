<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package alexandrie
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <meta name="viewport" content="width=device-width" />
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
        <?php wp_head(); ?>
    </head>
    <?php
    $menu_class = "menu " . get_theme_mod( 'primary_menu_align', 'nav' );
    $alexandrie_primary_menu = array( 'theme_location' => 'primary', 'container' => '', 'menu' => '', 'menu_class' => $menu_class, 'menu_id' => '', 'walker' => new alexandrie_navigation
    );

    $alexandrie_fallback_menu = array( 'theme_location' => '', 'container' => '', 'menu' => '', 'menu_class' => $menu_class, 'menu_id' => '', 'walker' => new alexandrie_navigation
    );
    ?>
    <body <?php body_class(); ?>>
        <div id="page" class="hfeed site <?php echo esc_attr( get_theme_mod( 'site_header_shadow', 'none' ) ); ?>">

            <?php
            $header_layout = alexandrie_header_layout_style();
            global $disable_header;
            if ( $disable_header == 0 ):
                do_action( 'alexandrie_before_header' );  // Before Header Hook
                ?> 
                <header class="header site-header" >

                    <?php if ( get_theme_mod( 'show_top_header', '1' ) ) { ?>
                        <div class="top-header bg-gray-100 py-2 ">
                            <div class="<?php alexandrie_header_layout(); ?>">
                                <div class="row">
                                    <div class="col-md-6">
                                        <?php
                                        wp_nav_menu( $args = array(
                                            'menu_class' => "navbar-social-menu", // (string) CSS class to use for the ul element which forms the menu. Default 'menu'.
                                            'depth' => "1", // (int) How many levels of the hierarchy are to be included. 0 means all. Default 0.
                                            'theme_location' => "header-social", // (string) Theme location to be used. Must be registered with register_nav_menu() in order to be selectable by the user.
                                            'menu_class' => 'nav justify-content-center justify-content-md-start ',
//                                        'fallback_cb' => false,
                                        ) );
                                        ?>

                                    </div>
                                    <div class="col-md-6 d-flex justify-content-center justify-content-lg-end">
                                        <?php get_search_form( $echo = true ); ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <?php do_action( 'alexandrie_before_header_content' );  // Before Header Hook  ?>


                    <?php if ( in_array( $header_layout, array( 'header-left', 'header-right' ) ) ) { ?>
                        <div id="<?php alexandrie_sticky_header(); ?>"><!--sticky header -->
                            <div  class="header-content <?php alexandrie_header_layout(); ?><?php echo " " . esc_attr( $header_layout ); ?>">
                            <?php } else { ?>
                                <div class="header-content <?php alexandrie_header_layout(); ?><?php echo " " . esc_attr( $header_layout ); ?> ">
                                <?php } ?>
                                <div class="site-branding">
                                    <?php
                                    the_custom_logo();
                                    if ( is_front_page() && is_home() ) :
                                        ?>
                                        <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
                                    <?php else : ?>
                                        <h1 class="site-title "><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
                                    <?php
                                    endif;
                                    $description = get_bloginfo( 'description', 'display' );
                                    if ( $description || is_customize_preview() ) :
                                        ?>
                                        <p class="site-description"><?php echo esc_html( $description ); /* WPCS: xss ok. */ ?></p>
                                    <?php endif;
                                    ?>


                                </div><!-- .site-branding -->
                                <?php
                                if ( $header_layout === 'header-left-widget' || $header_layout === 'header-right-widget' ) {
                                    ?>
                                    <div class="header-widget-area">
                                        <?php dynamic_sidebar( 'header-widget-area' ); ?>
                                    </div>
                                    <?php
                                } elseif ( in_array( $header_layout, array( 'header-left', 'header-right' ) ) ) {
                                    global $disable_nav;
                                    if ( $disable_nav == 0 ):
                                        ?> 
                                        <nav class="alexandrie_nav main-navigation">
                                            <label for="menu_toggle" class="menu_toggle"><?php _e( 'Menu' ); ?></label>
                                            <input type="checkbox" id="menu_toggle" /> 
                                            <?php
                                            if ( has_nav_menu( 'primary' ) ) {
                                                wp_nav_menu( $alexandrie_primary_menu );
                                            } else {
                                                wp_nav_menu( $alexandrie_fallback_menu );
                                            }
                                            ?>
                                        </nav><!-- #site-navigation -->

                                    <?php endif; ?>
                                <?php } ?>
                            </div>
                            <?php if ( in_array( $header_layout, array( 'header-left', 'header-right' ) ) ) { ?>
                            </div>
                        <?php } ?>        

                        <?php if ( in_array( $header_layout, array( 'header-left-widget', 'header-right-widget', 'header-center' ) ) ) { ?>
                            <div class="wrapper-navbar"  id="<?php alexandrie_sticky_header(); ?>">
                                <div class=" <?php alexandrie_header_layout(); ?>">
                                    <?php
                                    global $disable_nav;
                                    if ( $disable_nav == 0 ):
                                        ?> 
                                        <nav class="alexandrie_nav main-navigation">
                                            <label for="menu_toggle" class="menu_toggle"><?php _e( 'Menu' ); ?></label>
                                            <input type="checkbox" id="menu_toggle" />
                                            <?php
                                            if ( has_nav_menu( 'primary' ) ) {
                                                wp_nav_menu( $alexandrie_primary_menu );
                                            } else {
                                                wp_nav_menu( $alexandrie_fallback_menu );
                                            }
                                            ?>
                                        </nav><!-- #site-navigation -->

                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php } ?>

                        <?php do_action( 'alexandrie_after_header_content' );  // After Header Content Hook          ?>
                </header><!-- #masthead -->
                <?php do_action( 'alexandrie_after_header' ); ?><!-- After Header Hook -->
            <?php endif; ?>
        </div><!--hfeed-->
        <div id="content" class="site-content <?php echo alexandrie_content_layout(); ?>">
            <div class="alexandrie-content <?php echo alexandrie_layout(); ?>">  



