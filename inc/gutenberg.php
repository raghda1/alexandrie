<?php

/**
 * Enqueue WordPress theme styles within Gutenberg.
 */
add_action( 'enqueue_block_editor_assets', 'alexandrie_gutenberg_scripts' );
function alexandrie_gutenberg_scripts() {
    
    wp_enqueue_script(
            'alexandrie_guten_blocks', // Handle.
            get_theme_file_uri('assets/js/blocks.js'), array( 'wp-blocks', 'wp-i18n', 'wp-element' )
    );
    wp_enqueue_script(
            'alexandrie_guten_blocks', // Handle.
            get_theme_file_uri('assets/js/blocks.js'), array( 'wp-blocks', 'wp-i18n', 'wp-element' )
    );
}
function alexandrie_gutenberg_styles() {
    // Load the theme styles within Gutenberg.
    wp_enqueue_style( 'alexandrie-custom-style',  get_template_directory_uri().'/assets/css/styles.css', array( 'wp-blocks','wp-editor' ), '1.01' );
    wp_enqueue_style( 'alexandrie-gutenberg-in-style', get_template_directory_uri().'/assets/css/gutenberg.css', array( 'wp-blocks' ), '1.01' );
}

add_action( 'enqueue_block_assets', 'alexandrie_gutenberg_styles' );

function legit_block_editor_styles() {
    wp_enqueue_style( 'alexandrie-gutenberg-in-editor-style', get_template_directory_uri(). '/assets/css/gutenberg.css', false, '1.0', 'all' );
}
add_action( 'enqueue_block_editor_assets', 'legit_block_editor_styles' );

//function gutenberg_my_block_init() {
//    register_meta( 'post', 'author_count', array(
//        'show_in_rest' => true,
//        'single' => true,
//        'type' => 'integer',
//    ) );
//}
//add_action( 'init', 'gutenberg_my_block_init' );
