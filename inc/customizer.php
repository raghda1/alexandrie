<?php

/**
 * start Theme Customizer
 *
 * @package alexandrie
 */
if ( class_exists( 'Kirki' ) ) {
    /*     * **************************************************
     * Customizer controls
     * ************************************************** */

// Configuration
    Kirki::add_config( 'alexandrie', array(
        'capability' => 'edit_theme_options',
        'option_type' => 'theme_mod',
    ) );

    /*     * **************************************************
     * Panels
     * ************************************************** */
//    Kirki::add_panel( 'slider_sc', array(
//        'priority' => 10,
//        'title' => __( 'Alexandrie Plugin Options', 'alexandrie' ),
//        'description' => __( 'Alexandrie Plugin Options', 'alexandrie' ),
//    ) );
    
    Kirki::add_panel( 'theme_styles', array(
        'priority' => 10,
        'title' => __( 'Theme Options', 'alexandrie' ),
        'description' => __( 'Theme Options', 'alexandrie' ),
    ) );

    /*     * **************************************************
     * Sections
     * ************************************************** */
//Kirki::add_section( 'alexandrie_sc_slider_wide', array(
//        'title' => __( 'Slider', 'alexandrie' ),
//        'panel' => 'slider_sc',
//        'capability' => 'edit_theme_options',
//    ) );
    
// General Section
    Kirki::add_section( 'general', array(
        'title' => __( 'General', 'alexandrie' ),
        'panel' => 'theme_styles',
        'capability' => 'edit_theme_options',
    ) );

// Header Section
    Kirki::add_section( 'header', array(
        'title' => __( 'Header', 'alexandrie' ),
        'panel' => 'theme_styles',
        'capability' => 'edit_theme_options',
    ) );

// Menu Section
    Kirki::add_section( 'alexandrie_menu', array(
        'title' => __( 'Menu', 'alexandrie' ),
        'panel' => 'theme_styles',
        'capability' => 'edit_theme_options',
    ) );


// Blog Section

    Kirki::add_section( 'blog_archive', array(
        'title' => __( 'Blog / Archive ', 'alexandrie' ),
        'panel' => 'theme_styles',
        'capability' => 'edit_theme_options',
    ) );
    Kirki::add_section( 'single_post', array(
        'title' => __( 'Single Post', 'alexandrie' ),
        'panel' => 'theme_styles',
        'capability' => 'edit_theme_options',
    ) );




// Sidebar Section
    Kirki::add_section( 'sidebar', array(
        'title' => __( 'Sidebar', 'alexandrie' ),
        'panel' => 'theme_styles',
        'capability' => 'edit_theme_options',
    ) );

// 404
    Kirki::add_section( '404', array(
        'title' => __( '404', 'alexandrie' ),
        'panel' => 'theme_styles',
        'capability' => 'edit_theme_options',
    ) );

// Search
    Kirki::add_section( 'search_archive', array(
        'title' => __( 'Search', 'alexandrie' ),
        'panel' => 'theme_styles',
        'capability' => 'edit_theme_options',
    ) );

// Footer Section
    Kirki::add_section( 'footer', array(
        'title' => __( 'Footer', 'alexandrie' ),
        'panel' => 'theme_styles',
        'capability' => 'edit_theme_options',
    ) );

// Footer Copyright
    Kirki::add_section( 'copyright', array(
        'title' => __( 'Copyright', 'alexandrie' ),
        'panel' => 'theme_styles',
        'capability' => 'edit_theme_options',
    ) );

// Hooks
    Kirki::add_section( 'hooks', array(
        'title' => __( 'Hooks', 'alexandrie' ),
        'panel' => 'theme_styles',
        'capability' => 'edit_theme_options',
    ) );


    /*     * ****************************************************************************************
     * Fields
     * ***************************************************************************************** */

// Global Start Tabs
    function alexandrie_tabs( $setting, $section ) {
        Kirki::add_field( 'alexandrie', array(
            'type' => 'radio-buttonset',
            'settings' => $setting,
            'section' => $section,
            'default' => 'general',
            'transport' => 'postMessage',
            'choices' => array(
                'general' => __( 'General', 'alexandrie' ),
                'style' => __( 'Style', 'alexandrie' ),
                'advanced' => esc_attr__( 'Advanced', 'alexandrie' ),
            ),
        ) );
    }

// Shortcuts
    function alexandrie_shortcuts( $setting, $section, $partial_id, $partial_selector ) {
        Kirki::add_field( 'alexandrie', array(
            'type' => 'custom',
            'settings' => $setting,
            'section' => $section,
            'partial_refresh' => array(
                $partial_id => array(
                    'selector' => $partial_selector,
                    'render_callback' => '__return_false',
                ),
            ),
        ) );
    }

// Global Customizer Headlines
    function alexandrie_headlines( $setting, $section, $radio_setting, $tab, $text, $margin ) {
        Kirki::add_field( 'alexandrie', array(
            'type' => 'custom',
            'settings' => $setting,
            'section' => $section,
            'default' => '<div style="border-bottom: 1px solid #000; line-height: 2em; font-size: 16px; background: #000; color: #fff; padding: 0 10px; cursor: auto; margin-top: ' . $margin . '">' . esc_html__( $text, 'alexandrie' ) . '</div>',
            'active_callback' => array(
                array(
                    'setting' => $radio_setting,
                    'operator' => '==',
                    'value' => $tab,
                ),
            ),
        ) );
    }

// No Options Text
    function alexandrie_no_options( $setting, $section, $radio_setting, $tab ) {
        Kirki::add_field( 'alexandrie', array(
            'type' => 'custom',
            'settings' => $setting,
            'section' => $section,
            'default' => '<div style="padding: 20px;background-color: #333; color: #fff;">' . esc_html__( 'There are currently no options here.', 'alexandrie' ) . '</div>',
            'active_callback' => array(
                array(
                    'setting' => $radio_setting,
                    'operator' => '==',
                    'value' => $tab,
                ),
            ),
        ) );
    }

// Custom Logo Width
    Kirki::add_field( 'alexandrie', array(
        'type' => 'slider',
        'settings' => 'alexandrie_logo_width',
        'label' => __( 'Logo Width', 'alexandrie' ),
        'section' => 'title_tagline',
        'transport' => 'postMessage',
        'default' => 50,
        'choices' => array(
            'min' => '50',
            'max' => '600',
        ),
        'priority' => 8,
        'output' => array(
            array(
                'element' => '.site-header .site-branding .custom-logo',
                'property' => 'max-width',
                'units' => 'px',
            ),
        ),
        'js_vars' => array(
            array(
                'element' => '.site-header .site-branding .custom-logo',
                'function' => 'css',
                'property' => 'max-width',
                'units' => 'px',
            ),
        ),
    ) );
}
// Include files
// require get_template_directory() . '/inc/customizer/customizer-sections/general-customizer.php';
// require get_template_directory() . '/inc/customizer/customizer-sections/header-customizer.php';
// require get_template_directory() . '/inc/customizer/customizer-sections/menu-customizer.php';
// require get_template_directory() . '/inc/customizer/customizer-sections/blog-customizer.php';
// require get_template_directory() . '/inc/customizer/customizer-sections/sidebar-customizer.php';
// require get_template_directory() . '/inc/customizer/customizer-sections/footer-customizer.php';
// require get_template_directory() . '/inc/customizer/customizer-sections/copyright-customizer.php';
// require get_template_directory() . '/inc/customizer/customizer-sections/hooks-customizer.php';
// require get_template_directory() . '/inc/customizer/customizer-sections/404-customizer.php';
// require get_template_directory() . '/inc/customizer/customizer-sections/search-customizer.php';



include_once( dirname( __FILE__ ) . '/customizer/customizer-sections/general-customizer.php' );
include_once( dirname( __FILE__ ) . '/customizer/customizer-sections/header-customizer.php' );
include_once( dirname( __FILE__ ) . '/customizer/customizer-sections/menu-customizer.php' );
include_once( dirname( __FILE__ ) . '/customizer/customizer-sections/blog-archive-customizer.php' );
include_once( dirname( __FILE__ ) . '/customizer/customizer-sections/blog-single-customizer.php' );
//include_once( dirname( __FILE__ ) . '/customizer/customizer-sections/blog-customizer.php' );
include_once( dirname( __FILE__ ) . '/customizer/customizer-sections/sidebar-customizer.php' );
include_once( dirname( __FILE__ ) . '/customizer/customizer-sections/footer-customizer.php' );
include_once( dirname( __FILE__ ) . '/customizer/customizer-sections/copyright-customizer.php' );
include_once( dirname( __FILE__ ) . '/customizer/customizer-sections/hooks-customizer.php' );
//include_once( dirname( __FILE__ ) . '/customizer/customizer-sections/404-customizer.php' );
include_once( dirname( __FILE__ ) . '/customizer/customizer-sections/search-customizer.php' );
//include_once( dirname( __FILE__ ) . '/customizer/customizer-sections/slider-customizer.php' );



function alexandrie_kirki_configuration_styling( $config ) {
	return wp_parse_args( array(
		'logo_image'   => 'http://localhost/alex-demo/img/logo.png',
		'description'  => esc_html__( 'Blog Theme', 'alexandrie' ),
		'color_accent' => '#000',
		'color_back'   => '#FFFFFF',
	), $config );
}
//add_filter( 'kirki_config', 'alexandrie_kirki_configuration_styling' );