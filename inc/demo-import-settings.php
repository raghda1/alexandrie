<?php

if ( class_exists( 'OCDI_Plugin' ) ) {

    function alexandrie_ocdi_import_files() {
        return array(
            array(
                'import_file_name' => 'Demo Import 1',
//      'categories'                 => array( 'Category 1', 'Category 2' ),
                'import_file_url' => 'https://raghda1.gitlab.io/alex-demo-data/demo-basic/alexandrie-data.xml',
                'import_widget_file_url' => 'https://raghda1.gitlab.io/alex-demo-data/demo-basic/widgets.wie',
                'import_customizer_file_url' => 'https://raghda1.gitlab.io/alex-demo-data/demo-basic/customizer.dat',
//      'import_redux'               => array(
//        array(
//          'file_url'    => 'http://www.your_domain.com/ocdi/redux.json',
//          'option_name' => 'redux_option_name',
//        ),
//      ),
                'import_preview_image_url' => 'https://raghda1.gitlab.io/alex-demo-data/demo-basic/screenshot.png',
                'import_notice' => __( 'After you import this demo, you will have to setup the slider separately.', 'alexandrie' ),
                'preview_url' => 'http://alexwp.raghda.space/demo/',
            ),
            array(
                'import_file_name' => 'Demo Import 2',
//                'categories' => array( 'New category', 'Old category' ),
                'import_file_url' => 'https://raghda1.gitlab.io/alex-demo-data/demo-2/alexandrie-data.xml',
                'import_widget_file_url' => 'https://raghda1.gitlab.io/alex-demo-data/demo-2/widgets.wie',
                'import_customizer_file_url' => 'https://raghda1.gitlab.io/alex-demo-data/demo-2/customizer.dat',
//                'import_redux' => array(
//                    array(
//                        'file_url' => 'http://www.your_domain.com/ocdi/redux.json',
//                        'option_name' => 'redux_option_name',
//                    ),
//                    array(
//                        'file_url' => 'http://www.your_domain.com/ocdi/redux2.json',
//                        'option_name' => 'redux_option_name_2',
//                    ),
//                ),
                'import_preview_image_url' => 'https://raghda1.gitlab.io/alex-demo-data/demo-2/screenshot.png',
                'import_notice' => __( 'A special note for this import.', 'alexandrie' ),
                'preview_url' => 'http://alexwp2.raghda.space/demo',
            ),
        );
    }

    add_filter( 'pt-ocdi/import_files', 'alexandrie_ocdi_import_files' );

    

//Setup front page and menus
function alexandrie_taunita_after_import_setup() {
	
    // Assign menus to their locations
    $social_menu = get_term_by( 'name', 'social', 'nav_menu' );
    $main_menu = get_term_by( 'name', 'Main menu', 'nav_menu' );
    $footer_menu = get_term_by( 'name', 'footer', 'nav_menu' );
    set_theme_mod( 'nav_menu_locations', array(
            'header-social'  => $social_menu->term_id,
            'footer-social' => $social_menu->term_id,
            'primary' => $main_menu->term_id,
            'footer-menu' => $footer_menu->term_id,
        )
    );

    

}
add_action( 'pt-ocdi/after_import', 'alexandrie_taunita_after_import_setup' );

}