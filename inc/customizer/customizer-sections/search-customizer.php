<?php

/* * **************************************************
 * Search Fields
 * ************************************************** */

// Search Tabs
alexandrie_tabs( 'search_archive_setting', 'search_archive' );

alexandrie_shortcuts( 'search_shortcut', 'search_archive', 'partial_404', '.page_404 .error-404' );

// Search / Archive Options
alexandrie_headlines( 'search_archive_options', 'search_archive', 'search_archive_setting', 'general', 'Search / Archive', '10px' );

// Search Headline
Kirki::add_field( 'alexandrie', array(
    'type' => 'textarea',
    'settings' => 'search_headline',
    'label' => __( 'Search Headline', 'alexandrie' ),
    'section' => 'search_archive',
    'transport' => 'auto',
    'default' => __( 'Search Results for', 'alexandrie' ),
    'active_callback' => array(
        array(
            'setting' => 'search_archive_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );

// Search / Archive Layout
Kirki::add_field( 'alexandrie', array(
    'type' => 'sortable',
    'settings' => 'search_posts_layout',
    'label' => __( 'Search Posts Layout', 'alexandrie' ),
    'section' => 'search_archive',
    'default' => array(
        'thumbnail',
        'title',
        'meta',
        'content'
    ),
    'choices' => array(
        'thumbnail' => __( 'Featured Image', 'alexandrie' ),
        'title' => __( 'Title', 'alexandrie' ),
        'meta' => __( 'Meta', 'alexandrie' ),
        'content' => __( 'Content', 'alexandrie' ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'search_archive_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );

// Search / Archive Meta Layout
Kirki::add_field( 'alexandrie', array(
    'type' => 'sortable',
    'settings' => 'search_posts_meta_layout',
    'label' => __( 'Search Posts Meta Layout', 'alexandrie' ),
    'section' => 'search_archive',
    'default' => array(
        'author',
        'date',
        'category',
        'comments'
    ),
    'choices' => array(
        'author' => __( 'Author', 'alexandrie' ),
        'date' => __( 'Date', 'alexandrie' ),
        'category' => __( 'Category', 'alexandrie' ),
        'comments' => __( 'Comments', 'alexandrie' ),
        'tags' => __( 'Tags', 'alexandrie' ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'search_archive_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );

// Link Post Titles
Kirki::add_field( 'alexandrie', array(
    'type' => 'toggle',
    'settings' => 'search_archive_title_linkable',
    'label' => __( 'Link Title', 'alexandrie' ),
    'section' => 'search_archive',
    'default' => '1',
    'active_callback' => array(
        array(
            'setting' => 'search_archive_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );

// Link Post Thumbnails
Kirki::add_field( 'alexandrie', array(
    'type' => 'toggle',
    'settings' => 'search_archive_thumbnail_linkable',
    'label' => __( 'Link Thumbnail', 'alexandrie' ),
    'section' => 'search_archive',
    'default' => '1',
    'active_callback' => array(
        array(
            'setting' => 'search_archive_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );

// Search / Archive / Single Meta seprator
Kirki::add_field( 'alexandrie', array(
    'type' => 'radio-buttonset',
    'settings' => 'search_archive_single_meta_sep',
    'label' => __( 'Meta Separator Icon', 'alexandrie' ),
    'section' => 'search_archive',
    'transport' => 'auto',
    'default' => '/',
    'suffix' => '"',
    'choices' => array(
        '/' => esc_attr__( '/', 'alexandrie' ),
        '-' => esc_attr__( '-', 'alexandrie' ),
        '|' => esc_attr__( '|', 'alexandrie' ),
        ' ' => esc_attr__( 'space', 'alexandrie' ),
    ),
    'output' => array(
        array(
            'element' => '.search_blog .entry-meta span::after',
            'property' => 'content',
            'prefix' => '"',
            'suffix' => '"',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.search_blog .entry-meta span::after',
            'function' => 'css',
            'property' => 'content',
            'prefix' => '"',
            'suffix' => '"',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'search_archive_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );

// Search / Archive Excerpt / Content
Kirki::add_field( 'alexandrie', array(
    'type' => 'select',
    'settings' => 'search_archive_content_excerpt',
    'label' => __( 'Content / Excerpt', 'alexandrie' ),
    'section' => 'search_archive',
    'default' => 'search_archive_excerpt',
    'multiple' => 1,
    'choices' => array(
        'search_archive_excerpt' => esc_attr__( 'Excerpt', 'alexandrie' ),
        'search_archive_content' => esc_attr__( 'Content', 'alexandrie' ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'search_archive_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );

// Search / Archive Excerpt Length
Kirki::add_field( 'alexandrie', array(
    'type' => 'number',
    'settings' => 'search_archive_excerpt_length',
    'label' => esc_attr__( 'Excerpt Length', 'alexandrie' ),
    'section' => 'search_archive',
    'default' => 55,
    'active_callback' => array(
        array(
            'setting' => 'search_archive_content_excerpt',
            'operator' => '==',
            'value' => 'search_archive_excerpt',
        ),
        array(
            'setting' => 'search_archive_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );

// Search / Archive Button Headline
Kirki::add_field( 'alexandrie', array(
    'type' => 'custom',
    'settings' => 'search_button_headline',
    'section' => 'search_archive',
    'default' => '<div style="border-bottom: 1px solid #d5d8dc;line-height: 2em;font-size: 20px;text-transform: capitalize; font-weight: 600;">' . esc_html__( 'Read More', 'alexandrie' ) . '</div>',
    'active_callback' => array(
        array(
            'setting' => 'search_archive_content_excerpt',
            'operator' => '==',
            'value' => 'search_archive_excerpt',
        ),
        array(
            'setting' => 'search_archive_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );

// Search Archive Button Text
Kirki::add_field( 'alexandrie', array(
    'type' => 'text',
    'settings' => 'search_btn_text',
    'label' => __( 'Button Text', 'alexandrie' ),
    'section' => 'search_archive',
    'transport' => 'auto',
    'default' => __( 'Read More', 'alexandrie' ),
    'active_callback' => array(
        array(
            'setting' => 'search_archive_content_excerpt',
            'operator' => '==',
            'value' => 'search_archive_excerpt',
        ),
        array(
            'setting' => 'search_archive_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );

// Search Archive Button Position
Kirki::add_field( 'alexandrie', array(
    'type' => 'radio-buttonset',
    'settings' => 'search_archive_button_position',
    'label' => __( 'Button Position', 'alexandrie' ),
    'section' => 'search_archive',
    'transport' => 'auto',
    'default' => 'btn_left',
    'choices' => array(
        'btn_left' => esc_attr__( 'Left', 'alexandrie' ),
        'btn_right' => esc_attr__( 'Right', 'alexandrie' ),
        'btn_full' => esc_attr__( 'Full Width', 'alexandrie' ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'search_archive_content_excerpt',
            'operator' => '==',
            'value' => 'search_archive_excerpt',
        ),
        array(
            'setting' => 'search_archive_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );

// Search Archive Button Icon Show / Hide
Kirki::add_field( 'alexandrie', array(
    'type' => 'toggle',
    'settings' => 'search_archive_button_icon_toggle',
    'label' => __( 'Use Button Icon', 'alexandrie' ),
    'section' => 'search_archive',
    'default' => '1',
    'active_callback' => array(
        array(
            'setting' => 'search_archive_content_excerpt',
            'operator' => '==',
            'value' => 'search_archive_excerpt',
        ),
        array(
            'setting' => 'search_archive_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );

// Search Archive Button Icon Left / Right
Kirki::add_field( 'alexandrie', array(
    'type' => 'radio-buttonset',
    'settings' => 'search_archive_button_icon_position',
    'label' => __( 'Button Icon Position', 'alexandrie' ),
    'section' => 'search_archive',
    'transport' => 'auto',
    'default' => 'icon_right',
    'choices' => array(
        'icon_left' => esc_attr__( 'Icon Left', 'alexandrie' ),
        'icon_right' => esc_attr__( 'Icon Right', 'alexandrie' ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'search_archive_button_icon_toggle',
            'operator' => '==',
            'value' => '1',
        ),
        array(
            'setting' => 'search_archive_content_excerpt',
            'operator' => '==',
            'value' => 'search_archive_excerpt',
        ),
        array(
            'setting' => 'search_archive_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );

// Search Archive Button Icon Left
Kirki::add_field( 'alexandrie', array(
    'type' => 'radio-buttonset',
    'settings' => 'search_archive_button_left_icon',
    'label' => __( 'Left Icon', 'alexandrie' ),
    'section' => 'search_archive',
    'transport' => 'auto',
    'default' => '&#171;',
    'choices' => array(
        '&#171;' => esc_attr__( '«', 'alexandrie' ),
        '&#8249;' => esc_attr__( '‹', 'alexandrie' ),
        '&#8592;' => esc_attr__( '←', 'alexandrie' ),
        '&#9668;' => esc_attr__( '◄', 'alexandrie' ),
        '' => esc_attr__( 'none', 'alexandrie' ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'search_archive_button_icon_position',
            'operator' => '==',
            'value' => 'icon_left',
        ),
        array(
            'setting' => 'search_archive_button_icon_toggle',
            'operator' => '==',
            'value' => '1',
        ),
        array(
            'setting' => 'search_archive_content_excerpt',
            'operator' => '==',
            'value' => 'search_archive_excerpt',
        ),
        array(
            'setting' => 'search_archive_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );

// Search Archive Button Icon Right
Kirki::add_field( 'alexandrie', array(
    'type' => 'radio-buttonset',
    'settings' => 'search_archive_button_right_icon',
    'label' => __( 'Right Icon', 'alexandrie' ),
    'section' => 'search_archive',
    'transport' => 'auto',
    'default' => '&#187;',
    'choices' => array(
        '&#187;' => esc_attr__( '»', 'alexandrie' ),
        '&#8250;' => esc_attr__( '›', 'alexandrie' ),
        '&#8594;' => esc_attr__( '→', 'alexandrie' ),
        '&#9658;' => esc_attr__( '►', 'alexandrie' ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'search_archive_button_icon_position',
            'operator' => '==',
            'value' => 'icon_right',
        ),
        array(
            'setting' => 'search_archive_button_icon_toggle',
            'operator' => '==',
            'value' => '1',
        ),
        array(
            'setting' => 'search_archive_content_excerpt',
            'operator' => '==',
            'value' => 'search_archive_excerpt',
        ),
        array(
            'setting' => 'search_archive_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );



/* * ********* Style Fields ********** */

// Search For Title Styles
alexandrie_headlines( 'search_for_title_styles', 'search_archive', 'search_archive_setting', 'style', 'Search Title', '10px' );


// Search For Title
Kirki::add_field( 'alexandrie', array(
    'type' => 'typography',
    'settings' => 'search_for_title',
    'section' => 'search_archive',
    'transport' => 'auto',
    'default' => array(
        'font-family' => '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif',
        'font-size' => '48px',
        'line-height' => '1',
        'letter-spacing' => '0',
        'text-transform' => 'none',
        'text-align' => 'left',
        'variant' => 'regular',
        'color' => '#000',
    ),
    'output' => array(
        array(
            'element' => '.search_title',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.search_title',
            'function' => 'css',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'search_archive_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );


// Search Title Styles
alexandrie_headlines( 'search_title_styles', 'search_archive', 'search_archive_setting', 'style', 'Title', '10px' );

// Search Title Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'search_title_color',
    'label' => __( 'Title', 'alexandrie' ),
    'section' => 'search_archive',
    'transport' => 'auto',
    'default' => '#000000',
    'output' => array(
        array(
            'element' => '.search_blog .entry-title, .search_blog .entry-title a',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.search_blog .entry-title, .search_blog .entry-title a',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'search_archive_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Search Title Hover Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'search_title_hover_color',
    'label' => __( 'Title Hover', 'alexandrie' ),
    'section' => 'search_archive',
    'transport' => 'auto',
    'default' => '#1e73be',
    'output' => array(
        array(
            'element' => '.search_blog .entry-title a:hover',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.search_blog .entry-title a:hover',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'search_archive_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Search Archive Title
Kirki::add_field( 'alexandrie', array(
    'type' => 'typography',
    'settings' => 'search_title',
    'section' => 'search_archive',
    'transport' => 'auto',
    'default' => array(
        'font-family' => '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif',
        'font-size' => '48px',
        'line-height' => '1',
        'letter-spacing' => '0',
        'text-transform' => 'none',
        'text-align' => 'left',
    ),
    'output' => array(
        array(
            'element' => '.search_blog .entry-title, .search_blog .entry-title a',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'search_archive_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );



// Search Meta Styles
alexandrie_headlines( 'search_meta_styles', 'search_archive', 'search_archive_setting', 'style', 'Meta', '35px' );


// Search Meta Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'search_meta_color',
    'label' => __( 'Meta', 'alexandrie' ),
    'section' => 'search_archive',
    'transport' => 'auto',
    'default' => '#94979c',
    'output' => array(
        array(
            'element' => '.search_blog .entry-meta,.search_blog .entry-meta span::after',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.search_blog .entry-meta,.search_blog .entry-meta span::after',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'search_archive_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'search_meta_bg_color',
    'label' => __( 'Meta background', 'alexandrie' ),
    'section' => 'search_archive',
    'transport' => 'auto',
    'default' => '#94979c',
    'choices' => array(
        'alpha' => true,
    ),
    'output' => array(
        array(
            'element' => '.search_blog .entry-meta',
            'property' => 'background-color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.search_blog .entry-meta',
            'function' => 'css',
            'property' => 'background-color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'search_archive_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Search Meta Link Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'search_meta_link__color',
    'label' => __( 'Meta Link', 'alexandrie' ),
    'section' => 'search_archive',
    'transport' => 'auto',
    'default' => '#1e73be',
    'output' => array(
        array(
            'element' => '.search_blog .entry-meta a',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.search_blog .entry-meta a',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'search_archive_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Search Meta Link Hover Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'search_meta_hover_color',
    'label' => __( 'Meta Hover', 'alexandrie' ),
    'section' => 'search_archive',
    'transport' => 'auto',
    'default' => '#000000',
    'output' => array(
        array(
            'element' => '.search_blog .entry-meta a:hover',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.search_blog .entry-meta a:hover',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'search_archive_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Search Meta
Kirki::add_field( 'alexandrie', array(
    'type' => 'typography',
    'settings' => 'search_meta',
    'section' => 'search_archive',
    'transport' => 'auto',
    'default' => array(
        'font-family' => '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif',
        'font-size' => '16px',
        'line-height' => '1',
        'letter-spacing' => '0',
        'text-transform' => 'none',
        'text-align' => 'left',
    ),
    'output' => array(
        array(
            'element' => '.search_blog .entry-meta',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.search_blog .entry-meta',
            'function' => 'css',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'search_archive_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );


// Search Content Styles
alexandrie_headlines( 'search_content_styles', 'search_archive', 'search_archive_setting', 'style', 'Content', '35px' );

// Search Content Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'search_content_color',
    'label' => __( 'Text', 'alexandrie' ),
    'section' => 'search_archive',
    'transport' => 'auto',
    'default' => '#000000',
    'output' => array(
        array(
            'element' => '.search_blog .entry-content',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.search_blog .entry-content',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'search_archive_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Search Content
Kirki::add_field( 'alexandrie', array(
    'type' => 'typography',
    'settings' => 'search_content',
    'section' => 'search_archive',
    'transport' => 'auto',
    'default' => array(
        'font-family' => '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif',
        'font-size' => '17px',
        'line-height' => '1.5',
        'letter-spacing' => '0',
        'text-transform' => 'none',
        'text-align' => 'left',
    ),
    'output' => array(
        array(
            'element' => '.search_blog .entry-content',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'search_archive_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );


// Search Button Styles
alexandrie_headlines( 'search_btn_styles', 'search_archive', 'search_archive_setting', 'style', 'Button', '35px' );

// Search Button BG Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'search_btn_bg_color',
    'label' => __( 'Background', 'alexandrie' ),
    'section' => 'search_archive',
    'transport' => 'auto',
    'default' => '#e5e8e8',
    'output' => array(
        array(
            'element' => '.search_blog .entry-content .read-more',
            'property' => 'background-color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.search_blog .entry-content .read-more',
            'function' => 'css',
            'property' => 'background-color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'search_archive_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Search Button Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'search_btn_color',
    'label' => __( 'Text', 'alexandrie' ),
    'section' => 'search_archive',
    'transport' => 'auto',
    'default' => '#000000',
    'output' => array(
        array(
            'element' => '.search_blog .entry-content .read-more',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.search_blog .entry-content .read-more',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'search_archive_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Search Button Hover BG Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'search_btn_hover_bg_color',
    'label' => __( 'Hover Background', 'alexandrie' ),
    'section' => 'search_archive',
    'transport' => 'auto',
    'default' => '#cfd7cf',
    'output' => array(
        array(
            'element' => '.search_blog .entry-content .read-more:hover',
            'property' => 'background-color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.search_blog .entry-content .read-more:hover',
            'function' => 'css',
            'property' => 'background-color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'search_archive_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Search Button Hover Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'search_btn_hover_color',
    'label' => __( 'Hover Text', 'alexandrie' ),
    'section' => 'search_archive',
    'transport' => 'auto',
    'default' => '#000000',
    'output' => array(
        array(
            'element' => '.search_blog .entry-content .read-more:hover',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.search_blog .entry-content .read-more:hover',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'search_archive_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Search Button Typography
Kirki::add_field( 'alexandrie', array(
    'type' => 'typography',
    'settings' => 'search_btn_typography',
    'section' => 'search_archive',
    'transport' => 'auto',
    'default' => array(
        'font-family' => '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif',
        'font-size' => '16px',
        'line-height' => '1.5',
        'letter-spacing' => '0',
        'text-transform' => 'none',
        'text-align' => 'left',
    ),
    'output' => array(
        array(
            'element' => '.search_blog .entry-content .read-more',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.search_blog .entry-content .read-more',
            'function' => 'css',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'search_archive_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );


/* * ********* Advanced Fields ********** */

// Search Posts Padding
Kirki::add_field( 'alexandrie', array(
    'settings' => 'search_archive_spacing',
    'section' => 'search_archive',
    'label' => esc_html__( 'Search Posts Padding', 'alexandrie' ),
    'type' => 'spacing',
    'default' => array(
        'top' => '45px',
        'right' => '0px',
        'bottom' => '20px',
        'left' => '0px',
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.search_blog',
            'property' => 'padding',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.search_blog',
            'function' => 'css',
            'property' => 'padding',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'search_archive_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'settings' => 'search_archive_margin',
    'section' => 'search_archive',
    'label' => esc_html__( 'Search Posts margin', 'alexandrie' ),
    'type' => 'spacing',
    'default' => array(
        'top' => '40px',
        'right' => '0px',
        'bottom' => '20px',
        'left' => '0px',
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.search_blog',
            'property' => 'margin',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.search_blog',
            'function' => 'css',
            'property' => 'margin',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'search_archive_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'settings' => 'search_post_border_radius',
    'section' => 'search_archive',
    'label' => esc_html__( 'search Post Border radius', 'alexandrie' ),
    'type' => 'dimensions',
    'default' => array(
        'top-left-radius' => '0',
        'top-right-radius' => '0',
        'bottom-right-radius' => '0',
        'bottom-left-radius' => '0',
    ),
    'choices' => array(
        'top-left-radius' => esc_attr__( 'Top-Left radius', 'alexandrie' ),
        'top-right-radius' => esc_attr__( 'Top-Right-radius:', 'alexandrie' ),
        'bottom-right-radius' => esc_attr__( 'Bottom-Right-radius', 'alexandrie' ),
        'bottom-left-radius' => esc_attr__( 'Bottom-Left radius', 'alexandrie' ),
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.search_blog',
            'property' => 'border',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.search_blog',
            'function' => 'css',
            'property' => 'border',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'search_archive_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
    ),
) );

Kirki::add_field( 'alexandrie', array(
    'type' => 'select',
    'settings' => 'search_archive_post_shadow',
    'label' => __( 'Search Post Shadow', 'alexandrie' ),
    'section' => 'search_archive',
    'default' => 'shadow-none',
    'priority' => 10,
    'multiple' => 1,
    'choices' => array(
        'shadow-none' => esc_attr__( 'None', 'alexandrie' ),
        'shadow-sm' => esc_attr__( 'Small Shadow', 'alexandrie' ),
        'shadow' => esc_attr__( 'Shadow', 'alexandrie' ),
        'shadow-lg' => esc_attr__( 'large Shadow', 'alexandrie' ),
        'shadow-custom' => esc_attr__( '3D shadow', 'alexandrie' ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'search_archive_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
    ),
) );
alexandrie_headlines( 'search_meta_advanced_styles', 'search', 'search_archive_setting', 'advanced', 'Meta Advanced Style', '35px' );
// Blog / Archive meta border
Kirki::add_field( 'alexandrie', array(
    'settings' => 'search_meta_border_radius',
    'section' => 'search_archive',
    'label' => esc_html__( 'search Blog Meta Border radius', 'alexandrie' ),
    'type' => 'dimensions',
    'default' => array(
        'top-left-radius' => '0',
        'top-right-radius' => '0',
        'bottom-right-radius' => '0',
        'bottom-left-radius' => '0',
    ),
    'choices' => array(
        'top-left-radius' => esc_attr__( 'Top-Left radius', 'alexandrie' ),
        'top-right-radius' => esc_attr__( 'Top-Right-radius:', 'alexandrie' ),
        'bottom-right-radius' => esc_attr__( 'Bottom-Right-radius', 'alexandrie' ),
        'bottom-left-radius' => esc_attr__( 'Bottom-Left radius', 'alexandrie' ),
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.search_blog   .entry-meta',
            'property' => 'border',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.search_blog   .entry-meta',
            'function' => 'css',
            'property' => 'border',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'search_archive_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
    ),
) );

Kirki::add_field( 'alexandrie', array(
    'type' => 'radio-buttonset',
    'settings' => 'search_meta_border_style',
    'label' => __( 'Meta Border Style', 'alexandrie' ),
    'section' => 'search_archive',
    'default' => 'none',
//    'transport' => 'auto',
    'priority' => 10,
    'multiple' => 1,
    'choices' => array(
        'none' => esc_attr__( 'None', 'alexandrie' ),
        'solid' => esc_attr__( 'solid', 'alexandrie' ),
        'dotted' => esc_attr__( 'dotted', 'alexandrie' ),
        'dashed' => esc_attr__( 'dashed', 'alexandrie' ),
    ),
    'output' => array(
        array(
            'element' => '.search_blog   .entry-meta',
            'property' => 'border-style',
        ),
    ),
//    'js_vars' => array(
//        array(
//            'element' => '.search_blog   .entry-meta',
//            'function' => 'css',
//            'property' => 'border-style',
//        ),
//    ),
    'active_callback' => array(
        array(
            'setting' => 'search_archive_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
) ) );

Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'search_meta_border_color',
    'label' => esc_attr__( 'Meta Border color', 'alexandrie' ),
    'section' => 'search_archive',
//    'priority' => 10,
    'default' => '#ccc',
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.search_blog   .entry-meta',
            'property' => 'border-color',
        ),
    ),
//    'js_vars' => array(
//        array(
//            'element' => '.search_blog   .entry-meta',
//            'function' => 'css',
//            'property' => 'border-color',
//        ),
//    ),
    'active_callback' => array(
        array(
            'setting' => 'search_archive_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
        array(
            'setting' => 'search_meta_border_style',
            'value' => 'none',
            'operator' => '!='
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'settings' => 'search_meta_border',
    'section' => 'search_archive',
    'label' => esc_html__( 'Meta Border Thickness', 'alexandrie' ),
    'type' => 'dimensions',
    'default' => array(
        'top-width' => '0px',
        'right-width' => '0px',
        'bottom-width' => '0px',
        'left-width' => '0px',
    ),
    'choices' => array(
        'top-width' => esc_attr__( 'Top', 'alexandrie' ),
        'right-width' => esc_attr__( 'Right', 'alexandrie' ),
        'bottom-width' => esc_attr__( 'Bottom', 'alexandrie' ),
        'left-width' => esc_attr__( 'Left', 'alexandrie' ),
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.search_blog   .entry-meta',
            'property' => 'border',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.search_blog   .entry-meta',
            'function' => 'css',
            'property' => 'border',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'search_archive_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
        array(
            'setting' => 'search_meta_border_style',
            'value' => 'none',
            'operator' => '!='
        ),
    ),
) );


