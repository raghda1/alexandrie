<?php

/* * **************************************************
 * Blog And Archive Section
 * ************************************************** */

// Menu Tabs
alexandrie_tabs( 'single_post_setting', 'single_post' );

alexandrie_shortcuts( 'blog_shortcut', 'single_post', 'partial_blog', '#content .alexandrie_blog' );




/* Blog Single General  */

// Blog Single Options
alexandrie_headlines( 'blog_single_options', 'single_post', 'single_post_setting', 'general', 'Blog Single', '35px' );

// Single Post Layout
Kirki::add_field( 'alexandrie', array(
    'type' => 'sortable',
    'settings' => 'single_post_layout',
    'label' => __( 'Single Post Layout', 'alexandrie' ),
    'section' => 'single_post',
    'default' => array(
        'thumbnail',
        'title',
        'meta',
        'content'
    ),
    'choices' => array(
        'thumbnail' => __( 'Featured Image', 'alexandrie' ),
        'title' => __( 'Title', 'alexandrie' ),
        'meta' => __( 'Meta', 'alexandrie' ),
        'content' => __( 'Content', 'alexandrie' ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'single_post_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );
kirki::add_field( 'alexandrie', array(
    'type' => 'toggle',
    'settings' => 'single_posts_show_tags_after_content',
    'label' => __( 'Show Tags after Content ', 'alexandrie' ),
    'section' => 'single_post',
    'default' => '0',
//	'priority'    => 10,
    'choices' => array(
        'on' => esc_attr__( 'Show', 'alexandrie' ),
        'off' => esc_attr__( 'Hide', 'alexandrie' ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'single_post_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );
// Single Post Meta Layout
Kirki::add_field( 'alexandrie', array(
    'type' => 'sortable',
    'settings' => 'single_posts_meta_layout',
    'label' => __( 'Single Posts Meta Layout', 'alexandrie' ),
    'section' => 'single_post',
    'default' => array(
        'author',
        'date',
        'category',
        'comments'
    ),
    'choices' => array(
        'author' => __( 'Author', 'alexandrie' ),
        'date' => __( 'Date', 'alexandrie' ),
        'category' => __( 'Category', 'alexandrie' ),
        'comments' => __( 'Comments', 'alexandrie' ),
        'tags' => __( 'Tags', 'alexandrie' ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'single_post_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );
// Blog / Archive / Single Meta seprator
Kirki::add_field( 'alexandrie', array(
    'type' => 'radio-buttonset',
    'settings' => 'single_post_meta_sep',
    'label' => __( 'Meta Separator Icon', 'alexandrie' ),
    'section' => 'single_post',
    'transport' => 'postMessage',
    'default' => '/',
    'suffix' => '"',
    'choices' => array(
        '/' => esc_attr__( '/', 'alexandrie' ),
        '\2013' => esc_attr__( '-', 'alexandrie' ),
        '|' => esc_attr__( '|', 'alexandrie' ),
        '\0020' => esc_attr__( 'space', 'alexandrie' ),
    ),
    'output' => array(
        array(
            'element' => '.alexandrie_blog.blog_single .entry-meta span::after',
            'property' => 'content',
            'prefix' => '"',
            'suffix' => '"',
        ),
        array(
            'element' => '.alexandrie_blog.blog_single .entry-meta span:last-of-type::after',
            'property' => 'content',
            'value_pattern'=>'none',
//            'prefix' => '"',
//            'suffix' => '"',
        ),
        array(
            'element' => '.alexandrie_blog.blog_single .entry-meta span::after',
            'property' => 'margin-right',
            'value_pattern' => '5',
            'units'=>'px',
        ),
        array(
            'element' => '.alexandrie_blog.blog_single .entry-meta span::after',
            'property' => 'margin-left',
            'value_pattern' => '5',
            'units'=>'px',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.alexandrie_blog.blog_single .entry-meta span::after',
            'function' => 'css',
            'property' => 'content',
            'prefix' => '"',
            'suffix' => '"',
        ),
         array(
            'element' => '.alexandrie_blog.blog_single .entry-meta span:last-of-type::after',
             'function' => 'css',
            'property' => 'content',
            'value_pattern'=>'none',
//            'prefix' => '"',
//            'suffix' => '"',
        ),
        array(
            'element' => '.alexandrie_blog.blog_single .entry-meta span::after',
            'function' => 'css',
            'property' => 'margin-right',
            'value_pattern' => '5',
            'units'=>'px',
        ),
        array(
            'element' => '.alexandrie_blog.blog_single .entry-meta span::after',
            'function' => 'css',
            'property' => 'margin-left',
            'value_pattern' => '5',
            'units'=>'px',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'single_post_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );

// Single Post nav Condtion
Kirki::add_field( 'alexandrie', array(
    'type' => 'toggle',
    'settings' => 'single_post_nav_condition',
    'label' => __( 'Custom Post Navigation', 'alexandrie' ),
    'section' => 'single_post',
    'default' => '0',
    'active_callback' => array(
        array(
            'setting' => 'single_post_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );

// Older Posts Text
Kirki::add_field( 'alexandrie', array(
    'type' => 'text',
    'settings' => 'blog_single_old_nav',
    'label' => __( 'Prev', 'alexandrie' ),
    'section' => 'single_post',
    'default' => __( 'Previous', 'alexandrie' ),
    'active_callback' => array(
        array(
            'setting' => 'single_post_nav_condition',
            'operator' => '==',
            'value' => '1',
        ),
        array(
            'setting' => 'single_post_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );

// Newer Posts Text
Kirki::add_field( 'alexandrie', array(
    'type' => 'text',
    'settings' => 'blog_single_newer_nav',
    'label' => __( 'Next', 'alexandrie' ),
    'section' => 'single_post',
    'default' => __( 'Next', 'alexandrie' ),
    'active_callback' => array(
        array(
            'setting' => 'single_post_nav_condition',
            'operator' => '==',
            'value' => '1',
        ),
        array(
            'setting' => 'single_post_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );


/* * ********* Style Fields ********** */

// Blog Title Styles
alexandrie_headlines( 'post_title_styles', 'single_post', 'single_post_setting', 'style', 'Title', '10px' );


// Blog Archive Title
Kirki::add_field( 'alexandrie', array(
    'type' => 'typography',
    'settings' => 'blog_single_title',
    'label' => __( 'Title', 'alexandrie' ),
    'section' => 'single_post',
    'transport' => 'auto',
    'default' => array(
        'font-family' => 'Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif',
        'font-size' => '48px',
        'line-height' => '1',
        'letter-spacing' => '0',
        'text-align' => 'left',
        'color' => '#000',
        'text-transform' => 'none',
        'variant' => 'regular',
    ),
    'output' => array(
        array(
            'element' => '.alexandrie_blog.blog_single .entry-title,.page .entry-title, .alexandrie_blog.blog_single .entry-title a',
        ),
    ),
//    'js_vars' => array(
//        array(
//            'element' => '.alexandrie_blog.blog_single .entry-title,.page .entry-title, .alexandrie_blog.blog_single .entry-title a',
//        'function' => 'style',
//            ),
//    ),
    'active_callback' => array(
        array(
            'setting' => 'single_post_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );



// Blog Meta Styles
alexandrie_headlines( 'post_meta_styles', 'single_post', 'single_post_setting', 'style', 'Meta', '35px' );

//Blog Meta bg color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'blog_single_meta_bg_color',
    'label' => __( 'Meta Background', 'alexandrie' ),
    'section' => 'single_post',
    'transport' => 'auto',
    'default' => '#F8F9FA',
    'choices' => [
        'alpha' => true,
    ],
    'output' => array(
        array(
            'element' => '.blog_single .entry-meta,.blog_single .entry-meta span::after',
            'property' => 'background-color',
        ),
    ),
//    'js_vars' => array(
//        array(
//            'element' => '.alexandrie_blog.blog_single .entry-meta,.alexandrie_blog.blog_single .entry-meta span::after',
//            'function' => 'css',
//            'property' => 'background-color',
//        ),
//    ),
    'active_callback' => array(
        array(
            'setting' => 'single_post_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Blog Meta Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'blog_single_meta_color',
    'label' => __( 'Meta', 'alexandrie' ),
    'section' => 'single_post',
    'transport' => 'auto',
    'default' => '#848484',
    'output' => array(
        array(
            'element' => '.alexandrie_blog.blog_single .entry-meta,.alexandrie_blog.blog_single .entry-meta span::after',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.alexandrie_blog.blog_single .entry-meta,.alexandrie_blog.blog_single .entry-meta span::after',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'single_post_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Blog Meta Link Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'blog_single_meta_link_color',
    'label' => __( 'Meta Link', 'alexandrie' ),
    'section' => 'single_post',
    'transport' => 'auto',
    'default' => '#848484',
    'output' => array(
        array(
            'element' => '.alexandrie_blog.blog_single .entry-meta a',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.alexandrie_blog.blog_single .entry-meta a',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'single_post_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Blog Meta Link Hover Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'blog_single_meta_hover_color',
    'label' => __( 'Meta Hover', 'alexandrie' ),
    'section' => 'single_post',
    'transport' => 'auto',
    'default' => '#000000',
    'output' => array(
        array(
            'element' => '.alexandrie_blog.blog_single .entry-meta a:hover',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.alexandrie_blog.blog_single .entry-meta a:hover',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'single_post_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Blog Meta
Kirki::add_field( 'alexandrie', array(
    'type' => 'typography',
    'settings' => 'blog_single_meta',
    'section' => 'single_post',
    'transport' => 'auto',
    'default' => array(
        'font-family' => '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif',
        'font-size' => '16px',
        'line-height' => '1',
        'letter-spacing' => '0',
        'text-transform' => 'none',
        'text-align' => 'left',
    ),
    'output' => array(
        array(
            'element' => '.alexandrie_blog.blog_single .entry-meta',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'single_post_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );


// Blog Content Styles
alexandrie_headlines( 'Post_content_styles', 'single_post', 'single_post_setting', 'style', 'Content', '35px' );

// Blog Content Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'blog_single_content_color',
    'label' => __( 'Text', 'alexandrie' ),
    'section' => 'single_post',
    'transport' => 'auto',
    'default' => '#000000',
    'output' => array(
        array(
            'element' => '.alexandrie_blog.blog_single .entry-content',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.alexandrie_blog.blog_single .entry-content',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'single_post_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'blog_single_content_bg_color',
    'label' => __( 'content bg color', 'alexandrie' ),
    'section' => 'single_post',
    'transport' => 'auto',
    'default' => 'rgba(255,255,255,0)',
    'choices'=>array(
        'alpha'=>true,
    ),
    'output' => array(
        array(
            'element' => '.alexandrie_blog.blog_single .entry-content',
            'property' => 'background-color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.alexandrie_blog.blog_single .entry-content',
            'function' => 'css',
            'property' => 'background-color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'single_post_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Blog Content
Kirki::add_field( 'alexandrie', array(
    'type' => 'typography',
    'settings' => 'blog_single_content',
    'section' => 'single_post',
    'transport' => 'auto',
    'default' => array(
        'font-family' => '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif',
        'font-size' => '17px',
        'line-height' => '1.5',
        'letter-spacing' => '0',
        'text-align' => 'left',
        'text-transform' => 'none',
    ),
    'output' => array(
        array(
            'element' => '.alexandrie_blog.blog_single .entry-content',
        ),
    ),
//    'js_vars' => array(
//        array(
//            'element' => '.alexandrie_blog.blog_single .entry-content',
//            'function' => 'style',
//        ),
//    ),
    'active_callback' => array(
        array(
            'setting' => 'single_post_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );








// Blog Post Navigation Styles
alexandrie_headlines( 'post_navigation', 'single_post', 'single_post_setting', 'style', 'Single post Navigation', '35px' );

// Blog Navigation Link Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'blog_post_nav_link_color',
    'label' => __( 'Link color', 'alexandrie' ),
    'section' => 'single_post',
    'transport' => 'auto',
    'default' => '#fff',
    'output' => array(
        array(
            'element' => '.navigation.post-navigation .nav-links .nav-next a,.navigation.post-navigation .nav-links .nav-previous a',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.navigation.post-navigation .nav-links .nav-next a,.navigation.post-navigation .nav-links .nav-previous a',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'single_post_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'blog_post_nav_link_bg_color',
    'label' => __( 'Link Background color', 'alexandrie' ),
    'section' => 'single_post',
    'transport' => 'auto',
    'default' => '#000',
    'output' => array(
        array(
            'element' => '.navigation.post-navigation .nav-links .nav-next,.navigation.post-navigation .nav-links .nav-previous',
            'property' => 'background-color',
        ),
    ),
//    'js_vars' => array(
//        array(
//            'element' => '.navigation.post-navigation .nav-links .nav-next,.navigation.post-navigation .nav-links .nav-previous',
//            'function' => 'style',
//            'property' => 'background-color',
//        ),
//    ),
    'active_callback' => array(
        array(
            'setting' => 'single_post_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Blog Navigation Link Hover Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'blog_post_nav_link_hover_color',
    'label' => __( 'Link Hover', 'alexandrie' ),
    'section' => 'single_post',
    'transport' => 'auto',
    'default' => '#fff',
    'output' => array(
        array(
            'element' => '.navigation.post-navigation .nav-links .nav-next:hover a,.navigation.post-navigation .nav-links .nav-previous:hover a',
            'property' => 'color',
        ),
    ),
//    'js_vars' => array(
//        array(
//            'element' => '.navigation.post-navigation .nav-links .nav-next:hover a,.navigation.post-navigation .nav-links .nav-previous:hover a',
//            'function' => 'style',
//            'property' => 'color',
//        ),
//    ),
    'active_callback' => array(
        array(
            'setting' => 'single_post_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );
// Blog Navigation Link Hover Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'blog_nav_link_hover_bg_color',
    'label' => __( 'Link Hover Background', 'alexandrie' ),
    'section' => 'single_post',
    'transport' => 'auto',
    'default' => '#000000',
    'output' => array(
        array(
            'element' => '.navigation.post-navigation .nav-links .nav-next:hover,.navigation.post-navigation .nav-links .nav-previous:hover',
            'property' => 'background-color',
        ),
    ),
//    'js_vars' => array(
//        array(
//            'element' => '.navigation.post-navigation .nav-links .nav-next:hover,.navigation.post-navigation .nav-links .nav-previous:hover',
//            'function' => 'style',
//            'property' => 'background-color',
//        ),
//    ),
    'active_callback' => array(
        array(
            'setting' => 'single_post_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Blog Posts / Single post Nav Typography
Kirki::add_field( 'alexandrie', array(
    'type' => 'typography',
    'settings' => 'blog_post_nav_typography',
    'section' => 'single_post',
    'transport' => 'auto',
    'default' => array(
        'font-family' => '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif',
        'variant' => 'regular',
        'font-size' => '16px',
        'line-height' => '1.5',
        'letter-spacing' => '0',
        'text-transform' => 'none',
    ),
    'output' => array(
        array(
            'element' => '.navigation.post-navigation .nav-links .nav-next a,.navigation.post-navigation .nav-links .nav-previous a',
        ),
    ),
//    'js_vars' => array(
//        array(
//            'element' => '.navigation.post-navigation .nav-links .nav-next a,.navigation.post-navigation .nav-links .nav-previous a',
//            'function' => 'style',
//        ),
//    ),
    'active_callback' => array(
        array(
            'setting' => 'single_post_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );


/* * ********* Advanced Fields ********** */
alexandrie_headlines( 'Single_Post_spacing_advanced_styles', 'single_post', 'single_post_setting', 'advanced', 'Single Post Spacing ', '35px' );

Kirki::add_field( 'alexandrie', array(
    'settings' => 'blog_single_padding',
    'section' => 'single_post',
    'label' => esc_html__( 'Single Post container Padding', 'alexandrie' ),
    'type' => 'spacing',
    'default' => array(
        'top' => '40px',
        'right' => '0',
        'bottom' => '0',
        'left' => '0',
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.blog_single',
            'property' => 'padding',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'single_post_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
    ),
) );
alexandrie_headlines( 'Single_Post_thumb_advanced_styles', 'single_post', 'single_post_setting', 'advanced', 'Single Post Thumbnail Style', '35px' );

Kirki::add_field( 'alexandrie', array(
    'type' => 'select',
    'settings' => 'blog_single_thumb_shadow',
    'label' => __( 'Single Post Thummbnail Shadow', 'alexandrie' ),
    'section' => 'single_post',
    'default' => 'shadow-none',
    'priority' => 10,
    'multiple' => 1,
    'choices' => array(
        'shadow-none' => esc_attr__( 'None', 'alexandrie' ),
        'shadow-sm' => esc_attr__( 'Small Shadow', 'alexandrie' ),
        'shadow' => esc_attr__( 'Shadow', 'alexandrie' ),
        'shadow-lg' => esc_attr__( 'large Shadow', 'alexandrie' ),
        'shadow-custom' => esc_attr__( '3D shadow', 'alexandrie' ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'single_post_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'settings' => 'blog_single_thumb_border_radius',
    'section' => 'single_post',
    'label' => esc_html__( 'Post Thumbnail Border radius', 'alexandrie' ),
    'type' => 'dimensions',
    'default' => [
        'top-left-radius' => '0px',
        'top-right-radius' => '0px',
        'bottom-left-radius' => '0px',
        'bottom-right-radius' => '0px',
    ],
    'choices' => [
        'top-left-radius' => esc_attr__( 'Top Left', 'alexandrie' ),
        'top-right-radius' => esc_attr__( 'Top Right', 'alexandrie' ),
        'bottom-left-radius' => esc_attr__( 'Bottom Left', 'alexandrie' ),
        'bottom-right-radius' => esc_attr__( 'Bottom Right', 'alexandrie' ),
    ],
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.alexandrie_blog.blog_single .post-image',
            'property' => 'border',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.alexandrie_blog.blog_single .post-image',
            'function' => 'style',
            'property' => 'border',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'single_post_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'settings' => 'blog_single_thumb_margin',
    'section' => 'single_post',
    'label' => esc_html__( 'Post Thumbnail margin', 'alexandrie' ),
    'type' => 'dimensions',
    'default' => array(
        'left' => '0',
        'right' => '0',
        'top' => '0',
        'bottom' => '0',
    ),
    'choices' => array(
        'left' => esc_attr__( 'Margin left', 'alexandrie' ),
        'right' => esc_attr__( 'Margin right:', 'alexandrie' ),
        'top' => esc_attr__( 'Margin Top', 'alexandrie' ),
        'bottom' => esc_attr__( 'Margin Bottom', 'alexandrie' ),
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.alexandrie_blog.blog_single .post-image',
            'property' => 'margin',
        ),
    ),
//    'js_vars' => array(
//        array(
//            'element' => '.alexandrie_blog.blog_single .post-image',
//            'function' => 'style',
//            'property' => 'margin',
//        ),
//    ),
    'active_callback' => array(
        array(
            'setting' => 'single_post_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
    ),
) );

alexandrie_headlines( 'Single_Post_Meta_advanced_styles', 'single_post', 'single_post_setting', 'advanced', 'Single Post Meta Advanced Style', '35px' );

Kirki::add_field( 'alexandrie', array(
    'type' => 'select',
    'settings' => 'blog_single_meta_shadow',
    'label' => __( 'Single Post Meta Shadow', 'alexandrie' ),
    'section' => 'single_post',
    'default' => 'shadow-none',
    'priority' => 10,
    'multiple' => 1,
    'choices' => array(
        'shadow-none' => esc_attr__( 'None', 'alexandrie' ),
        'shadow-sm' => esc_attr__( 'Small Shadow', 'alexandrie' ),
        'shadow' => esc_attr__( 'Shadow', 'alexandrie' ),
        'shadow-lg' => esc_attr__( 'large Shadow', 'alexandrie' ),
        'shadow-custom' => esc_attr__( '3D shadow', 'alexandrie' ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'single_post_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'settings' => 'blog_single_meta_padding',
    'section' => 'single_post',
    'label' => esc_html__( 'Single Post Meta Padding', 'alexandrie' ),
    'type' => 'spacing',
    'default' => array(
        'top' => '0',
        'right' => '0',
        'bottom' => '0',
        'left' => '0',
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.blog_single .entry-meta',
            'property' => 'padding',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'single_post_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
    ),
) );

// single post meta border
Kirki::add_field( 'alexandrie', array(
    'settings' => 'blog_single_meta_border_radius',
    'section' => 'single_post',
    'label' => esc_html__( 'Single Post Meta Border radius', 'alexandrie' ),
    'type' => 'dimensions',
    'default' => array(
        'top-left-radius' => '0',
        'top-right-radius' => '0',
        'bottom-right-radius' => '0',
        'bottom-left-radius' => '0',
    ),
    'choices' => array(
        'top-left-radius' => esc_attr__( 'Top-Left radius', 'alexandrie' ),
        'top-right-radius' => esc_attr__( 'Top-Right-radius:', 'alexandrie' ),
        'bottom-right-radius' => esc_attr__( 'Bottom-Right-radius', 'alexandrie' ),
        'bottom-left-radius' => esc_attr__( 'Bottom-Left radius', 'alexandrie' ),
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.blog_single .entry-meta',
            'property' => 'border',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'single_post_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
    ),
) );

Kirki::add_field( 'alexandrie', array(
    'type' => 'radio-buttonset',
    'settings' => 'blog_single_meta_border_style',
    'label' => __( 'Single Post Meta Border Style', 'alexandrie' ),
    'section' => 'single_post',
    'default' => 'none',
    'priority' => 10,
    'multiple' => 1,
    'choices' => array(
        'none' => esc_attr__( 'None', 'alexandrie' ),
        'solid' => esc_attr__( 'solid', 'alexandrie' ),
        'dotted' => esc_attr__( 'dotted', 'alexandrie' ),
        'dashed' => esc_attr__( 'dashed', 'alexandrie' ),
    ),
    'output' => array(
        array(
            'element' => '.alexandrie_blog.blog_single .entry-meta',
            'property' => 'border-style',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'single_post_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
) ) );

Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'single_post_meta_border_color',
    'label' => esc_attr__( 'Border color', 'alexandrie' ),
    'section' => 'single_post',
    'priority' => 10,
    'default' => '#ccc',
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.alexandrie_blog.blog_single .entry-meta',
            'property' => 'border-color',
        ),
    ),
//    'js_vars' => array(
//        array(
//            'element' => '.alexandrie_blog.blog_single .entry-meta',
//            'function' => 'style',
//            'property' => 'border-color',
//        ),
//    ),
    'active_callback' => array(
        array(
            'setting' => 'single_post_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
        array(
            'setting' => 'blog_single_meta_border_style',
            'value' => 'none',
            'operator' => '!='
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'settings' => 'blog_single_meta_border',
    'section' => 'single_post',
    'label' => esc_html__( 'Border Thickness', 'alexandrie' ),
    'type' => 'dimensions',
    'default' => array(
        'top-width' => '0px',
        'right-width' => '0px',
        'bottom-width' => '0px',
        'left-width' => '0px',
    ),
    'choices' => array(
        'top-width' => esc_attr__( 'Top', 'alexandrie' ),
        'right-width' => esc_attr__( 'Right', 'alexandrie' ),
        'bottom-width' => esc_attr__( 'Bottom', 'alexandrie' ),
        'left-width' => esc_attr__( 'Left', 'alexandrie' ),
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.alexandrie_blog.blog_single .entry-meta',
            'property' => 'border',
        ),
    ),
//    'js_vars' => array(
//        array(
//            'element' => '.alexandrie_blog.blog_single .entry-meta',
//            'function' => 'style',
//            'property' => 'border',
//        ),
//    ),
    'active_callback' => array(
        array(
            'setting' => 'single_post_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
        array(
            'setting' => 'blog_single_meta_border_style',
            'value' => 'none',
            'operator' => '!='
        ),
    ),
) );

alexandrie_headlines( 'Single_Post_nav_advanced_styles', 'single_post', 'single_post_setting', 'advanced', 'Single Post Nav Advanced Style', '35px' );
Kirki::add_field( 'alexandrie', array(
    'settings' => 'blog_post_nav_border_radius',
    'section' => 'single_post',
    'label' => esc_html__( 'Single Post Navigation Border radius', 'alexandrie' ),
    'type' => 'dimensions',
    'default' => array(
        'top-left-radius' => '1rem',
        'top-right-radius' => '1rem',
        'bottom-left-radius' => '1rem',
        'bottom-right-radius' => '1rem',
    ),
    'choices' => array(
        'top-left-radius' => esc_attr__( 'Top-Left radius', 'alexandrie' ),
        'top-right-radius' => esc_attr__( 'Top-Right-radius:', 'alexandrie' ),
        'bottom-left-radius' => esc_attr__( 'Bottom-Left radius', 'alexandrie' ),
        'bottom-right-radius' => esc_attr__( 'Bottom-Right-radius', 'alexandrie' ),
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.navigation.post-navigation .nav-links .nav-next, .navigation.post-navigation .nav-links .nav-previous',
            'property' => 'border',
        ),
    ),
//    'js_vars' => array(
//        array(
//            'element' => '.navigation.post-navigation .nav-links .nav-next, .navigation.post-navigation .nav-links .nav-previous',
//            'function' => 'style',
//            'property' => 'border',
//        ),
//    ),
    'active_callback' => array(
        array(
            'setting' => 'single_post_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
    ),
) );
