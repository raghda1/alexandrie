<?php

/* * **************************************************
 * General Section
 * ************************************************** */

// General Tabs
alexandrie_tabs( 'general_setting', 'general' );

// Content Layout
Kirki::add_field( 'alexandrie', array(
    'type' => 'select',
    'settings' => 'global_content_layout',
    'label' => __( 'Content Layout', 'alexandrie' ),
    'section' => 'general',
    'transport' => 'auto',
    'default' => 'container',
    'choices' => array(
        'container' => esc_attr__( 'Boxed', 'alexandrie' ),
        'container-fluid' => esc_attr__( 'Full Width', 'alexandrie' ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'general_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );


// Site Width
Kirki::add_field( 'alexandrie', array(
    'type' => 'slider',
    'settings' => 'site_width',
    'label' => __( 'Container Width', 'alexandrie' ),
    'section' => 'general',
    'default' => 1170,
    'transport' => 'postMessage',
    'choices' => array(
        'min' => '700',
        'max' => '2000',
    ),
    'active_callback' => array(
        array(
            'setting' => 'general_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
    'output' => array(
        array(
            'element' => '.container',
            'property' => 'max-width',
            'units' => 'px',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.container',
            'function' => 'css',
            'property' => 'max-width',
            'units' => 'px',
        ),
    ),
) );


/* General Styles Fields */

// Base Styles
alexandrie_headlines( 'base_styles', 'general', 'general_setting', 'style', 'Base', '10px' );

// Body Text Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'accent_color_1',
    'label' => __( 'Primary color', 'alexandrie' ),
    'describtion'=>__('quote left border color,featured label background,submit button background','alexandrie'),
    'section' => 'general',
    'transport' => 'postMessage',
    'default' => '#3a3a3a',
    'output' => array(
        array(
            'element' => '.wp-block-quote',
            'property' => 'border-color',
        ),
        array(
            'element' => '.featured-label',
            'property' => 'background-color',
        ),
        array(
            'element' => '.btn-primary',
            'property' => 'background-color',
        ),
        array(
            'element' => '.comments-area .comment-respond .form-submit .submit',
            'property' => 'background-color',
        ),
        array(
            'element' => '.btn-primary',
            'property' => 'border-color',
        ),
        array(
            'element' => '.comments-area .comment-respond .form-submit .submit',
            'property' => 'border-color',
        ),
//        array(
//            'element' => '.mejs-container, .mejs-container .mejs-controls, .mejs-embed, .mejs-embed body',
//            'property' => 'background-color',
//        ),
        
    ),
    'js_vars' => array(
        array(
            'element' => '.wp-block-quote',
            'function' => 'css',
            'property' => 'border-color',
        ),
        array(
            'element' => '.featured-label',
            'function' => 'css',
            'property' => 'background-color',
        ),
        array(
            'element' => '.btn-primary',
            'function' => 'css',
            'property' => 'background-color',
        ),
//        array(
//            'element' => '.mejs-container, .mejs-container .mejs-controls, .mejs-embed, .mejs-embed body',
//            'function' => 'css',
//            'property' => 'background-color',
//        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'general_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'accent_color_2',
    'label' => __( 'Secondary Color ', 'alexandrie' ),
    'description'=>__('from feild bg,image caption bg,page header bg,tag cloud link bg','alexandrie'),
    'section' => 'general',
    'transport' => 'postMessage',
    'default' => '#F8F9FA',
    'output' => array(
//        array(
//            'element' => '.wp-block-quote.is-style-large',
//            'property' => 'border-color',
//        ),
        array(
            'element' => '.form-control, .widget_archive select, .widget select',
            'property' => 'background-color',
        ),
        array(
            'element' => 'wp-block-image figcaption',
            'function' => 'css',
            'property' => 'background-color',
        ),
        array(
            'element' => '.wp-caption-text.gallery-caption',
            'property' => 'background-color',
        ),
        array(
            'element' => '.tagcloud a',
            'property' => 'background-color',
        ),
        array(
            'element' => '.page-header',
            'property' => 'background-color',
        ),
    ),
    'js_vars' => array(
//        array(
//            'element' => '.wp-block-quote.is-style-large',
//            'function' => 'css',
//            'property' => 'background-color',
//        ),
        array(
            'element' => '.form-control, .widget_archive select, .widget select',
            'function' => 'css',
            'property' => 'background-color',
        ),
        array(
            'element' => 'wp-block-image figcaption',
            'function' => 'css',
            'property' => 'background-color',
        ),
        array(
            'element' => '.wp-caption-text.gallery-caption',
            'property' => 'background-color',
        ),
        array(
            'element' => '.tagcloud a',
            'property' => 'background-color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'general_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'comment_bg_color',
    'label' => __( 'comment bg Color', 'alexandrie' ),
    'section' => 'general',
    'transport' => 'postMessage',
    'default' => '#Fff',
    'output' => array(
        array(
            'element' => '.comments-area ol.comment-list li.depth-1',
            'property' => 'background-color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.comments-area ol.comment-list li.depth-1',
            'function' => 'css',
            'property' => 'background-color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'general_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Link Color / Link Visited
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'link_color',
    'label' => __( 'Link', 'alexandrie' ),
    'section' => 'general',
    'transport' => 'postMessage',
    'default' => '#1e73be',
    'output' => array(
        array(
            'element' => 'a, a:visited',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => 'a, a:visited',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'general_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Link Hover Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'link_hover_color',
    'label' => __( 'Link Hover', 'alexandrie' ),
    'section' => 'general',
    'transport' => 'postMessage',
    'default' => '#000000',
    'output' => array(
        array(
            'element' => 'a:hover',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => 'a:hover',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'general_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

Kirki::add_field( 'alexandrie', array(
    'type' => 'typography',
    'settings' => 'body_typography',
    'section' => 'general',
    'transport' => 'auto',
    'default' => array(
        'font-family' => '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif',
        'variant' => 'regular',
        'font-size' => '17px',
        'line-height' => '1.5',
        'letter-spacing' => '0',
        'color' => '#3a3a3a',
        'subsets' => array( 'latin-ext' ),
    ),
    'output' => array(
        array(
            'element' => 'body',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'general_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );



// Headlines Styles
alexandrie_headlines( 'heading_styles', 'general', 'general_setting', 'style', 'Headings', '35px' );

// Headings Typography
Kirki::add_field( 'alexandrie', array(
    'type' => 'typography',
    'settings' => 'headings_typography',
    'section' => 'general',
    'transport' => 'auto',
    'default' => array(
        'font-family' => '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif',
        'variant' => 'regular',
    ),
    'output' => array(
        array(
            'element' => array( 'h1', 'h2', 'h3', 'h4', 'h5', 'h6' ),
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'general_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

Kirki::add_field( 'alexandrie', array(
    'type' => 'radio-buttonset',
    'settings' => 'headings_setting',
    'section' => 'general',
    'default' => 'h1',
    'transport' => 'postMessage',
    'choices' => array(
        'h1' => esc_attr__( 'H1', 'alexandrie' ),
        'h2' => esc_attr__( 'H2', 'alexandrie' ),
        'h3' => esc_attr__( 'H3', 'alexandrie' ),
        'h4' => esc_attr__( 'H4', 'alexandrie' ),
        'h5' => esc_attr__( 'H5', 'alexandrie' ),
        'h6' => esc_attr__( 'H6', 'alexandrie' ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'general_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// H1 Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'h1_color',
    'section' => 'general',
    'transport' => 'postMessage',
    'default' => '#000000',
    'output' => array(
        array(
            'element' => 'h1',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => 'h1',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'general_setting',
            'operator' => '==',
            'value' => 'style',
        ),
        array(
            'setting' => 'headings_setting',
            'operator' => '==',
            'value' => 'h1',
        ),
    ),
) );

// H2 Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'h2_color',
    'section' => 'general',
    'transport' => 'postMessage',
    'default' => '#000000',
    'output' => array(
        array(
            'element' => 'h2',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => 'h2',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'general_setting',
            'operator' => '==',
            'value' => 'style',
        ),
        array(
            'setting' => 'headings_setting',
            'operator' => '==',
            'value' => 'h2',
        ),
    ),
) );

// H3 Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'h3_color',
    'section' => 'general',
    'transport' => 'postMessage',
    'default' => '#000000',
    'output' => array(
        array(
            'element' => 'h3',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => 'h3',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'general_setting',
            'operator' => '==',
            'value' => 'style',
        ),
        array(
            'setting' => 'headings_setting',
            'operator' => '==',
            'value' => 'h3',
        ),
    ),
) );

// H4 Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'h4_color',
    'section' => 'general',
    'transport' => 'postMessage',
    'default' => '#000000',
    'output' => array(
        array(
            'element' => 'h4',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => 'h4',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'general_setting',
            'operator' => '==',
            'value' => 'style',
        ),
        array(
            'setting' => 'headings_setting',
            'operator' => '==',
            'value' => 'h4',
        ),
    ),
) );

// H5 Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'h5_color',
    'section' => 'general',
    'transport' => 'postMessage',
    'default' => '#000000',
    'output' => array(
        array(
            'element' => 'h5',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => 'h5',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'general_setting',
            'operator' => '==',
            'value' => 'style',
        ),
        array(
            'setting' => 'headings_setting',
            'operator' => '==',
            'value' => 'h5',
        ),
    ),
) );

// H6 Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'h6_color',
    'section' => 'general',
    'transport' => 'postMessage',
    'default' => '#000000',
    'output' => array(
        array(
            'element' => 'h6',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => 'h6',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'general_setting',
            'operator' => '==',
            'value' => 'style',
        ),
        array(
            'setting' => 'headings_setting',
            'operator' => '==',
            'value' => 'h6',
        ),
    ),
) );

// Heading H1
Kirki::add_field( 'alexandrie', array(
    'type' => 'typography',
    'settings' => 'h1_typography',
    'section' => 'general',
    'transport' => 'auto',
    'default' => array(
        'font-size' => '48px',
        'line-height' => '1.5',
        'letter-spacing' => '0',
        'text-transform' => 'none',
    ),
    'output' => array(
        array(
            'element' => 'h1',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'general_setting',
            'operator' => '==',
            'value' => 'style',
        ),
        array(
            'setting' => 'headings_setting',
            'operator' => '==',
            'value' => 'h1',
        ),
    ),
) );

// Heading H2
Kirki::add_field( 'alexandrie', array(
    'type' => 'typography',
    'settings' => 'h2_typography',
    'section' => 'general',
    'transport' => 'auto',
    'default' => array(
        'font-size' => '42px',
        'line-height' => '1.5',
        'letter-spacing' => '0',
        'text-transform' => 'none',
    ),
    'output' => array(
        array(
            'element' => 'h2',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'general_setting',
            'operator' => '==',
            'value' => 'style',
        ),
        array(
            'setting' => 'headings_setting',
            'operator' => '==',
            'value' => 'h2',
        ),
    ),
) );

// Heading H3
Kirki::add_field( 'alexandrie', array(
    'type' => 'typography',
    'settings' => 'h3_typography',
    'section' => 'general',
    'transport' => 'auto',
    'default' => array(
        'font-size' => '30px',
        'line-height' => '1.5',
        'letter-spacing' => '0',
        'text-transform' => 'none',
    ),
    'output' => array(
        array(
            'element' => 'h3',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'general_setting',
            'operator' => '==',
            'value' => 'style',
        ),
        array(
            'setting' => 'headings_setting',
            'operator' => '==',
            'value' => 'h3',
        ),
    ),
) );

// Heading H4
Kirki::add_field( 'alexandrie', array(
    'type' => 'typography',
    'settings' => 'h4_typography',
    'section' => 'general',
    'transport' => 'auto',
    'default' => array(
        'font-size' => '20px',
        'line-height' => '1.5',
        'letter-spacing' => '0',
        'text-transform' => 'none',
    ),
    'output' => array(
        array(
            'element' => 'h4',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'general_setting',
            'operator' => '==',
            'value' => 'style',
        ),
        array(
            'setting' => 'headings_setting',
            'operator' => '==',
            'value' => 'h4',
        ),
    ),
) );

// Heading H5
Kirki::add_field( 'alexandrie', array(
    'type' => 'typography',
    'settings' => 'h5_typography',
    'section' => 'general',
    'transport' => 'auto',
    'default' => array(
        'font-size' => '18px',
        'line-height' => '1.5',
        'letter-spacing' => '0',
        'text-transform' => 'none',
    ),
    'output' => array(
        array(
            'element' => 'h5',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'general_setting',
            'operator' => '==',
            'value' => 'style',
        ),
        array(
            'setting' => 'headings_setting',
            'operator' => '==',
            'value' => 'h5',
        ),
    ),
) );

// Heading H6
Kirki::add_field( 'alexandrie', array(
    'type' => 'typography',
    'settings' => 'h6_typography',
    'section' => 'general',
    'transport' => 'auto',
    'default' => array(
        'font-size' => '15px',
        'line-height' => '1.5',
        'letter-spacing' => '0',
        'text-transform' => 'none',
    ),
    'output' => array(
        array(
            'element' => 'h6',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'general_setting',
            'operator' => '==',
            'value' => 'style',
        ),
        array(
            'setting' => 'headings_setting',
            'operator' => '==',
            'value' => 'h6',
        ),
    ),
) );

// General backgrounds Styles
alexandrie_headlines( 'background_styles', 'general', 'general_setting', 'style', 'Backgrounds', '35px' );

// Body BG Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'body_bg',
    'label' => __( 'Body', 'alexandrie' ),
    'section' => 'general',
    'transport' => 'postMessage',
    'default' => '#ffffff',
    'choices' => array(
        'alpha' => true,
    ),
    'output' => array(
        array(
            'element' => 'body',
            'property' => 'background-color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => 'body',
            'function' => 'css',
            'property' => 'background-color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'general_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Container BG Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'container_bg',
    'label' => __( 'Content', 'alexandrie' ),
    'section' => 'general',
    'transport' => 'postMessage',
    'default' => 'transparent',
    'choices' => array(
        'alpha' => true,
    ),
    'output' => array(
        array(
            'element' => '.site-content',
            'property' => 'background-color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.site-content',
            'function' => 'css',
            'property' => 'background-color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'general_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );


/* General advance Fields */
Kirki::add_field( 'alexandrie', array(
    'settings' => 'content_spacing',
    'section' => 'general',
    'label' => esc_html__( 'Container Padding', 'alexandrie' ),
    'type' => 'spacing',
    'default' => array(
        'top' => '40px',
        'right' => '15px',
        'bottom' => '40px',
        'left' => '15px',
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.alexandrie-content',
            'property' => 'padding',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'general_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
    ),
) );
