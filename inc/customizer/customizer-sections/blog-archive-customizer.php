<?php

/* * **************************************************
 * Blog And Archive Section
 * ************************************************** */

// Menu Tabs
alexandrie_tabs( 'blog_archive_setting', 'blog_archive' );

alexandrie_shortcuts( 'blog_shortcut', 'blog_archive', 'partial_blog', '#content .alexandrie_blog' );

// Blog / Archive Options
alexandrie_headlines( 'blog_archive_options', 'blog_archive', 'blog_archive_setting', 'general', 'Blog / Archive', '10px' );

Kirki::add_field( 'alexandrie', array(
    'type' => 'number',
    'settings' => 'blog_posts_col_num',
    'label' => esc_html__( 'Number of columns', 'alexandrie' ),
    'section' => 'blog_archive',
    'default' => 1,
    'choices' => array(
        'min' => 1,
        'max' => 4,
        'step' => 1,
    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );

// Blog / Archive Layout
Kirki::add_field( 'alexandrie', array(
    'type' => 'sortable',
    'settings' => 'blog_posts_layout',
    'label' => __( 'Blog Posts Layout', 'alexandrie' ),
    'section' => 'blog_archive',
    'default' => array(
        'thumbnail',
        'title',
        'meta',
        'content',
    ),
    'choices' => array(
        'thumbnail' => __( 'Featured Image', 'alexandrie' ),
        'title' => __( 'Title', 'alexandrie' ),
        'meta' => __( 'Meta', 'alexandrie' ),
        'content' => __( 'Content', 'alexandrie' ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );
kirki::add_field( 'alexandrie', array(
    'type' => 'toggle',
    'settings' => 'blog_archive_show_cat_before_title',
    'label' => __( 'show Category before title ', 'alexandrie' ),
    'section' => 'blog_archive',
    'default' => '0',
//	'priority'    => 10,
    'choices' => array(
        'on' => esc_attr__( 'Show', 'alexandrie' ),
        'off' => esc_attr__( 'Hide', 'alexandrie' ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );
// Blog / Archive Meta Layout
Kirki::add_field( 'alexandrie', array(
    'type' => 'sortable',
    'settings' => 'blog_posts_meta_layout',
    'label' => __( 'Blog Posts Meta Layout', 'alexandrie' ),
    'section' => 'blog_archive',
    'default' => array(
        'author',
        'date',
//        'category',        
        'comments'
    ),
    'choices' => array(
        'author' => __( 'Author', 'alexandrie' ),
        'date' => __( 'Date', 'alexandrie' ),
        'category' => __( 'Category', 'alexandrie' ),
        'comments' => __( 'Comments', 'alexandrie' ),
        'tags' => __( 'Tags', 'alexandrie' ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );

// Link Post Titles
Kirki::add_field( 'alexandrie', array(
    'type' => 'toggle',
    'settings' => 'blog_archive_title_linkable',
    'label' => __( 'Link Title', 'alexandrie' ),
    'section' => 'blog_archive',
    'default' => '1',
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );

// Link Post Thumbnails
Kirki::add_field( 'alexandrie', array(
    'type' => 'toggle',
    'settings' => 'blog_archive_thumbnail_linkable',
    'label' => __( 'Link Thumbnail', 'alexandrie' ),
    'section' => 'blog_archive',
    'default' => '1',
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'type' => 'toggle',
    'settings' => 'blog_archive_show_athour_avatar',
    'label' => __( 'Show author Avatar', 'alexandrie' ),
    'section' => 'blog_archive',
    'default' => '1',
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );

// Blog / Archive / Single Meta seprator
Kirki::add_field( 'alexandrie', array(
    'type' => 'radio-buttonset',
    'settings' => 'blog_archive_single_meta_sep',
    'label' => __( 'Meta Separator Icon', 'alexandrie' ),
    'section' => 'blog_archive',
    'transport' => 'auto',
    'default' => '/',
    'suffix' => '"',
    'choices' => array(
        '/' => esc_attr__( '/', 'alexandrie' ),
        '\2013' => esc_attr__( '-', 'alexandrie' ),
        '|' => esc_attr__( '|', 'alexandrie' ),
        '\0020' => esc_attr__( 'space', 'alexandrie' ),
    ),
    'output' => array(
        array(
            'element' => '.blog_and_archive  .entry-meta span::after',
            'property' => 'content',
            'prefix' => '"',
            'suffix' => '"',
        ),
        array(
            'element' => '.blog_and_archive  .entry-meta span:last-of-type::after',
            'property' => 'content',
            'value_pattern' => 'none',
//            'prefix' => '"',
//            'suffix' => '"',
        ),
        array(
            'element' => '.blog_and_archive  .entry-meta span::after',
            'property' => 'margin-right',
            'value_pattern' => '5',
            'units' => 'px',
        ),
        array(
            'element' => '.blog_and_archive  .entry-meta span::after',
            'property' => 'margin-left',
            'value_pattern' => '5',
            'units' => 'px',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.blog_and_archive  .entry-meta span::after',
            'function' => 'css',
            'property' => 'content',
            'prefix' => '"',
            'suffix' => '"',
        ),
        array(
            'element' => '.blog_and_archive  .entry-meta span:last-of-type::after',
            'function' => 'css',
            'property' => 'content',
            'value_pattern' => 'none',
//            'prefix' => '"',
//            'suffix' => '"',
        ),
        array(
            'element' => '.blog_and_archive  .entry-meta span::after',
            'function' => 'css',
            'property' => 'margin-right',
            'value_pattern' => '5',
            'units' => 'px',
        ),
        array(
            'element' => '.blog_and_archive  .entry-meta span::after',
            'function' => 'css',
            'property' => 'margin-left',
            'value_pattern' => '5',
            'units' => 'px',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );

// Blog / Archive Excerpt / Content
Kirki::add_field( 'alexandrie', array(
    'type' => 'select',
    'settings' => 'blog_archive_content_excerpt',
    'label' => __( 'Content / Excerpt', 'alexandrie' ),
    'section' => 'blog_archive',
    'default' => 'blog_archive_excerpt',
    'multiple' => 1,
    'choices' => array(
        'blog_archive_excerpt' => esc_attr__( 'Excerpt', 'alexandrie' ),
        'blog_archive_content' => esc_attr__( 'Content', 'alexandrie' ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );

// Blog / Archive Excerpt Length
Kirki::add_field( 'alexandrie', array(
    'type' => 'number',
    'settings' => 'blog_archive_excerpt_length',
    'label' => esc_attr__( 'Excerpt Length', 'alexandrie' ),
    'section' => 'blog_archive',
    'default' => 45,
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_content_excerpt',
            'operator' => '==',
            'value' => 'blog_archive_excerpt',
        ),
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );

// Blog / Archive Button Headline
Kirki::add_field( 'alexandrie', array(
    'type' => 'custom',
    'settings' => 'blog_button_headline',
    'section' => 'blog_archive',
    'default' => '<div style="border-bottom: 1px solid #d5d8dc;line-height: 2em;font-size: 20px;text-transform: capitalize; font-weight: 600;">' . esc_html__( 'Read More', 'alexandrie' ) . '</div>',
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_content_excerpt',
            'operator' => '==',
            'value' => 'blog_archive_excerpt',
        ),
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );

// Blog Button Text
Kirki::add_field( 'alexandrie', array(
    'type' => 'text',
    'settings' => 'blog_btn_text',
    'label' => __( 'Button Text', 'alexandrie' ),
    'section' => 'blog_archive',
    'transport' => 'auto',
    'default' => __( 'Read More', 'alexandrie' ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_content_excerpt',
            'operator' => '==',
            'value' => 'blog_archive_excerpt',
        ),
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );

// Button Position
Kirki::add_field( 'alexandrie', array(
    'type' => 'radio-buttonset',
    'settings' => 'blog_archive_button_position',
    'label' => __( 'Button Position', 'alexandrie' ),
    'section' => 'blog_archive',
    'transport' => 'auto',
    'default' => 'btn_left',
    'choices' => array(
        'btn_left' => esc_attr__( 'Left', 'alexandrie' ),
        'btn_right' => esc_attr__( 'Right', 'alexandrie' ),
        'btn_full' => esc_attr__( 'Full Width', 'alexandrie' ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_content_excerpt',
            'operator' => '==',
            'value' => 'blog_archive_excerpt',
        ),
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );

// Button Icon Show / Hide
Kirki::add_field( 'alexandrie', array(
    'type' => 'toggle',
    'settings' => 'blog_archive_button_icon_toggle',
    'label' => __( 'Use Button Icon', 'alexandrie' ),
    'section' => 'blog_archive',
    'default' => '1',
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_content_excerpt',
            'operator' => '==',
            'value' => 'blog_archive_excerpt',
        ),
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );

// Button Icon Left / Right
Kirki::add_field( 'alexandrie', array(
    'type' => 'radio-buttonset',
    'settings' => 'blog_archive_button_icon_position',
    'label' => __( 'Button Icon Position', 'alexandrie' ),
    'section' => 'blog_archive',
    'transport' => 'auto',
    'default' => 'icon_right',
    'choices' => array(
        'icon_left' => esc_attr__( 'Icon Left', 'alexandrie' ),
        'icon_right' => esc_attr__( 'Icon Right', 'alexandrie' ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_button_icon_toggle',
            'operator' => '==',
            'value' => '1',
        ),
        array(
            'setting' => 'blog_archive_content_excerpt',
            'operator' => '==',
            'value' => 'blog_archive_excerpt',
        ),
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );

// Button Icon Left
Kirki::add_field( 'alexandrie', array(
    'type' => 'radio-buttonset',
    'settings' => 'blog_archive_button_left_icon',
    'label' => __( 'Left Icon', 'alexandrie' ),
    'section' => 'blog_archive',
    'transport' => 'auto',
    'default' => '&#171;',
    'choices' => array(
        '&#171;' => esc_attr__( '«', 'alexandrie' ),
        '&#8249;' => esc_attr__( '‹', 'alexandrie' ),
        '&#8592;' => esc_attr__( '←', 'alexandrie' ),
        '&#9668;' => esc_attr__( '◄', 'alexandrie' ),
        '' => esc_attr__( 'none', 'alexandrie' ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_button_icon_position',
            'operator' => '==',
            'value' => 'icon_left',
        ),
        array(
            'setting' => 'blog_archive_button_icon_toggle',
            'operator' => '==',
            'value' => '1',
        ),
        array(
            'setting' => 'blog_archive_content_excerpt',
            'operator' => '==',
            'value' => 'blog_archive_excerpt',
        ),
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );

// Button Icon Right
Kirki::add_field( 'alexandrie', array(
    'type' => 'radio-buttonset',
    'settings' => 'blog_archive_button_right_icon',
    'label' => __( 'Right Icon', 'alexandrie' ),
    'section' => 'blog_archive',
    'transport' => 'auto',
    'default' => '&#187;',
    'choices' => array(
        '&#187;' => esc_attr__( '»', 'alexandrie' ),
        '&#8250;' => esc_attr__( '›', 'alexandrie' ),
        '&#8594;' => esc_attr__( '→', 'alexandrie' ),
        '&#9658;' => esc_attr__( '►', 'alexandrie' ),
        '' => esc_attr__( 'none', 'alexandrie' ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_button_icon_position',
            'operator' => '==',
            'value' => 'icon_right',
        ),
        array(
            'setting' => 'blog_archive_button_icon_toggle',
            'operator' => '==',
            'value' => '1',
        ),
        array(
            'setting' => 'blog_archive_content_excerpt',
            'operator' => '==',
            'value' => 'blog_archive_excerpt',
        ),
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );

// Blog / Archive Post Navigation
Kirki::add_field( 'alexandrie', array(
    'type' => 'custom',
    'settings' => 'blog_post_nav',
    'section' => 'blog_archive',
    'default' => '<div style="border-bottom: 1px solid #d5d8dc;line-height: 2em;font-size: 20px;text-transform: capitalize; font-weight: 600;">' . esc_html__( 'Posts Navigation', 'alexandrie' ) . '</div>',
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );

// Older Posts Text
Kirki::add_field( 'alexandrie', array(
    'type' => 'text',
    'settings' => 'blog_old_nav',
    'label' => __( 'Older Posts Text', 'alexandrie' ),
    'section' => 'blog_archive',
    'default' => __( 'Older posts', 'alexandrie' ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );

// Newer Posts Text
Kirki::add_field( 'alexandrie', array(
    'type' => 'text',
    'settings' => 'blog_newer_nav',
    'label' => __( 'Newer Posts Text', 'alexandrie' ),
    'section' => 'blog_archive',
    'default' => __( 'Newer posts', 'alexandrie' ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );


alexandrie_headlines( 'Blog_title_styles', 'blog_archive', 'blog_archive_setting', 'style', 'Title', '35px' );


// Blog Archive Title
Kirki::add_field( 'alexandrie', array(
    'type' => 'typography',
    'settings' => 'blog_archive_title',
    'label' => __( 'Post Title', 'alexandrie' ),
    'section' => 'blog_archive',
    'transport' => 'auto',
    'default' => array(
        'font-family' => 'Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif',
        'font-size' => '48px',
        'line-height' => '1',
        'letter-spacing' => '0',
        'text-transform' => 'none',
        'text-align' => 'left',
        'variant' => 'regular',
        'color' => '#000',
    ),
    'output' => array(
        array(
            'element' => '.alexandrie_blog.blog_and_archive .entry-title,.alexandrie_blog.blog_and_archive .entry-title a,.alexandriepost-list-sc .entry-title a,.ab-block-post-grid h2',
        ),
    ),
//    'js_vars' => array(
//        array(
//            'element' => '.alexandrie_blog.blog_and_archive .entry-title, .alexandrie_blog.blog_and_archive .entry-title a,.alexandriepost-list-sc .entry-title a,.ab-block-post-grid h2',
//            'function' => 'css',
//        ),
//    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'type' => 'typography',
    'settings' => 'category_before_title',
    'label' => __( 'Post Category', 'alexandrie' ),
    'section' => 'blog_archive',
    'transport' => 'auto',
    'default' => array(
        'font-family' => 'Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif',
        'font-size' => '18px',
        'line-height' => '1',
        'letter-spacing' => '0',
        'text-transform' => 'none',
        'text-align' => 'left',
        'variant' => 'regular',
        'color' => '#939393',
    ),
    'output' => array(
        array(
            'element' => '.alexandrie_blog.blog_and_archive .post-category a,.alexandrie_blog.blog_and_archive .post-category',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.alexandrie_blog.blog_and_archive .post-category a,.alexandrie_blog.blog_and_archive .post-category',
            'function' => 'css',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'style',
        ),
        array(
            'setting' => 'blog_archive_show_cat_before_title',
            'operator' => '==',
            'value' => '1',
        ),
    ),
) );


Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'blog_title_hover_color',
    'label' => __( 'Post Title Hover color', 'alexandrie' ),
    'section' => 'blog_archive',
    'transport' => 'auto',
    'default' => '#ddd',
    'output' => array(
        array(
            'element' => '.alexandrie_blog.blog_and_archive  .entry-title a:hover,.alexandriepost-list-sc .entry-title a:hover,.ab-block-post-grid h2 a:hover',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.alexandrie_blog.blog_and_archive  .entry-title a:hover,.alexandriepost-list-sc .entry-title a:hover,.ab-block-post-grid h2 a:hover',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'style',
        ),
        array(
            'setting' => 'blog_archive_title_linkable',
            'operator' => '==',
            'value' => '1',
        ),
    ),
) );

// Blog Meta Styles
alexandrie_headlines( 'Blog_meta_styles', 'blog_archive', 'blog_archive_setting', 'style', 'Meta', '35px' );

//Blog Meta bg color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'blog_meta_bg_color',
    'label' => __( 'Meta Background', 'alexandrie' ),
    'section' => 'blog_archive',
    'transport' => 'auto',
    'default' => '#fff',
    'choices' => [
        'alpha' => true
    ],
    'output' => array(
        array(
            'element' => '.alexandrie_blog.blog_and_archive  .entry-meta',
            'property' => 'background-color',
        ),
    ),
//    'js_vars' => array(
//        array(
//            'element' => '.alexandrie_blog.blog_and_archive  .entry-meta',
//            'function' => 'css',
//            'property' => 'background-color',
//        ),
//    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Blog Meta Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'blog_meta_color',
    'label' => __( 'Meta', 'alexandrie' ),
    'section' => 'blog_archive',
    'transport' => 'postMessage',
    'default' => '#848484',
    'output' => array(
        array(
            'element' => '.alexandrie_blog.blog_and_archive  .entry-meta, .entry-meta span::after',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.alexandrie_blog.blog_and_archive  .entry-meta, .entry-meta span::after',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Blog Meta Link Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'blog_meta_link_color',
    'label' => __( 'Meta Link', 'alexandrie' ),
    'section' => 'blog_archive',
    'transport' => 'postMessage',
    'default' => '#848484',
    'output' => array(
        array(
            'element' => '.alexandrie_blog.blog_and_archive  .entry-meta a',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.alexandrie_blog.blog_and_archive  .entry-meta a',
            'function' => 'style',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Blog Meta Link Hover Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'blog_meta_hover_color',
    'label' => __( 'Meta Hover', 'alexandrie' ),
    'section' => 'blog_archive',
    'transport' => 'postMessage',
    'default' => '#000000',
    'output' => array(
        array(
            'element' => '.alexandrie_blog.blog_and_archive  .entry-meta a:hover',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.alexandrie_blog.blog_and_archive  .entry-meta a:hover',
            'function' => 'style',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Blog Meta
Kirki::add_field( 'alexandrie', array(
    'type' => 'typography',
    'label' => __( 'Meta Typography', 'alexandrie' ),
    'settings' => 'blog_meta',
    'section' => 'blog_archive',
    'transport' => 'auto',
    'default' => array(
        'font-family' => '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif',
        'font-size' => '16px',
        'line-height' => '1',
        'letter-spacing' => '0',
        'text-align' => 'left',
        'text-transform' => 'none',
    ),
    'output' => array(
        array(
            'element' => '.alexandrie_blog.blog_and_archive  .entry-meta',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );


// Blog Content Styles
alexandrie_headlines( 'Blog_content_styles', 'blog_archive', 'blog_archive_setting', 'style', 'Content', '35px' );

// Blog Content Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'blog_content_color',
    'label' => __( 'Text', 'alexandrie' ),
    'section' => 'blog_archive',
    'transport' => 'auto',
    'default' => '#000000',
    'output' => array(
        array(
            'element' => '.alexandrie_blog.blog_and_archive  .entry-content',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.alexandrie_blog.blog_and_archive  .entry-content',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Blog Content
Kirki::add_field( 'alexandrie', array(
    'type' => 'typography',
    'settings' => 'blog_content',
    'section' => 'blog_archive',
    'transport' => 'auto',
    'default' => array(
        'font-family' => '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif',
        'font-size' => '17px',
        'line-height' => '1.5',
        'letter-spacing' => '0',
        'text-transform' => 'none',
    ),
    'output' => array(
        array(
            'element' => '.alexandrie_blog.blog_and_archive  .entry-content',
        ),
    ),
//    'js_vars' => array(
//        array(
//            'element' => '.alexandrie_blog.blog_and_archive  .entry-content',
//            'function' => 'css',
//        ),
//    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );


// Blog Button Styles
alexandrie_headlines( 'Blog_btn_styles', 'blog_archive', 'blog_archive_setting', 'style', 'Button', '35px' );

// Blog Button BG Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'blog_btn_bg_color',
    'label' => __( 'Background', 'alexandrie' ),
    'section' => 'blog_archive',
    'transport' => 'auto',
    'default' => '#000',
    'choices' => [
        'alpha' => true
    ],
    'output' => array(
        array(
            'element' => '.alexandrie_blog.blog_and_archive  .entry-content .read-more',
            'property' => 'background-color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.alexandrie_blog.blog_and_archive  .entry-content .read-more',
            'function' => 'css',
            'property' => 'background-color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Blog Button Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'blog_btn_color',
    'label' => __( 'Text', 'alexandrie' ),
    'section' => 'blog_archive',
    'transport' => 'auto',
    'default' => '#fff',
    'output' => array(
        array(
            'element' => '.alexandrie_blog.blog_and_archive  .entry-content .read-more',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.alexandrie_blog.blog_and_archive  .entry-content .read-more',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Blog Button Hover BG Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'blog_btn_hover_bg_color',
    'label' => __( 'Hover Background', 'alexandrie' ),
    'section' => 'blog_archive',
    'transport' => 'auto',
    'default' => '#fff',
    'choices' => [
        'alpha' => true
    ],
    'output' => array(
        array(
            'element' => '.alexandrie_blog.blog_and_archive  .entry-content .read-more:hover',
            'property' => 'background-color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.alexandrie_blog.blog_and_archive  .entry-content .read-more:hover',
            'function' => 'css',
            'property' => 'background-color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Blog Button Hover Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'blog_btn_hover_color',
    'label' => __( 'Hover Text', 'alexandrie' ),
    'section' => 'blog_archive',
    'transport' => 'auto',
    'default' => '#000000',
    'output' => array(
        array(
            'element' => '.alexandrie_blog.blog_and_archive  .entry-content .read-more:hover',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.alexandrie_blog.blog_and_archive  .entry-content .read-more:hover',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Blog Button Typography
Kirki::add_field( 'alexandrie', array(
    'type' => 'typography',
    'settings' => 'blog_btn_typography',
    'section' => 'blog_archive',
    'transport' => 'auto',
    'default' => array(
        'font-family' => '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif',
        'font-size' => '16px',
        'line-height' => '1.5',
        'letter-spacing' => '0',
        'text-transform' => 'none',
    ),
    'output' => array(
        array(
            'element' => '.alexandrie_blog.blog_and_archive  .entry-content .read-more',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );
// Index page ,Archive Navigation Styles
alexandrie_headlines( 'Blog_archive_posts_navigation', 'blog_archive', 'blog_archive_setting', 'style', 'Blog Navigation', '35px' );

// Blog Navigation Link Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'blog_archive_nav_link_color',
    'label' => __( 'Link color', 'alexandrie' ),
    'section' => 'blog_archive',
    'transport' => 'auto',
    'default' => '#fff',
    'output' => array(
        array(
            'element' => '.navigation.posts-navigation .nav-links a,.pagination a',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.navigation.posts-navigation .nav-links a,.pagination a',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'blog_archive_nav_link_bg_color',
    'label' => __( 'Link Background color', 'alexandrie' ),
    'section' => 'blog_archive',
    'transport' => 'auto',
    'default' => '#000',
    'output' => array(
        array(
            'element' => '.navigation.posts-navigation .nav-links a,.pagination a',
            'property' => 'background-color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.navigation.posts-navigation .nav-links a,.pagination a',
            'function' => 'css',
            'property' => 'background-color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Blog Navigation Link Hover Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'blog_archive_nav_link_hover_color',
    'label' => __( 'Link Hover', 'alexandrie' ),
    'section' => 'blog_archive',
    'transport' => 'auto',
    'default' => '#fff',
    'output' => array(
        array(
            'element' => '.navigation.posts-navigation .nav-links a:hover,.pagination a:hover',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.navigation.posts-navigation .nav-links a:hover,.pagination a:hover',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );
// Blog Navigation Link Hover Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'blog_archive_nav_link_hover_bg_color',
    'label' => __( 'Link Hover Background', 'alexandrie' ),
    'section' => 'blog_archive',
    'transport' => 'auto',
    'default' => '#000000',
    'output' => array(
        array(
            'element' => '.navigation.posts-navigation .nav-links a:hover ,.pagination a:hover',
            'property' => 'background-color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.navigation.posts-navigation .nav-links a:hover ,.pagination a:hover',
            'function' => 'css',
            'property' => 'background-color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Blog Posts / Single post Nav Typography
Kirki::add_field( 'alexandrie', array(
    'type' => 'typography',
    'settings' => 'blog_archive_nav_typography',
    'section' => 'blog_archive',
    'transport' => 'auto',
    'default' => array(
        'font-family' => '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif',
        'variant' => 'regular',
        'font-size' => '16px',
        'line-height' => '1.5',
        'letter-spacing' => '0',
        'text-transform' => 'none',
    ),
    'output' => array(
        array(
            'element' => '.navigation.posts-navigation .nav-links a,.pagination a',
        ),
    ),
//    'js_vars' => array(
//        array(
//            'element' => '.navigation.posts-navigation .nav-links a',
//            'function' => 'css',
//        ),
//    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );



/* * ********* Advanced Fields ********** */
alexandrie_headlines( 'Blog_advanced_styles', 'blog_archive', 'blog_archive_setting', 'advanced', 'Psot Advanced Style', '35px' );
// Blog / Archive Posts Padding
Kirki::add_field( 'alexandrie', array(
    'settings' => 'blog_archive_spacing',
    'section' => 'blog_archive',
    'label' => esc_html__( 'Blog Posts Padding', 'alexandrie' ),
    'type' => 'spacing',
    'default' => array(
        'top' => '40px',
        'right' => '0',
        'bottom' => '0',
        'left' => '0',
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.blog_and_archive',
            'property' => 'padding',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.blog_and_archive',
            'function' => 'css',
            'property' => 'padding',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'settings' => 'blog_archive_margin',
    'section' => 'blog_archive',
    'label' => esc_html__( 'Blog Posts margin', 'alexandrie' ),
    'type' => 'spacing',
    'default' => array(
        'top' => '0',
        'right' => '0',
        'bottom' => '0',
        'left' => '0',
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.blog_and_archive',
            'property' => 'margin',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.blog_and_archive',
            'function' => 'css',
            'property' => 'margin',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
    ),
) );

Kirki::add_field( 'alexandrie', array(
    'type' => 'select',
    'settings' => 'blog_archive_post_shadow',
    'label' => __( 'Post Shadow', 'alexandrie' ),
    'section' => 'blog_archive',
    'default' => 'none',
    'priority' => 10,
    'multiple' => 1,
    'choices' => array(
        'shadow-none' => esc_attr__( 'None', 'alexandrie' ),
        'shadow-sm' => esc_attr__( 'Small Shadow', 'alexandrie' ),
        'shadow' => esc_attr__( 'Shadow', 'alexandrie' ),
        'shadow-lg' => esc_attr__( 'large Shadow', 'alexandrie' ),
        'shadow-custom' => esc_attr__( '3D shadow', 'alexandrie' ),
    ),
    'output' => array(
        array(
            'element' => '.blog_and_archive',
            'property' => 'box-shadow',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
    ),
) );

Kirki::add_field( 'alexandrie', array(
    'settings' => 'blog_archive_thumb_border_radius',
    'section' => 'blog_archive',
    'label' => esc_html__( 'Post Thumbnail Border radius', 'alexandrie' ),
    'type' => 'dimensions',
    'default' => [
        'top-left-radius' => '0px',
        'top-right-radius' => '0px',
        'bottom-left-radius' => '0px',
        'bottom-right-radius' => '0px',
    ],
    'choices' => [
        'top-left-radius' => esc_attr__( 'Top Left', 'alexandrie' ),
        'top-right-radius' => esc_attr__( 'Top Right', 'alexandrie' ),
        'bottom-left-radius' => esc_attr__( 'Bottom Left', 'alexandrie' ),
        'bottom-right-radius' => esc_attr__( 'Bottom Right', 'alexandrie' ),
    ],
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.blog_and_archive .post-image img',
            'property' => 'border',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.blog_and_archive .post-image img',
            'function' => 'style',
            'property' => 'border',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
    ),
) );

// Blog / Archive Posts border
Kirki::add_field( 'alexandrie', array(
    'settings' => 'blog_archive_border_radius',
    'section' => 'blog_archive',
    'label' => esc_html__( 'Blog Posts Border radius', 'alexandrie' ),
    'type' => 'dimensions',
    'default' => array(
        'top-left-radius' => '1rem',
        'top-right-radius' => '1rem',
        'bottom-right-radius' => '1rem',
        'bottom-left-radius' => '1rem',
    ),
    'choices' => array(
        'top-left-radius' => esc_attr__( 'Top-Left radius', 'alexandrie' ),
        'top-right-radius' => esc_attr__( 'Top-Right-radius:', 'alexandrie' ),
        'bottom-right-radius' => esc_attr__( 'Bottom-Right-radius', 'alexandrie' ),
        'bottom-left-radius' => esc_attr__( 'Bottom-Left radius', 'alexandrie' ),
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.blog_and_archive',
            'property' => 'border',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.blog_and_archive',
            'function' => 'css',
            'property' => 'border',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
    ),
) );

Kirki::add_field( 'alexandrie', array(
    'type' => 'radio-buttonset',
    'settings' => 'blog_archive_border_style',
    'label' => __( 'Border Style', 'alexandrie' ),
    'section' => 'blog_archive',
    'default' => 'solid',
    'priority' => 10,
    'multiple' => 1,
    'choices' => array(
        'none' => esc_attr__( 'None', 'alexandrie' ),
        'solid' => esc_attr__( 'solid', 'alexandrie' ),
        'dotted' => esc_attr__( 'dotted', 'alexandrie' ),
        'dashed' => esc_attr__( 'dashed', 'alexandrie' ),
    ),
    'output' => array(
        array(
            'element' => '.blog_and_archive',
            'property' => 'border-style',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
) ) );

Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'blog_archive_border_color',
    'label' => esc_attr__( 'Border color', 'alexandrie' ),
    'section' => 'blog_archive',
    'priority' => 10,
    'default' => '#ccc',
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.blog_and_archive',
            'property' => 'border-color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.blog_and_archive',
            'function' => 'css',
            'property' => 'border-color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
        array(
            'setting' => 'blog_archive_border_style',
            'value' => 'none',
            'operator' => '!='
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'settings' => 'blog_archive_border',
    'section' => 'blog_archive',
    'label' => esc_html__( 'Blog Posts Border Thickness', 'alexandrie' ),
    'type' => 'dimensions',
    'default' => array(
        'top-width' => '0px',
        'right-width' => '0px',
        'bottom-width' => '0px',
        'left-width' => '0px',
    ),
    'choices' => array(
        'top-width' => esc_attr__( 'Top', 'alexandrie' ),
        'right-width' => esc_attr__( 'Right', 'alexandrie' ),
        'bottom-width' => esc_attr__( 'Bottom', 'alexandrie' ),
        'left-width' => esc_attr__( 'Left', 'alexandrie' ),
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.blog_and_archive',
            'property' => 'border',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.blog_and_archive',
            'function' => 'css',
            'property' => 'border',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
        array(
            'setting' => 'blog_archive_border_style',
            'value' => 'none',
            'operator' => '!='
        ),
    ),
) );
//Kirki::add_field( 'alexandrie', array(
//    'type' => 'color',
//    'settings' => 'border_color',
//    'label' => esc_attr__( 'Border color', 'alexandrie' ),
//    'section' => 'blog_archive',
//    'priority' => 10,
//    'default' => '#ccc',
//    'transport' => 'auto',
//    'output' => array(
//        array(
//            'element' => '.blog_and_archive',
//            'property' => 'border-color',
//        ),
//    ),
//    'active_callback' => array(
//        array(
//            'setting' => 'blog_archive_setting',
//            'operator' => '==',
//            'value' => 'advanced',
//        ),
//    ),
//) );
//alexandrie_headlines( 'Blog_Meta_advanced_styles', 'blog_archive', 'blog_archive_setting', 'advanced', 'Psot Meta Advanced Style', '35px' );
//Kirki::add_field( 'alexandrie', array(
//    'type' => 'select',
//    'settings' => 'blog_archive_meta_shadow',
//    'label' => __( 'Post Shadow', 'alexandrie' ),
//    'section' => 'blog_archive',
//    'default' => 'none',
//    'priority' => 10,
//    'multiple' => 1,
//    'choices' => array(
//        'shadow-none' => esc_attr__( 'None', 'alexandrie' ),
//        'shadow-sm' => esc_attr__( 'Small Shadow', 'alexandrie' ),
//        'shadow' => esc_attr__( 'Shadow', 'alexandrie' ),
//        'shadow-lg' => esc_attr__( 'large Shadow', 'alexandrie' ),
//        'shadow-custom' => esc_attr__( '3D shadow', 'alexandrie' ),
//    ),
//    'active_callback' => array(
//        array(
//            'setting' => 'blog_archive_setting',
//            'operator' => '==',
//            'value' => 'advanced',
//        ),
//    ),
//) );
//Button spacing 
alexandrie_headlines( 'read_more_advanced_styles', 'blog_archive', 'blog_archive_setting', 'advanced', 'Read More Advanced Style', '35px' );
Kirki::add_field( 'alexandrie', array(
    'settings' => 'blog_archive_more_btn_padding',
    'section' => 'blog_archive',
    'label' => esc_html__( 'Read More Button Padding', 'alexandrie' ),
    'type' => 'spacing',
    'default' => array(
        'top' => '0',
        'right' => '0',
        'bottom' => '0',
        'left' => '0',
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.read-more',
            'property' => 'padding',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.read-more',
            'function' => 'css',
            'property' => 'padding',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'settings' => 'blog_archive_more_btn_margin',
    'section' => 'blog_archive',
    'label' => esc_html__( 'Read More Button Margin', 'alexandrie' ),
    'type' => 'spacing',
    'default' => array(
        'top' => '0',
        'right' => '0',
        'bottom' => '0',
        'left' => '0',
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.link.read-more',
            'property' => 'margin',
        ),
    ),
//    'js_vars' => array(
//        array(
//            'element' => '.read-more',
//            'function' => 'css',
//            'property' => 'margin',
//        ),
//    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
    ),
) );
kirki::add_field( 'alexandrie', array(
    'settings' => 'blog_archive_read_more_border_radius',
    'section' => 'blog_archive',
    'label' => esc_html__( 'Read More Border radius', 'alexandrie' ),
    'type' => 'dimensions',
    'default' => array(
        'top-left-radius' => '0',
        'top-right-radius' => '0',
        'bottom-left-radius' => '0',
        'bottom-right-radius' => '0',
    ),
    'choices' => array(
        'top-left-radius' => esc_attr__( 'Top-Left radius', 'alexandrie' ),
        'top-right-radius' => esc_attr__( 'Top-Right-radius:', 'alexandrie' ),
        'bottom-left-radius' => esc_attr__( 'Bottom-Left radius', 'alexandrie' ),
        'bottom-right-radius' => esc_attr__( 'Bottom-Right-radius', 'alexandrie' ),
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.read-more',
            'property' => 'border',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.read-more',
            'function' => 'css',
            'property' => 'border',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
    ),
) );
alexandrie_headlines( 'blog_archive_meta_advanced_styles', 'blog_archive', 'blog_archive_setting', 'advanced', 'Meta Advanced Style', '35px' );
// Blog / Archive meta border
Kirki::add_field( 'alexandrie', array(
    'settings' => 'blog_archive_meta_border_radius',
    'section' => 'blog_archive',
    'label' => esc_html__( 'Blog Meta Border radius', 'alexandrie' ),
    'type' => 'dimensions',
    'default' => array(
        'top-left-radius' => '0',
        'top-right-radius' => '0',
        'bottom-right-radius' => '0',
        'bottom-left-radius' => '0',
    ),
    'choices' => array(
        'top-left-radius' => esc_attr__( 'Top-Left radius', 'alexandrie' ),
        'top-right-radius' => esc_attr__( 'Top-Right-radius:', 'alexandrie' ),
        'bottom-right-radius' => esc_attr__( 'Bottom-Right-radius', 'alexandrie' ),
        'bottom-left-radius' => esc_attr__( 'Bottom-Left radius', 'alexandrie' ),
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.alexandrie_blog.blog_and_archive  .entry-meta',
            'property' => 'border',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.alexandrie_blog.blog_and_archive  .entry-meta',
            'function' => 'css',
            'property' => 'border',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
    ),
) );

Kirki::add_field( 'alexandrie', array(
    'type' => 'radio-buttonset',
    'settings' => 'blog_archive_meta_border_style',
    'label' => __( 'Meta Border Style', 'alexandrie' ),
    'section' => 'blog_archive',
    'default' => 'none',
//    'transport' => 'auto',
    'priority' => 10,
    'multiple' => 1,
    'choices' => array(
        'none' => esc_attr__( 'None', 'alexandrie' ),
        'solid' => esc_attr__( 'solid', 'alexandrie' ),
        'dotted' => esc_attr__( 'dotted', 'alexandrie' ),
        'dashed' => esc_attr__( 'dashed', 'alexandrie' ),
    ),
    'output' => array(
        array(
            'element' => '.alexandrie_blog.blog_and_archive  .entry-meta',
            'property' => 'border-style',
        ),
    ),
//    'js_vars' => array(
//        array(
//            'element' => '.alexandrie_blog.blog_and_archive  .entry-meta',
//            'function' => 'css',
//            'property' => 'border-style',
//        ),
//    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
) ) );

Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'blog_archive_meta_border_color',
    'label' => esc_attr__( 'Meta Border color', 'alexandrie' ),
    'section' => 'blog_archive',
//    'priority' => 10,
    'default' => '#ccc',
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.alexandrie_blog.blog_and_archive  .entry-meta',
            'property' => 'border-color',
        ),
    ),
//    'js_vars' => array(
//        array(
//            'element' => '.alexandrie_blog.blog_and_archive  .entry-meta',
//            'function' => 'css',
//            'property' => 'border-color',
//        ),
//    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
        array(
            'setting' => 'blog_archive_meta_border_style',
            'value' => 'none',
            'operator' => '!='
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'settings' => 'blog_archive_meta_border',
    'section' => 'blog_archive',
    'label' => esc_html__( 'Meta Border Thickness', 'alexandrie' ),
    'type' => 'dimensions',
    'default' => array(
        'top-width' => '0px',
        'right-width' => '0px',
        'bottom-width' => '0px',
        'left-width' => '0px',
    ),
    'choices' => array(
        'top-width' => esc_attr__( 'Top', 'alexandrie' ),
        'right-width' => esc_attr__( 'Right', 'alexandrie' ),
        'bottom-width' => esc_attr__( 'Bottom', 'alexandrie' ),
        'left-width' => esc_attr__( 'Left', 'alexandrie' ),
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.alexandrie_blog.blog_and_archive  .entry-meta',
            'property' => 'border',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.alexandrie_blog.blog_and_archive  .entry-meta',
            'function' => 'css',
            'property' => 'border',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
        array(
            'setting' => 'blog_archive_meta_border_style',
            'value' => 'none',
            'operator' => '!='
        ),
    ),
) );


alexandrie_headlines( 'blog_nav_advanced_styles', 'blog_archive', 'blog_archive_setting', 'advanced', 'Blog Nav Advanced Style', '35px' );
Kirki::add_field( 'alexandrie', array(
    'settings' => 'blog_archive_nav_border_radius',
    'section' => 'blog_archive',
    'label' => esc_html__( 'Blog Navigation Border radius', 'alexandrie' ),
    'type' => 'dimensions',
    'default' => array(
        'top-left-radius' => '1rem',
        'top-right-radius' => '1rem',
        'bottom-left-radius' => '1rem',
        'bottom-right-radius' => '1rem',
    ),
    'choices' => array(
        'top-left-radius' => esc_attr__( 'Top-Left radius', 'alexandrie' ),
        'top-right-radius' => esc_attr__( 'Top-Right-radius:', 'alexandrie' ),
        'bottom-left-radius' => esc_attr__( 'Bottom-Left radius', 'alexandrie' ),
        'bottom-right-radius' => esc_attr__( 'Bottom-Right-radius', 'alexandrie' ),
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.navigation.posts-navigation .nav-links a,.pagination a',
            'property' => 'border',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.navigation.posts-navigation .nav-links a,.pagination a',
            'function' => 'css',
            'property' => 'border',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'blog_archive_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
    ),
) );
