<?php

/* * **************************************************
 * Footer Section
 * ************************************************** */

// Footer Tabs
alexandrie_tabs( 'footer_setting', 'footer' );

alexandrie_shortcuts( 'footer_shortcut', 'footer', 'partial_footer', '.site-footer .footer-area .container' );

/* Footer general Fields */

Kirki::add_field( 'alexandrie', array(
    'type' => 'select',
    'settings' => 'global_footer_content_layout',
    'label' => __( 'Footer Layout', 'alexandrie' ),
    'section' => 'footer',
    'transport' => 'auto',
    'default' => 'container',
    'choices' => array(
        'container' => esc_attr__( 'Boxed', 'alexandrie' ),
        'container-fluid' => esc_attr__( 'Full Width', 'alexandrie' ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'footer_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );

// Footer Layout
Kirki::add_field( 'alexandrie', array(
    'type' => 'select',
    'settings' => 'footer_layout',
    'label' => __( 'Footer Widgets', 'alexandrie' ),
    'section' => 'footer',
    'transport' => 'auto',
    'default' => 'footer-no',
    'choices' => array(
        'footer-no' => esc_attr__( 'Disable', 'alexandrie' ),
        'footer-one' => esc_attr__( 'One', 'alexandrie' ),
        'footer-two' => esc_attr__( 'Two', 'alexandrie' ),
        'footer-three' => esc_attr__( 'Three', 'alexandrie' ),
        'footer-four' => esc_attr__( 'Four', 'alexandrie' ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'footer_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );

// Footer Gutter Spacing
Kirki::add_field( 'alexandrie', array(
    'type' => 'slider',
    'settings' => 'footer_gutter',
    'label' => __( 'Gutter Spacing', 'alexandrie' ),
    'section' => 'footer',
    'transport' => 'auto',
    'default' => 15,
    'choices' => array(
        'min' => '0',
        'max' => '100',
        'step' => '1',
    ),
    'active_callback' => array(
        array(
            'setting' => 'footer_layout',
            'operator' => '!=',
            'value' => 'footer-no',
        ),
        array(
            'setting' => 'footer_layout',
            'operator' => '!=',
            'value' => 'footer-one',
        ),
        array(
            'setting' => 'footer_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
    'output' => array(
        array(
            'element' => '.site-footer .footer-area .footer-two, .site-footer .footer-area .footer-three, .site-footer .footer-area .footer-four',
            'property' => 'grid-column-gap',
            'units' => 'px',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.site-footer .footer-area .footer-two, .site-footer .footer-area .footer-three, .site-footer .footer-area .footer-four',
            'function' => 'css',
            'property' => 'grid-column-gap',
            'units' => 'px',
        ),
    )
) );

// footer sitemap 
Kirki::add_field( 'alexandrie', array(
    'type' => 'toggle',
    'settings' => 'show_footer_social_menu',
    'label' => esc_attr__( 'show footer  social menu', 'alexandrie' ),
    'section' => 'footer',
    'default' => '1',
    'active_callback' => array(
        array(
            'setting' => 'footer_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );



/* Footer Style Fields */

// Footer Title Styles
alexandrie_headlines( 'footer_title_styles', 'footer', 'footer_setting', 'style', 'Widget Title', '10px' );

// Footer Title Color
//Kirki::add_field( 'alexandrie', array(
//    'type' => 'color',
//    'settings' => 'footer_title_color',
//    'label' => __( 'Title', 'alexandrie' ),
//    'section' => 'footer',
//    'transport' => 'auto',
//    'default' => '#000000',
//    'output' => array(
//        array(
//            'element' => '.site-footer .widget-title',
//            'property' => 'color',
//        ),
//    ),
//    'js_vars' => array(
//        array(
//            'element' => '.site-footer .widget-title',
//            'function' => 'css',
//            'property' => 'color',
//        ),
//    ),
//    'active_callback' => array(
//        array(
//            'setting' => 'footer_setting',
//            'operator' => '==',
//            'value' => 'style',
//        ),
//    ),
//) );

// Footer Widgets Titles
Kirki::add_field( 'alexandrie', array(
    'type' => 'typography',
    'settings' => 'widgets_title_typography',
    'section' => 'footer',
    'transport' => 'auto',
    'default' => array(
        'font-family' => '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif',
        'variant' => 'regular',
        'font-size' => '30px',
        'line-height' => '1.5',
        'letter-spacing' => '0',
        'text-transform' => 'none',
        'color'=>'#383838',
//        'text-align' => 'left',
    ),
    'output' => array(
        array(
            'element' => '.site-footer .widget-title',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.site-footer .widget-title',
            'function' => 'css',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'footer_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Footer Text Styles
alexandrie_headlines( 'footer_text_styles', 'footer', 'footer_setting', 'style', 'Widget Text', '35px' );

// Footer Text Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'footer_link_color',
    'label' => __( 'footer link color', 'alexandrie' ),
    'section' => 'footer',
    'transport' => 'auto',
    'default' => '#3a3a3a',
    'output' => array(
        array(
            'element' => '.site-footer .widget a,.site-footer a',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.site-footer .widget a,.site-footer a',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'footer_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'footer_link_hover_color',
    'label' => __( 'footer link hover color', 'alexandrie' ),
    'section' => 'footer',
    'transport' => 'auto',
    'default' => '#000',
    'output' => array(
        array(
            'element' => '.site-footer .widget a:hover,.site-footer a:hover',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.site-footer .widget a,.site-footer a',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'footer_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Footer Widgets Text 
Kirki::add_field( 'alexandrie', array(
    'type' => 'typography',
    'settings' => 'widgets_text_typography',
    'section' => 'footer',
    'transport' => 'auto',
    'default' => array(
        'font-family' => '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif',
        'variant' => 'regular',
        'font-size' => '17px',
        'line-height' => '1.5',
        'letter-spacing' => '0',
        'text-transform' => 'none',
        'text-align' => 'left',
        'color'=>'#3a3a3a',
    ),
    'output' => array(
        array(
            'element' => '.site-footer .widget',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'footer_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Footer Backgrounds Styles
alexandrie_headlines( 'footer_backgrounds_styles', 'footer', 'footer_setting', 'style', 'Backgrounds', '35px' );

// Footer BG Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'footer_bg',
    'label' => __( 'Footer', 'alexandrie' ),
    'section' => 'footer',
    'transport' => 'auto',
    'default' => '#fff',
    'choices' => array(
        'alpha' => true,
    ),
    'output' => array(
        array(
            'element' => '.site-footer .footer-area',
            'property' => 'background-color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.site-footer .footer-area',
            'function' => 'css',
            'property' => 'background-color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'footer_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );
// Footer Backgrounds Styles
alexandrie_headlines( 'footer_back_top_btn_styles', 'footer', 'footer_setting', 'style', 'Back to Top button', '35px' );

// back to top btn BG Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'b2top_btn_bg',
    'label' => __( 'Back to Top button background ', 'alexandrie' ),
    'section' => 'footer',
    'transport' => 'auto',
    'default' => '#000',
    'choices' => array(
        'alpha' => true,
    ),
    'output' => array(
        array(
            'element' => '.btn-2-top',
            'property' => 'background-color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.btn-2-top',
            'function' => 'css',
            'property' => 'background-color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'footer_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'b2top_btn_color',
    'label' => __( 'Back to Top button color ', 'alexandrie' ),
    'section' => 'footer',
    'transport' => 'auto',
    'default' => '#fff',
    'output' => array(
        array(
            'element' => '.btn-2-top',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.btn-2-top',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'footer_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'settings' => 'back2top_btn_border_radius',
    'section' => 'footer',
    'label' => esc_html__( 'Back to Top border radius', 'alexandrie' ),
    'type' => 'dimensions',
    'default' => array(
        'top-left-radius' => '2rem',
        'top-right-radius' => '2rem',
        'bottom-left-radius' => '2rem',
        'bottom-right-radius' => '2rem',
    ),
    'choices' => array(
        'top-left-radius' => esc_attr__( 'Top-Left radius', 'alexandrie' ),
        'top-right-radius' => esc_attr__( 'Top-Right-radius:', 'alexandrie' ),
        'bottom-left-radius' => esc_attr__( 'Bottom-Left radius', 'alexandrie' ),
        'bottom-right-radius' => esc_attr__( 'Bottom-Right-radius', 'alexandrie' ),
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.btn-2-top',
            'property' => 'border',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.btn-2-top',
            'function' => 'css',
            'property' => 'border',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'footer_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

/* Footer Advanced Fields */
alexandrie_headlines( 'footer_back_top_advanced_styles', 'footer', 'footer_setting', 'advanced', 'footer', '35px' );
// Footer Padding
Kirki::add_field( 'alexandrie', array(
    'settings' => 'footer_spacing_padding',
    'section' => 'footer',
    'label' => esc_html__( 'Footer Padding', 'alexandrie' ),
    'type' => 'spacing',
    'default' => array(
        'top' => '30px',
        'right' => '15px',
        'bottom' => '30px',
        'left' => '15px',
    ),
    'transport' => 'auto',
    'active_callback' => array(
        array(
            'setting' => 'footer_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
    ),
    'output' => array(
        array(
            'element' => '.site-footer .footer-area ',
            'property' => 'padding',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.site-footer .footer-area',
            'function' => 'css',
            'property' => 'padding',
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'type' => 'radio-buttonset',
    'settings' => 'footer_border_style',
    'label' => __( 'Footer Border Style', 'alexandrie' ),
    'section' => 'footer',
    'default' => 'none',
    'priority' => 10,
    'multiple' => 1,
    'choices' => array(
        'none' => esc_attr__( 'None', 'alexandrie' ),
        'solid' => esc_attr__( 'solid', 'alexandrie' ),
        'dotted' => esc_attr__( 'dotted', 'alexandrie' ),
        'dashed' => esc_attr__( 'dashed', 'alexandrie' ),
    ),
    'output' => array(
        array(
            'element' => '.site-footer',
            'property' => 'border-style',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'footer_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
) ) );

Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'footer_content_border_color',
    'label' => esc_attr__( 'Footer content Border color', 'alexandrie' ),
    'section' => 'footer',
    'priority' => 10,
    'default' => '#f8f9fa',
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.site-footer',
            'property' => 'border-color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.site-footer',
             'function'=>'css',
            'property' => 'border-color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'footer_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
        array(
            'setting' => 'footer_border_style',
            'value' => 'none',
            'operator' => '!='
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'settings' => 'footer_content_border_thickness',
    'section' => 'footer',
    'label' => esc_html__( 'Footer Content Border Thickness', 'alexandrie' ),
    'type' => 'dimensions',
    'default' => array(
        'top-width' => '0',
        'right-width' => '0',
        'bottom-width' => '0',
        'left-width' => '0',
    ),
    'choices' => array(
        'top-width' => esc_attr__( 'Top', 'alexandrie' ),
        'right-width' => esc_attr__( 'Right', 'alexandrie' ),
        'bottom-width' => esc_attr__( 'Bottom', 'alexandrie' ),
        'left-width' => esc_attr__( 'Left', 'alexandrie' ),
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.site-footer',
            'property' => 'border',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.site-footer',
            'function' => 'css',
            'property' => 'border',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'footer_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
        array(
            'setting' => 'footer_border_style',
            'value' => 'none',
            'operator' => '!='
        ),
    ),
) );



alexandrie_headlines( 'footer_social_menu_styles', 'footer', 'footer_setting', 'advanced', 'Social Menu', '35px' );

Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'footer_social_menu_bg',
    'label' => __( 'Social Menu background ', 'alexandrie' ),
    'section' => 'footer',
    'transport' => 'auto',
    'default' => '#fff',
    'choices' => array(
        'alpha' => true,
    ),
    'output' => array(
        array(
            'element' => '.footer-social-menu-wrapper',
            'property' => 'background-color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.footer-social-menu-wrapper',
            'function' => 'css',
            'property' => 'background-color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'footer_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'footer_social_menu_link_color',
    'label' => __( 'Social Menu Link ', 'alexandrie' ),
    'section' => 'footer',
    'transport' => 'auto',
    'default' => '#000',
    'output' => array(
        array(
            'element' => '.footer-social-menu-wrapper a',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.footer-social-menu-wrapper a',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'footer_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'footer_social_menu_link_hover',
    'label' => __( 'Social Menu Link hover ', 'alexandrie' ),
    'section' => 'footer',
    'transport' => 'auto',
    'default' => '#000',
    'output' => array(
        array(
            'element' => '.footer-social-menu-wrapper a:hover',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.footer-social-menu-wrapper a:hover',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'footer_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
    ),
) );

Kirki::add_field( 'alexandrie', array(
    'type' => 'radio-buttonset',
    'settings' => 'footer_social_menu_border_style',
    'label' => __( 'Social Menue Border Style', 'alexandrie' ),
    'section' => 'footer',
    'default' => 'solid',
    'priority' => 10,
    'multiple' => 1,
    'choices' => array(
        'none' => esc_attr__( 'None', 'alexandrie' ),
        'solid' => esc_attr__( 'solid', 'alexandrie' ),
        'dotted' => esc_attr__( 'dotted', 'alexandrie' ),
        'dashed' => esc_attr__( 'dashed', 'alexandrie' ),
    ),
    'output' => array(
        array(
            'element' => '.footer-social-menu-wrapper',
            'property' => 'border-style',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'footer_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
) ) );

Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'footer_social_menu_border_color',
    'label' => esc_attr__( 'Footer social menu Border color', 'alexandrie' ),
    'section' => 'footer',
    'priority' => 10,
    'default' => '#f8f9fa',
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.footer-social-menu-wrapper',
            'property' => 'border-color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.footer-social-menu-wrapper',
             'function'=>'css',
            'property' => 'border-color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'footer_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
        array(
            'setting' => 'footer_social_menu_border_style',
            'value' => 'none',
            'operator' => '!='
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'settings' => 'footer_social_menu_border_thickness',
    'section' => 'footer',
    'label' => esc_html__( 'Footer social Border Thickness', 'alexandrie' ),
    'type' => 'dimensions',
    'default' => array(
        'top-width' => '1px',
        'right-width' => '0',
        'bottom-width' => '1px',
        'left-width' => '0',
    ),
    'choices' => array(
        'top-width' => esc_attr__( 'Top', 'alexandrie' ),
        'right-width' => esc_attr__( 'Right', 'alexandrie' ),
        'bottom-width' => esc_attr__( 'Bottom', 'alexandrie' ),
        'left-width' => esc_attr__( 'Left', 'alexandrie' ),
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.footer-social-menu-wrapper',
            'property' => 'border',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.footer-social-menu-wrapper',
            'function' => 'css',
            'property' => 'border',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'footer_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
        array(
            'setting' => 'footer_social_menu_border_style',
            'value' => 'none',
            'operator' => '!='
        ),
    ),
) );

