<?php

/* * **************************************************
 * Sidebar Section
 * ************************************************** */

// Menu Tabs
alexandrie_tabs( 'sidebar_setting', 'sidebar' );

alexandrie_shortcuts( 'sidebar_shortcut', 'sidebar', 'partial_sidebar', '#content #secondary' );

/* Sidebar General Fields */

// Archive Sidebar
Kirki::add_field( 'alexandrie', array(
    'type' => 'select',
    'settings' => 'global_sidebar_col',
    'label' => __( 'Archive Sidebar', 'alexandrie' ),
    'description' => 'Applied to all the archive pages. Such as Blog, Category, Tag, Date, Search, etc.',
    'section' => 'sidebar',
    'transport' => 'auto',
    'default' => 'right-sidebar',
    'choices' => array(
        'no-sidebar' => esc_attr__( 'Disable', 'alexandrie' ),
        'left-sidebar' => esc_attr__( 'Left', 'alexandrie' ),
        'right-sidebar' => esc_attr__( 'Right', 'alexandrie' ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'sidebar_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );

// Singular 
Kirki::add_field( 'alexandrie', array(
    'type' => 'select',
    'settings' => 'singular_sidebar_col',
    'label' => __( 'Singular Sidebar', 'alexandrie' ),
    'description' => 'Applied to all the single posts/pages. Will get override by the page/post meta options for specific page/post.',
    'section' => 'sidebar',
    'transport' => 'auto',
    'default' => 'right-sidebar',
    'choices' => array(
        'no-sidebar' => esc_attr__( 'Disable', 'alexandrie' ),
        'left-sidebar' => esc_attr__( 'Left', 'alexandrie' ),
        'right-sidebar' => esc_attr__( 'Right', 'alexandrie' ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'sidebar_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );
// author 
Kirki::add_field( 'alexandrie', array(
    'type' => 'select',
    'settings' => 'author_sidebar_col',
    'label' => __( 'Author Sidebar', 'alexandrie' ),
    'description' => __( 'Applied to all the Author page.', 'alexandrie' ),
    'section' => 'sidebar',
    'transport' => 'auto',
    'default' => 'no-sidebar',
    'choices' => array(
        'no-sidebar' => esc_attr__( 'Disable', 'alexandrie' ),
        'left-sidebar' => esc_attr__( 'Left', 'alexandrie' ),
        'right-sidebar' => esc_attr__( 'Right', 'alexandrie' ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'sidebar_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );


// Sidebar Padding
Kirki::add_field( 'alexandrie', array(
    'type' => 'slider',
    'settings' => 'content_sidebar_spacing',
    'label' => esc_attr__( 'Content Sidebar Spacing', 'alexandrie' ),
    'section' => 'sidebar',
    'transport' => 'auto',
    'default' => 30,
    'output' => array(
        array(
            'element' => '.right-sidebar, .left-sidebar',
            'property' => 'grid-column-gap',
            'units' => 'px'
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'sidebar_col',
            'operator' => '!=',
            'value' => 'no-sidebar',
        ),
        array(
            'setting' => 'sidebar_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.right-sidebar, .left-sidebar',
            'property' => 'grid-column-gap',
            'function' => 'css',
            'units' => 'px',
        ),
    ),
    'choices' => array(
        'min' => '0',
        'max' => '300',
        'step' => '1',
    ),
) );

/* Sidebar Style Fields */

// Sidebar title Styles
alexandrie_headlines( 'sidebar_title_styles', 'sidebar', 'sidebar_setting', 'style', 'Widget Title', '10px' );

// Sidebar Title Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'sidebar_link_color',
    'label' => __( 'Link', 'alexandrie' ),
    'section' => 'sidebar',
    'transport' => 'auto',
    'default' => '#8E959B',
    'output' => array(
        array(
            'element' => '#secondary a',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '#secondary a',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'sidebar_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'sidebar_link_hover_color',
    'label' => __( 'Link hover', 'alexandrie' ),
    'section' => 'sidebar',
    'transport' => 'auto',
    'default' => '#000',
    'output' => array(
        array(
            'element' => '#secondary a:hover',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '#secondary a:hover',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'sidebar_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Sidebar Widgets Titles
Kirki::add_field( 'alexandrie', array(
    'type' => 'typography',
    'label' => __( 'Title', 'alexandrie' ),
    'settings' => 'sidebar_widgets_title_typography',
    'section' => 'sidebar',
    'transport' => 'auto',
    'default' => array(
        'font-family' => '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif',
        'variant' => 'regular',
        'font-size' => '30px',
        'line-height' => '1.5',
        'letter-spacing' => '0',
        'text-transform' => 'none',
        'text-align' => 'left',
        'color' => '#8E959B',
    ),
    'output' => array(
        array(
            'element' => '#secondary .widget-title',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '#secondary .widget-title',
            'function'=>'css',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'sidebar_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );



// Sidebar Text Styles
alexandrie_headlines( 'sidebar_text_styles', 'sidebar', 'sidebar_setting', 'style', 'Widget Text', '35px' );

// Sidebar Widgets Text 
Kirki::add_field( 'alexandrie', array(
    'type' => 'typography',
    'settings' => 'sidebar_widgets_text_typography',
    'section' => 'sidebar',
    'label' => __( 'Text', 'alexandrie' ),
    'transport' => 'auto',
    'default' => array(
        'font-family' => '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif',
        'variant' => 'regular',
        'font-size' => '17px',
        'line-height' => '1.5',
        'letter-spacing' => '0',
        'text-transform' => 'none',
        'text-align' => 'left',
        'color' => '#3a3a3a',
    ),
    'output' => array(
        array(
            'element' => '#secondary .widget',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '#secondary .widget',
            'function'=>'css',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'sidebar_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );


// Sidebar Bakcground Styles
alexandrie_headlines( 'sidebar_widgets_backgrounds', 'sidebar', 'sidebar_setting', 'style', 'Backgrounds', '35px' );


Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'sidebar_wid_bg',
    'label' => __( 'Widgets', 'alexandrie' ),
    'section' => 'sidebar',
    'transport' => 'auto',
    'default' => 'transparent',
    'choices' => array(
        'alpha' => true,
    ),
    'output' => array(
        array(
            'element' => '#secondary .widget',
            'property' => 'background-color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '#secondary .widget',
            'function' => 'css',
            'property' => 'background-color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'sidebar_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );



/* Advanced Field */
Kirki::add_field( 'alexandrie', array(
    'settings' => 'sidebar_widget_padding',
    'section' => 'sidebar',
    'label' => esc_html__( 'Sidebar widget Padding', 'alexandrie' ),
    'type' => 'spacing',
    'default' => array(
        'top' => '2rem',
        'right' => '1rem',
        'bottom' => '2rem',
        'left' => '1rem',
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '#secondary .widget ',
            'property' => 'padding',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '#secondary .widget ',
             'function'=>'css',
            'property' => 'padding',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'sidebar_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'settings' => 'sidebar_widget_margin',
    'section' => 'sidebar',
    'label' => esc_html__( 'Sidebar widget Padding', 'alexandrie' ),
    'type' => 'spacing',
    'default' => array(
        'top' => '40px',
        'right' => '0',
        'bottom' => '40px',
        'left' => '0',
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '#secondary .widget ',
            'property' => 'margin',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '#secondary .widget ',
             'function'=>'css',
            'property' => 'margin',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'sidebar_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
    ),
) );

Kirki::add_field( 'alexandrie', array(
    'type' => 'select',
    'settings' => 'sidebar_shadow',
    'label' => __( 'widget Shadow', 'alexandrie' ),
    'section' => 'sidebar',
    'default' => 'none',
    'priority' => 10,
    'multiple' => 1,
    'choices' => array(
        'shadow-none' => esc_attr__( 'None', 'alexandrie' ),
        'shadow-sm' => esc_attr__( 'Small Shadow', 'alexandrie' ),
        'shadow' => esc_attr__( 'Shadow', 'alexandrie' ),
        'shadow-lg' => esc_attr__( 'large Shadow', 'alexandrie' ),
        'shadow-custom' => esc_attr__( '3D shadow', 'alexandrie' ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'sidebar_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'settings' => 'sidebar_border_radius',
    'section' => 'sidebar',
    'label' => esc_html__( 'widget Border radius', 'alexandrie' ),
    'type' => 'dimensions',
    'default' => array(
        'top-left-radius' => '1rem',
        'top-right-radius' => '1rem',
        'bottom-right-radius' => '1rem',
        'bottom-left-radius' => '1rem',
    ),
    'choices' => array(
        'top-left-radius' => esc_attr__( 'Top-Left radius', 'alexandrie' ),
        'top-right-radius' => esc_attr__( 'Top-Right-radius:', 'alexandrie' ),
        'bottom-left-radius' => esc_attr__( 'Bottom-Left radius', 'alexandrie' ),
        'bottom-right-radius' => esc_attr__( 'Bottom-Right-radius', 'alexandrie' ),
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '#secondary .widget',
            'property' => 'border',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '#secondary .widget',
            'function' => 'css',
            'property' => 'border',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'sidebar_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'type' => 'radio-buttonset',
    'settings' => 'sidebar_border_style',
    'label' => __( 'widget Border Style', 'alexandrie' ),
    'section' => 'sidebar',
    'default' => 'solid',
    'priority' => 10,
    'multiple' => 1,
    'choices' => array(
        'none' => esc_attr__( 'None', 'alexandrie' ),
        'solid' => esc_attr__( 'solid', 'alexandrie' ),
        'dotted' => esc_attr__( 'dotted', 'alexandrie' ),
        'dashed' => esc_attr__( 'dashed', 'alexandrie' ),
    ),
    'output' => array(
        array(
            'element' => '#secondary .widget',
            'property' => 'border-style',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'sidebar_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
) ) );

Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'sidebar_border_color',
    'label' => esc_attr__( 'widget Border color', 'alexandrie' ),
    'section' => 'sidebar',
    'priority' => 10,
    'default' => '#ccc',
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '#secondary .widget',
            'property' => 'border-color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '#secondary .widget',
            'function'=>'css',
            'property' => 'border-color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'sidebar_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
        array(
            'setting' => 'sidebar_border_style',
            'value' => 'none',
            'operator' => '!='
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'settings' => 'sidebar_border',
    'section' => 'sidebar',
    'label' => esc_html__( 'widget Border Thickness', 'alexandrie' ),
    'type' => 'dimensions',
    'default' => array(
        'top-width' => '1px',
        'right-width' => '1px',
        'bottom-width' => '1px',
        'left-width' => '1px',
    ),
    'choices' => array(
        'top-width' => esc_attr__( 'Top', 'alexandrie' ),
        'right-width' => esc_attr__( 'Right', 'alexandrie' ),
        'bottom-width' => esc_attr__( 'Bottom', 'alexandrie' ),
        'left-width' => esc_attr__( 'Left', 'alexandrie' ),
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '#secondary .widget',
            'property' => 'border',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '#secondary .widget',
            'function' => 'css',
            'property' => 'border',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'sidebar_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
        array(
            'setting' => 'sidebar_border_style',
            'value' => 'none',
            'operator' => '!='
        ),
    ),
) );alexandrie_headlines( 'sidebar_menu_styles', 'sidebar', 'sidebar_setting', 'advanced', 'Menu Item', '35px' );
//menu li spacing
Kirki::add_field( 'alexandrie', array(
    'settings' => 'sidebar_menu_li_padding',
    'section' => 'sidebar',
    'label' => esc_html__( 'Sidebar Meu item padding', 'alexandrie' ),
    'type' => 'spacing',
    'default' => array(
        'top' => '5px',
        'right' => '5px',
        'bottom' => '5px',
        'left' => '5px',
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '#secondary .widget li',
            'property' => 'padding',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '#secondary .widget li',
             'function'=>'css',
            'property' => 'padding',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'sidebar_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
    ),
) );

//menu li border
Kirki::add_field( 'alexandrie', array(
    'type' => 'radio-buttonset',
    'settings' => 'sidebar_menu_li_border_style',
    'label' => __( 'widget menu item Border Style', 'alexandrie' ),
    'section' => 'sidebar',
    'default' => 'dashed',
    'priority' => 10,
    'multiple' => 1,
    'choices' => array(
        'none' => esc_attr__( 'None', 'alexandrie' ),
        'solid' => esc_attr__( 'solid', 'alexandrie' ),
        'dotted' => esc_attr__( 'dotted', 'alexandrie' ),
        'dashed' => esc_attr__( 'dashed', 'alexandrie' ),
    ),
    'output' => array(
        array(
            'element' => '#secondary .widget li',
            'property' => 'border-style',
        ),
    ),
    
    'active_callback' => array(
        array(
            'setting' => 'sidebar_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
) ) );

Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'sidebar_menu_li_border_color',
    'label' => esc_attr__( 'widget Border color', 'alexandrie' ),
    'section' => 'sidebar',
    'priority' => 10,
    'default' => '#DEE2E6',
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '#secondary .widget li',
            'property' => 'border-color',
        ),
//        array(
//            'element' => '#secondary .widget li:last-of-type',
//            'property' => 'border-color',
//            'value_pattern'=>'transparent',
//        ),
    ),
    'js_vars' => array(
        array(
            'element' => '#secondary .widget li',
            'function'=>'css',
            'property' => 'border-color',
        ),
//        array(
//            'element' => '#secondary .widget li:last-of-type',
////            'property' => 'border-color',
////            'function'=>'css',
////            'value_pattern'=>'transparent',
//        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'sidebar_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
        array(
            'setting' => 'sidebar_menu_li_border_style',
            'value' => 'none',
            'operator' => '!='
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'settings' => 'sidebar_menu_li_border',
    'section' => 'sidebar',
    'label' => esc_html__( 'widget Border Thickness', 'alexandrie' ),
    'type' => 'dimensions',
    'default' => array(
        'top-width' => '1px',
        'right-width' => '1px',
        'bottom-width' => '1px',
        'left-width' => '1px',
    ),
    'choices' => array(
        'top-width' => esc_attr__( 'Top', 'alexandrie' ),
        'right-width' => esc_attr__( 'Right', 'alexandrie' ),
        'bottom-width' => esc_attr__( 'Bottom', 'alexandrie' ),
        'left-width' => esc_attr__( 'Left', 'alexandrie' ),
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '#secondary .widget li',
            'property' => 'border',
        ),
       
    ),
    'js_vars' => array(
        array(
            'element' => '#secondary .widget li',
            'function' => 'css',
            'property' => 'border',
        ),
        
    ),
    'active_callback' => array(
        array(
            'setting' => 'sidebar_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
        array(
            'setting' => 'sidebar_menu_li_border_style',
            'value' => 'none',
            'operator' => '!='
        ),
    ),
) );
