<?php

/* * **************************************************
 * Menu Section
 * ************************************************** */

// Menu Tabs
alexandrie_tabs( 'menu_setting', 'alexandrie_menu' );

/* General Fields */
//alexandrie_no_options( 'menu_general_no_options', 'alexandrie_menu', 'menu_setting', 'general' );


Kirki::add_field( 'alexandrie', [
    'type' => 'radio-buttonset',
    'settings' => 'primary_menu_align',
    'label' => esc_html__( 'Primary Menu alignment', 'alexandrie' ),
//	'description' => esc_html__( 'select Main menu alignmebt', 'alexandrie' ),
    'section' => 'alexandrie_menu',
    'default' => 'nav',
    'choices' => [
        'nav' => esc_html__( 'Left', 'alexandrie' ),
        'nav justify-content-center' => esc_html__( 'Center', 'alexandrie' ),
        'nav justify-content-end' => esc_html__( 'Right', 'alexandrie' ),
    ],
    'active_callback' => array(
        array(
            'setting' => 'menu_setting',
            'operator' => '==',
            'value' => 'general',
        ),
        
    ),
    'output' => array(
            array(
                'element' => '.menu.nav',
                'property' => 'display',
                'value_pattern' => 'none',
                'media_query'=> '@media (max-width: 768px)',
            ),
        ),
] );

// Top Menu Styles
alexandrie_headlines( 'top_menu_styles', 'alexandrie_menu', 'menu_setting', 'style', 'Top Level Menu', '10px' );

// Menu BG Color
//Kirki::add_field( 'alexandrie', array(
//    'type' => 'color',
//    'settings' => 'menu_wrapper_bg',
//    'label' => __( 'Menu Background', 'alexandrie' ),
//    'section' => 'alexandrie_menu',
//    'transport' => 'auto',
//    'default' => 'transparent',
//    'choices' => array(
//        'alpha' => true,
//    ),
//    'output' => array(
//        array(
//            'element' => '.wrapper-navbar',
//            'property' => 'background-color',
//        ),
//    ),
//    'js_vars' => array(
//        array(
//            'element' => '.wrapper-navbar',
//            'function' => 'css',
//            'property' => 'background-color',
//        ),
//    ),
//    'active_callback' => array(
//        array(
//            'setting' => 'menu_setting',
//            'operator' => '==',
//            'value' => 'style',
//        ),
//        array(
//            'setting' => 'header_layout',
//            'operator' => '==',
//            'value' => 'header-center',
//        ),
//    ),
//) );
// Menu BG Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'menu_bg',
    'label' => __( 'Background', 'alexandrie' ),
    'section' => 'alexandrie_menu',
    'transport' => 'auto',
    'default' => 'transparent',
    'choices' => array(
        'alpha' => true,
    ),
    'output' => array(
        array(
            'element' => '.alexandrie_nav.main-navigation',
            'property' => 'background-color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.alexandrie_nav.main-navigation',
            'function' => 'css',
            'property' => 'background-color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'menu_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Menu Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'menu_color',
    'label' => __( 'Text', 'alexandrie' ),
    'section' => 'alexandrie_menu',
    'transport' => 'auto',
    'default' => '#000000',
    'output' => array(
        array(
            'element' => '.alexandrie_nav ul li a',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.alexandrie_nav ul li a',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'menu_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Menu BG Hover Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'menu_hover_bg',
    'label' => __( 'Hover Background', 'alexandrie' ),
    'section' => 'alexandrie_menu',
    'transport' => 'auto',
    'default' => '#d5d8dc',
    'choices' => array(
        'alpha' => true,
    ),
    'output' => array(
        array(
            'element' => '.alexandrie_nav ul li a:hover, .alexandrie_nav ul li:hover a',
            'property' => 'background-color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.alexandrie_nav ul li a:hover, .alexandrie_nav ul li:hover a',
            'function' => 'css',
            'property' => 'background-color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'menu_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Menu Hover Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'menu_hover_color',
    'label' => __( 'Hover Text', 'alexandrie' ),
    'section' => 'alexandrie_menu',
    'transport' => 'auto',
    'default' => '#0088CC',
    'output' => array(
        array(
            'element' => '.alexandrie_nav ul li a:hover,.alexandrie_nav ul li a:visited',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.alexandrie_nav ul li a:hover,.alexandrie_nav ul li a:visited',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'menu_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Menu BG Active Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'menu_active_bg',
    'label' => __( 'Active Background', 'alexandrie' ),
    'section' => 'alexandrie_menu',
    'transport' => 'auto',
    'default' => '#d5d8dc',
    'choices' => array(
        'alpha' => true,
    ),
    'output' => array(
        array(
            'element' => '.alexandrie_nav ul li.current-menu-item a, .alexandrie_nav ul li.current_page_ancestor a',
            'property' => 'background-color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.alexandrie_nav ul li.current-menu-item a, .alexandrie_nav ul li.current_page_ancestor a',
            'function' => 'css',
            'property' => 'background-color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'menu_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Menu Active Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'menu_active_color',
    'label' => __( 'Active Text', 'alexandrie' ),
    'section' => 'alexandrie_menu',
    'transport' => 'auto',
    'default' => '#0088CC',
    'output' => array(
        array(
            'element' => '.alexandrie_nav ul li.current-menu-item a, .alexandrie_nav ul li.current_page_ancestor a',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.alexandrie_nav ul li.current-menu-item a, .alexandrie_nav ul li.current_page_ancestor a',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'menu_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Menu Icon Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'menu_icon_color',
    'label' => __( 'Icon Color', 'alexandrie' ),
    'section' => 'alexandrie_menu',
    'transport' => 'auto',
    'default' => '#000000',
    'output' => array(
        array(
            'element' => '.alexandrie_nav li a:after, .alexandrie_nav .sub_toggle',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.alexandrie_nav li a:after, .alexandrie_nav .sub_toggle',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'menu_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Top Level Menu Typography 
Kirki::add_field( 'alexandrie', array(
    'type' => 'typography',
    'settings' => 'primary_menu_typography',
    'section' => 'alexandrie_menu',
    'transport' => 'auto',
    'default' => array(
        'font-family' => '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif',
        'variant' => 'regular',
        'font-size' => '16px',
        'line-height' => '3.8',
        'letter-spacing' => '0',
        'text-transform' => 'none',
    ),
    'output' => array(
        array(
            'element' => '.alexandrie_nav ul li a',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'menu_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );


// Sub Menu Styles
alexandrie_headlines( 'submenu_styles', 'alexandrie_menu', 'menu_setting', 'style', 'Sub Menu', '35px' );

// Menu Dropdown BG Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'menu_dropdown_bg',
    'label' => __( 'Background', 'alexandrie' ),
    'section' => 'alexandrie_menu',
    'transport' => 'auto',
    'default' => '#d5d8dc',
    'choices' => array(
        'alpha' => true,
    ),
    'output' => array(
        array(
            'element' => '.alexandrie_nav ul.sub-menu li a',
            'property' => 'background-color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.alexandrie_nav ul.sub-menu li a',
            'function' => 'css',
            'property' => 'background-color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'menu_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Menu Dropdown Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'menu_dropdown_color',
    'label' => __( 'Text', 'alexandrie' ),
    'section' => 'alexandrie_menu',
    'transport' => 'auto',
    'default' => '#000000',
    'output' => array(
        array(
            'element' => '.alexandrie_nav ul.sub-menu li a',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.alexandrie_nav ul.sub-menu li a',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'menu_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Menu Dropdown BG Hover Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'menu_dropdown_bg_hover',
    'label' => __( 'Hover Background', 'alexandrie' ),
    'section' => 'alexandrie_menu',
    'transport' => 'auto',
    'default' => '#c7ccd0',
    'choices' => array(
        'alpha' => true,
    ),
    'output' => array(
        array(
            'element' => '.alexandrie_nav .sub-menu li a:hover',
            'property' => 'background-color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.alexandrie_nav .sub-menu li a:hover',
            'function' => 'css',
            'property' => 'background-color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'menu_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Menu Dropdown Hover Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'menu_dropdown_hover_color',
    'label' => __( 'Hover Text', 'alexandrie' ),
    'section' => 'alexandrie_menu',
    'transport' => 'auto',
    'default' => '#0088CC',
    'output' => array(
        array(
            'element' => '.alexandrie_nav .sub-menu li a:hover',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.alexandrie_nav .sub-menu li a:hover',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'menu_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Menu Dropdown Active BG
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'menu_dropdown_active_bg',
    'label' => __( 'Active Background', 'alexandrie' ),
    'section' => 'alexandrie_menu',
    'transport' => 'auto',
    'default' => '#c7ccd0',
    'choices' => array(
        'alpha' => true,
    ),
    'output' => array(
        array(
            'element' => '.alexandrie_nav .sub-menu li.current-menu-ancestor > a, .alexandrie_nav .sub-menu li.current_page_item > a',
            'property' => 'background-color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.alexandrie_nav .sub-menu li.current-menu-ancestor > a, .alexandrie_nav .sub-menu li.current_page_item > a',
            'function' => 'css',
            'property' => 'background-color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'menu_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Menu Dropdown Active Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'menu_dropdown_active_color',
    'label' => __( 'Active Text', 'alexandrie' ),
    'section' => 'alexandrie_menu',
    'transport' => 'auto',
    'default' => '#0088CC',
    'output' => array(
        array(
            'element' => '.alexandrie_nav .sub-menu  li.current-menu-ancestor > a, .alexandrie_nav .sub-menu  li.current_page_item > a',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.alexandrie_nav .sub-menu  li.current-menu-ancestor > a, .alexandrie_nav .sub-menu  li.current_page_item > a',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'menu_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Submenu Typography
Kirki::add_field( 'alexandrie', array(
    'type' => 'typography',
    'settings' => 'submenu_typography',
    'section' => 'alexandrie_menu',
    'transport' => 'auto',
    'default' => array(
        'font-family' => '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif',
        'variant' => 'regular',
        'font-size' => '16px',
        'line-height' => '3.8',
        'letter-spacing' => '0',
        'text-transform' => 'none',
    ),
    'output' => array(
        array(
            'element' => '.alexandrie_nav ul ul li a',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.alexandrie_nav ul ul li a',
            'function' => 'css',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'menu_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );



// Mobile Menu Styles
alexandrie_headlines( 'mobilemenu_styles', 'alexandrie_menu', 'menu_setting', 'style', 'Mobile Menu', '35px' );

// Mobile Menu Toggle Bg Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'mobile_menu_toggle_bg_color',
    'label' => __( 'Menu Background', 'alexandrie' ),
    'section' => 'alexandrie_menu',
    'transport' => 'auto',
    'default' => '#000000',
    'choices' => array(
        'alpha' => true,
    ),
    'output' => array(
        array(
            'element' => '.alexandrie_nav .menu_toggle',
            'property' => 'background-color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.alexandrie_nav .menu_toggle',
            'function' => 'css',
            'property' => 'background-color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'menu_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Mobile Menu Toggle Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'mobile_menu_toggle_color',
    'label' => __( 'Menu Color', 'alexandrie' ),
    'section' => 'alexandrie_menu',
    'transport' => 'auto',
    'default' => '#ffffff',
    'output' => array(
        array(
            'element' => '.alexandrie_nav .menu_toggle',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.alexandrie_nav .menu_toggle',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'menu_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Mobile Menu Submenu Toggle Icon BG Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'mobile_menu_open_bg_color',
    'label' => __( 'Toggle Icon Background', 'alexandrie' ),
    'section' => 'alexandrie_menu',
    'transport' => 'auto',
    'default' => '#000000',
    'choices' => array(
        'alpha' => true,
    ),
    'output' => array(
        array(
            'element' => '.alexandrie_nav .sub_toggle',
            'property' => 'background-color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.alexandrie_nav .sub_toggle',
            'function' => 'css',
            'property' => 'background-color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'menu_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Mobile Menu Submenu Toggle Icon Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'mobile_menu_toggle_text_color',
    'label' => __( 'Toggle Icon Color', 'alexandrie' ),
    'section' => 'alexandrie_menu',
    'transport' => 'auto',
    'default' => '#ffffff',
    'output' => array(
        array(
            'element' => '.alexandrie_nav .sub_toggle',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.alexandrie_nav .sub_toggle',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'menu_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );


alexandrie_headlines( 'advanced_menu_styles', 'alexandrie_menu', 'menu_setting', 'advanced', 'Menu Wrapper style', '35px' );
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'menu_wrapper_bg_color',
    'label' => esc_attr__( 'Background color', 'alexandrie' ),
    'section' => 'alexandrie_menu',
    'priority' => 10,
    'default' => '#fff',
    'choices' => array(
        'alpha' => true,
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.wrapper-navbar,wrapper-navbar.header-pin,#visible_scroll_down.header-pin, #visible_scroll_up.header-pin',
            'property' => 'background-color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.wrapper-navbar,.wrapper-navbar.header-pin,#visible_scroll_down.header-pin, #visible_scroll_up.header-pin',
            'function' => 'css',
            'property' => 'background-color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'menu_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
        array(
            'setting' => 'header_layout',
            'operator' => 'in',
            'value' => array( 'header-center', 'header-right-widget', 'header-left-widget' ),
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'type' => 'radio-buttonset',
    'settings' => 'menu_wrapper_border_style',
    'label' => __( 'Border Style', 'alexandrie' ),
    'section' => 'alexandrie_menu',
    'transport' => 'auto',
    'default' => 'solid',
    'priority' => 10,
    'multiple' => 1,
    'choices' => array(
        'none' => esc_attr__( 'None', 'alexandrie' ),
        'solid' => esc_attr__( 'solid', 'alexandrie' ),
        'dotted' => esc_attr__( 'dotted', 'alexandrie' ),
        'dashed' => esc_attr__( 'dashed', 'alexandrie' ),
    ),
    'output' => array(
        array(
            'element' => '.wrapper-navbar',
            'property' => 'border-style',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.wrapper-navbar',
            'function' => 'css',
            'property' => 'border-style',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'menu_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
        array(
            'setting' => 'header_layout',
            'operator' => 'in',
            'value' => array( 'header-center', 'header-right-widget', 'header-left-widget' ),
        ),
) ) );

Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'menu_wrapper_border_color',
    'label' => esc_attr__( 'Border color', 'alexandrie' ),
    'section' => 'alexandrie_menu',
    'priority' => 10,
    'default' => '#ccc',
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.wrapper-navbar',
            'property' => 'border-color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.wrapper-navbar',
            'function' => 'css',
            'property' => 'border-color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'menu_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
        array(
            'setting' => 'menu_wrapper_border_style',
            'value' => 'none',
            'operator' => '!='
        ),
        array(
            'setting' => 'header_layout',
            'operator' => 'in',
            'value' => array( 'header-center', 'header-right-widget', 'header-left-widget' ),
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'settings' => 'menu_wrapper_border_thickness',
    'section' => 'alexandrie_menu',
    'label' => esc_html__( 'Border Thickness', 'alexandrie' ),
    'type' => 'dimensions',
    'default' => array(
        'top-width' => '0px',
        'right-width' => '0px',
        'bottom-width' => '0px',
        'left-width' => '0px',
    ),
    'choices' => array(
        'top-width' => esc_attr__( 'Top', 'alexandrie' ),
        'right-width' => esc_attr__( 'Right', 'alexandrie' ),
        'bottom-width' => esc_attr__( 'Bottom', 'alexandrie' ),
        'left-width' => esc_attr__( 'Left', 'alexandrie' ),
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.wrapper-navbar',
            'property' => 'border',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.wrapper-navbar',
            'function' => 'css',
            'property' => 'border',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'menu_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
        array(
            'setting' => 'menu_wrapper_border_style',
            'value' => 'none',
            'operator' => '!='
        ),
        array(
            'setting' => 'header_layout',
            'operator' => 'in',
            'value' => array( 'header-center', 'header-right-widget', 'header-left-widget' ),
        ),
    ),
) );
