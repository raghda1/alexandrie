<?php

/* * **************************************************
 * Copyright Section
 * ************************************************** */

// Copyright Tabs
alexandrie_tabs( 'copyright_setting', 'copyright' );

alexandrie_shortcuts( 'copyright_shortcut', 'copyright', 'partial_copyright', '.site-footer .site-info .container' );

/* Copyright General Fields */

Kirki::add_field( 'alexandrie', array(
    'type' => 'select',
    'settings' => 'global_copyright_content_layout',
    'label' => __( 'Copyright Layout', 'alexandrie' ),
    'section' => 'copyright',
    'transport' => 'auto',
    'default' => 'container',
    'choices' => array(
        'container' => esc_attr__( 'Boxed', 'alexandrie' ),
        'container-fluid' => esc_attr__( 'Full Width', 'alexandrie' ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'copyright_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'type' => 'toggle',
    'settings' => 'show_footer_sitemap_menu',
    'label' => esc_attr__( 'show site map menu', 'alexandrie' ),
    'section' => 'copyright',
    'default' => '1',
    'active_callback' => array(
        array(
            'setting' => 'copyright_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );

// Footer Bar
Kirki::add_field( 'alexandrie', array(
    'type' => 'editor',
    'settings' => 'above_copyright',
    'label' => __( 'Above Copyright', 'alexandrie' ),
    'section' => 'copyright',
    'transport' => 'auto',
    'default' => __( 'Alexandrie', 'alexandrie' ),
    'active_callback' => array(
        array(
            'setting' => 'copyright_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'type' => 'textarea',
    'settings' => 'footer_copyright',
    'label' => __( 'Copyright', 'alexandrie' ),
    'section' => 'copyright',
//    'transport' => 'auto',
    'default' => __( 'Copyright &copy; 2017 | Powered by wordpress', 'alexandrie' ),
    'active_callback' => array(
        array(
            'setting' => 'copyright_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
    
    
) );


/* Copyright Style Fields */

// Copyright Text Styles
alexandrie_headlines( 'copyright_text_styles', 'copyright', 'copyright_setting', 'style', 'Content', '10px' );

// Copyright Text Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'typography',
    'settings' => 'copyright_content_color',
    'label' => __( 'Text Above Coyright', 'alexandrie' ),
    'section' => 'copyright',
    'transport' => 'auto',
    'default' => array(
        'font-family' => '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif',
        'variant' => 'regular',
        'font-size' => '30px',
        'line-height' => '1.5',
        'letter-spacing' => '0',
        'text-transform' => 'none',
        'text-align' => 'center',
        'color' => '#000',
    ),
    'output' => array(
        array(
            'element' => '.site-footer .site-info .site-info-text',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.site-footer .site-info .site-info-text',
            'function' => 'css',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'copyright_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'copyright_text_color',
    'label' => __( 'Copyright Text', 'alexandrie' ),
    'section' => 'copyright',
    'transport' => 'auto',
    'default' => '#3a3a3a',
    'output' => array(
        array(
            'element' => '.site-footer .site-info',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.site-footer .site-info',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'copyright_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Link Color / Link Visited
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'copyright_link_color',
    'label' => __( 'Link', 'alexandrie' ),
    'section' => 'copyright',
    'transport' => 'auto',
    'default' => '#000',
    'output' => array(
        array(
            'element' => '.site-info a, .site-info a:visited',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.site-info a,.site-info a:visited',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'copyright_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Link Hover Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'copyright_link_hover_color',
    'label' => __( 'Link Hover', 'alexandrie' ),
    'section' => 'copyright',
    'transport' => 'auto',
    'default' => '#000000',
    'output' => array(
        array(
            'element' => '.site-info a:hover',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.site-info a:hover',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'copyright_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );


// Copyright Typography 
Kirki::add_field( 'alexandrie', array(
    'type' => 'typography',
    'settings' => 'copyright_text_typography',
    'section' => 'copyright',
    'transport' => 'auto',
    'default' => array(
        'font-size' => '17px',
        'line-height' => '1.5',
        'letter-spacing' => '0',
        'text-transform' => 'none',
        'text-align' => 'center'
    ),
    'output' => array(
        array(
            'element' => '.site-footer .site-info',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'copyright_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Copyright Backgrounds Styles
alexandrie_headlines( 'copyright_backgrounds_styles', 'copyright', 'copyright_setting', 'style', 'Backgrounds', '35px' );

// Copyright BG Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'copyright_content_bg',
    'label' => __( 'Content above copyright bg', 'alexandrie' ),
    'section' => 'copyright',
    'transport' => 'auto',
    'default' => '#D5D8DC',
    'choices' => array(
        'alpha' => true,
    ),
    'output' => array(
        array(
            'element' => '.site-info',
            'property' => 'background-color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.site-info',
            'function' => 'css',
            'property' => 'background-color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'copyright_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'copyright_bg',
    'label' => __( 'Copyright', 'alexandrie' ),
    'section' => 'copyright',
    'transport' => 'auto',
    'default' => '#D5D8DC',
    'choices' => array(
        'alpha' => true,
    ),
    'output' => array(
        array(
            'element' => '.site-info .copyright-text',
            'property' => 'background-color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.site-info .copyright-text',
            'function' => 'css',
            'property' => 'background-color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'copyright_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

/* Copyright Advanced Fields */

// Copyright Padding
Kirki::add_field( 'alexandrie', array(
    'settings' => 'copyright_content_spacing',
    'section' => 'copyright',
    'label' => esc_html__( 'Content above copyright Padding', 'alexandrie' ),
    'type' => 'spacing',
    'default' => array(
        'top' => '3rem',
        'right' => '15px',
        'bottom' => '3rem',
        'left' => '15px',
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.site-footer .site-info-content',
            'property' => 'padding',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.site-footer .site-info-content',
             'function'=>'css',
            'property' => 'padding',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'copyright_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'settings' => 'copyright_spacing',
    'section' => 'copyright',
    'label' => esc_html__( 'Copyright Padding', 'alexandrie' ),
    'type' => 'spacing',
    'default' => array(
        'top' => '10px',
        'right' => '15px',
        'bottom' => '10px',
        'left' => '15px',
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.site-footer .copyright-text',
            'property' => 'padding',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.site-footer .copyright-text',
             'function'=>'css',
            'property' => 'padding',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'copyright_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
    ),
) );

Kirki::add_field( 'alexandrie', array(
    'type' => 'radio-buttonset',
    'settings' => 'copyright_border_style',
    'label' => __( 'Footer Border Style', 'alexandrie' ),
    'section' => 'copyright',
    'default' => 'none',
    'priority' => 10,
    'multiple' => 1,
    'choices' => array(
        'none' => esc_attr__( 'None', 'alexandrie' ),
        'solid' => esc_attr__( 'solid', 'alexandrie' ),
        'dotted' => esc_attr__( 'dotted', 'alexandrie' ),
        'dashed' => esc_attr__( 'dashed', 'alexandrie' ),
    ),
    'output' => array(
        array(
            'element' => '.site-footer .site-info',
            'property' => 'border-style',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'copyright_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
) ) );

Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'copyright_border_color',
    'label' => esc_attr__( 'Footer social menu Border color', 'alexandrie' ),
    'section' => 'copyright',
    'priority' => 10,
    'default' => '#f8f9fa',
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.site-footer .site-info',
            'property' => 'border-color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'copyright_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
        array(
            'setting' => 'copyright_border_style',
            'value' => 'none',
            'operator' => '!='
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'settings' => 'copyright_border_thickness',
    'section' => 'copyright',
    'label' => esc_html__( 'Footer social Border Thickness', 'alexandrie' ),
    'type' => 'dimensions',
    'default' => array(
        'top-width' => '0',
        'right-width' => '0',
        'bottom-width' => '0',
        'left-width' => '0',
    ),
    'choices' => array(
        'top-width' => esc_attr__( 'Top', 'alexandrie' ),
        'right-width' => esc_attr__( 'Right', 'alexandrie' ),
        'bottom-width' => esc_attr__( 'Bottom', 'alexandrie' ),
        'left-width' => esc_attr__( 'Left', 'alexandrie' ),
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.site-footer .site-info',
            'property' => 'border',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.site-footer .site-info',
            'function' => 'css',
            'property' => 'border',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'copyright_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
        array(
            'setting' => 'copyright_border_style',
            'value' => 'none',
            'operator' => '!='
        ),
    ),
) );

