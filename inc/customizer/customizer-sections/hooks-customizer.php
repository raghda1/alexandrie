<?php

/****************************************************
* Hooks Fields
****************************************************/
Kirki::add_field( 'alexandrie', array(
    'type' => 'custom',
    'settings' => 'top_header_section',
    'section' => 'hooks',
    'default' => '<div style="border-bottom: 1px solid #32373c; line-height: 2em; font-size: 14px; background: #32373c; color: #fff; padding: 0 10px; cursor: auto; margin-top: 20px">' . esc_html__( 'NOTE: Theses fields accepts html in case you need to add addition html in available positions', 'alexandrie' ) . '</div>',
) );
// Before Header
Kirki::add_field( 'alexandrie', array(
	'type'     => 'editor',
	'settings' => 'alexandrie_before_header',
	'label'    => __( 'Before Header', 'alexandrie' ),
	'description' => 'alexandrie_before_header',
	'section'  => 'hooks',
) );

// Before Header contant
Kirki::add_field( 'alexandrie', array(
	'type'     => 'editor',
	'settings' => 'alexandrie_before_header_content',
	'label'    => __( 'Before Header Content', 'alexandrie' ),
	'description' => 'alexandrie_before_header_content',
	'section'  => 'hooks',
) );

// After Header contant
Kirki::add_field( 'alexandrie', array(
	'type'     => 'editor',
	'settings' => 'alexandrie_after_header_content',
	'label'    => __( 'After Header Content', 'alexandrie' ),
	'description' => 'alexandrie_after_header_content',
	'section'  => 'hooks',
) );

// After Header
Kirki::add_field( 'alexandrie', array(
	'type'     => 'editor',
	'settings' => 'alexandrie_after_header',
	'label'    => __( 'After Header', 'alexandrie' ),
	'description' => 'alexandrie_after_header',
	'section'  => 'hooks',
) );

// Content Container Before
Kirki::add_field( 'alexandrie', array(
	'type'     => 'editor',
	'settings' => 'alexandrie_inside_content_container_before',
	'label'    => __( 'Inside Content Container Before', 'alexandrie' ),
	'description' => 'alexandrie_inside_content_container_before',
	'section'  => 'hooks',
) );

// Before Content
Kirki::add_field( 'alexandrie', array(
	'type'     => 'editor',
	'settings' => 'alexandrie_before_content',
	'label'    => __( 'Before Content', 'alexandrie' ),
	'description' => 'alexandrie_before_content',
	'section'  => 'hooks',
) );

// After Content Title
Kirki::add_field( 'alexandrie', array(
	'type'     => 'editor',
	'settings' => 'alexandrie_after_content_title',
	'label'    => __( 'After Content Title', 'alexandrie' ),
	'description' => 'alexandrie_after_content_title',
	'section'  => 'hooks',
) );

// After Content
Kirki::add_field( 'alexandrie', array(
	'type'     => 'editor',
	'settings' => 'alexandrie_after_content',
	'label'    => __( 'After Content', 'alexandrie' ),
	'description' => 'alexandrie_after_content',
	'section'  => 'hooks',
) );

// Content Container After
Kirki::add_field( 'alexandrie', array(
	'type'     => 'editor',
	'settings' => 'alexandrie_inside_content_container_after',
	'label'    => __( 'Inside Content Container After', 'alexandrie' ),
	'description' => 'alexandrie_inside_content_container_after',
	'section'  => 'hooks',
) );

// Before Right Sidebar
Kirki::add_field( 'alexandrie', array(
	'type'     => 'editor',
	'settings' => 'alexandrie_before_sidebar',
	'label'    => __( 'Before Sidebar', 'alexandrie' ),
	'description' => 'alexandrie_before_sidebar',
	'section'  => 'hooks',
) );

// After Right Sidebar
Kirki::add_field( 'alexandrie', array(
	'type'     => 'editor',
	'settings' => 'alexandrie_after_sidebar',
	'label'    => __( 'After Sidebar', 'alexandrie' ),
	'description' => 'alexandrie_after_sidebar',
	'section'  => 'hooks',
) );

// Before Footer
Kirki::add_field( 'alexandrie', array(
	'type'     => 'editor',
	'settings' => 'alexandrie_before_footer',
	'label'    => __( 'Before Footer', 'alexandrie' ),
	'description' => 'alexandrie_before_footer',
	'section'  => 'hooks',
) );

// After Footer Widgets
Kirki::add_field( 'alexandrie', array(
	'type'     => 'editor',
	'settings' => 'alexandrie_after_footer_widgets',
	'label'    => __( 'After Footer Widgets', 'alexandrie' ),
	'description' => 'alexandrie_after_footer_widgets',
	'section'  => 'hooks',
) );

// After Footer
Kirki::add_field( 'alexandrie', array(
	'type'     => 'editor',
	'settings' => 'alexandrie_after_footer',
	'label'    => __( 'After Footer', 'alexandrie' ),
	'description' => 'alexandrie_after_footer',
	'section'  => 'hooks',
) );






// Hooks

/****************************************
 * Before Header
 ******************************************/
function hook_alexandrie_before_header(){
	 echo do_shortcode(get_theme_mod( 'alexandrie_before_header'));
}
add_action('alexandrie_before_header', 'hook_alexandrie_before_header');


/****************************************
 * Before Header Content
 ******************************************/
function hook_alexandrie_before_header_content(){
	 echo do_shortcode(get_theme_mod( 'alexandrie_before_header_content'));
}
add_action('alexandrie_before_header_content', 'hook_alexandrie_before_header_content');


/****************************************
 * After Header Content
 ******************************************/
function hook_alexandrie_after_header_content(){
	 echo do_shortcode(get_theme_mod( 'alexandrie_after_header_content'));
}
add_action('alexandrie_after_header_content', 'hook_alexandrie_after_header_content');


/******************************************
 * After Header
 ******************************************/
function hook_alexandrie_after_header(){
	 echo do_shortcode(get_theme_mod( 'alexandrie_after_header'));
}
add_action('alexandrie_after_header', 'hook_alexandrie_after_header');


/******************************************
 * Content Container Before
 ******************************************/
function hook_alexandrie_inside_content_container_before(){
	 echo do_shortcode(get_theme_mod( 'alexandrie_inside_content_container_before'));
}
add_action('alexandrie_inside_content_container_before', 'hook_alexandrie_inside_content_container_before');


/****************************************
 * Before Content
 ******************************************/
function hook_alexandrie_before_content(){
	 echo do_shortcode(get_theme_mod( 'alexandrie_before_content'));
}
add_action('alexandrie_before_content', 'hook_alexandrie_before_content');


/****************************************
 * Content Title After
 ******************************************/
function hook_alexandrie_content_title_after(){
	 echo do_shortcode(get_theme_mod( 'alexandrie_after_content_title'));
}
add_action('alexandrie_after_content_title', 'hook_alexandrie_content_title_after');


/****************************************
 * After Content
 ******************************************/
function hook_alexandrie_after_content(){
	 echo do_shortcode(get_theme_mod( 'alexandrie_after_content'));
}
add_action('alexandrie_after_content', 'hook_alexandrie_after_content');


/****************************************
 * Content Container After
 ******************************************/
function hook_alexandrie_inside_content_container_after(){
	 echo do_shortcode(get_theme_mod( 'alexandrie_inside_content_container_after'));
}
add_action('alexandrie_inside_content_container_after', 'hook_alexandrie_inside_content_container_after');


/****************************************
 * Before Sidebar
 ******************************************/
function hook_alexandrie_before_sidebar(){
	 echo do_shortcode(get_theme_mod( 'alexandrie_before_sidebar'));
}
add_action('alexandrie_before_sidebar', 'hook_alexandrie_before_sidebar');


/****************************************
 * After Sidebar
 ******************************************/
function hook_alexandrie_after_sidebar(){
	 echo do_shortcode(get_theme_mod( 'alexandrie_after_sidebar'));
}
add_action('alexandrie_after_sidebar', 'hook_alexandrie_after_sidebar');


/****************************************
 * Before Footer
 ******************************************/
function hook_alexandrie_before_footer(){
	 echo do_shortcode(get_theme_mod( 'alexandrie_before_footer'));
}
add_action('alexandrie_before_footer', 'hook_alexandrie_before_footer');


/****************************************
 * After Footer Widgets
 ******************************************/
function hook_alexandrie_after_footer_widgets(){
	 echo do_shortcode(get_theme_mod( 'alexandrie_after_footer_widgets'));
}
add_action('alexandrie_after_footer_widgets', 'hook_alexandrie_after_footer_widgets');


/****************************************
 * After Footer
 ******************************************/
function hook_alexandrie_after_footer(){
	 echo do_shortcode(get_theme_mod( 'alexandrie_after_footer'));
}
add_action('alexandrie_after_footer', 'hook_alexandrie_after_footer');




