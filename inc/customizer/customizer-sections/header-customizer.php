<?php

/* * **************************************************
 * Header Fields
 * ************************************************** */

// Header Tabs
alexandrie_tabs( 'header_setting', 'header' );

alexandrie_shortcuts( 'header_shortcut', 'header', 'partial_header', '.site-header .container' );

Kirki::add_field( 'alexandrie', array(
    'type' => 'select',
    'settings' => 'global_header_content_layout',
    'label' => __( 'Header Layout', 'alexandrie' ),
    'section' => 'header',
    'transport' => 'auto',
    'default' => 'bg-stretched',
    'choices' => array(
        'container' => esc_attr__( 'Boxed', 'alexandrie' ),
//        'bg-stretched'   => esc_attr__( 'BG Stretched', 'alexandrie' ),
        'container-fluid' => esc_attr__( 'Full Width', 'alexandrie' ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'header_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );

Kirki::add_field( 'alexandrie', array(
    'type' => 'select',
    'settings' => 'header_layout',
    'label' => __( 'Header Style', 'alexandrie' ),
    'section' => 'header',
    'transport' => 'auto',
    'default' => 'header-left',
    'choices' => array(
        'header-left-widget' => esc_attr__( 'Logo Left - widget Area Right', 'alexandrie' ),
        'header-left' => esc_attr__( 'Logo Left - Menu Right', 'alexandrie' ),
        'header-center' => esc_attr__( 'All Center', 'alexandrie' ),
        'header-right-widget' => esc_attr__( 'Logo Right - widget Area Left', 'alexandrie' ),
        'header-right' => esc_attr__( 'Logo Right - Menu Left', 'alexandrie' ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'header_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );


// Transparent Header
Kirki::add_field( 'alexandrie', array(
    'type' => 'toggle',
    'settings' => 'transparent_header',
    'label' => esc_attr__( 'Overlap Header', 'alexandrie' ),
    'description' => 'Hint: Enable this to make a transparent header effect. Adjust the header opacity via header background option under design tab.',
    'section' => 'header',
    'default' => '0',
    'active_callback' => array(
        array(
            'setting' => 'header_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
    'output' => array(
        array(
            'element' => '.site-header',
            'property' => 'position',
            'value_pattern' => 'absolute',
            // 'suffix'          => ' !important',
            'exclude' => array( false ),
        ),
        array(
            'element' => '.site-header',
            'property' => 'width',
            'value_pattern' => '100%',
            'exclude' => array( false ),
        ),
        array(
            'element' => '.site-header',
            'property' => 'left',
            'value_pattern' => '0 ',
            'exclude' => array( false ),
        ),
        array(
            'element' => '.site-header',
            'property' => 'right',
            'value_pattern' => '0 ',
            'exclude' => array( false ),
        ),
        array(
            'element' => '.site-header',
            'property' => 'z-index',
            'value_pattern' => '100',
            'exclude' => array( false ),
        ),
    ),
) );

// Sticky Header
Kirki::add_field( 'alexandrie', array(
    'type' => 'toggle',
    'settings' => 'sticky_header',
    'label' => esc_attr__( 'Make Header Sticky', 'alexandrie' ),
    'section' => 'header',
    'default' => '0',
    'active_callback' => array(
        array(
            'setting' => 'header_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );

// Select Stickey Header
Kirki::add_field( 'alexandrie', array(
    'type' => 'select',
    'settings' => 'select_sticky_header',
    'label' => __( 'Sticky Header Styles', 'alexandrie' ),
    'section' => 'header',
    'transport' => 'auto',
    'default' => 'visible_scroll_up',
    'choices' => array(
        'visible_scroll_up' => esc_attr__( 'Visible on Scroll Up', 'alexandrie' ),
        'visible_scroll_down' => esc_attr__( 'Visible on Scroll Down', 'alexandrie' ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'header_setting',
            'operator' => '==',
            'value' => 'general',
        ),
        array(
            'setting' => 'sticky_header',
            'operator' => '==',
            'value' => '1',
        ),
    ),
) );

// Sticky Header
Kirki::add_field( 'alexandrie', array(
    'type' => 'toggle',
    'settings' => 'show_top_header',
    'label' => esc_attr__( 'Show Top header', 'alexandrie' ),
    'section' => 'header',
    'default' => '1',
    'active_callback' => array(
        array(
            'setting' => 'header_setting',
            'operator' => '==',
            'value' => 'general',
        ),
    ),
) );

// Site Title Styles
alexandrie_headlines( 'site_title_styles', 'header', 'header_setting', 'style', 'Site Title', '10px' );

// Site Title Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'site_title_color',
    'label' => __( 'Site Title', 'alexandrie' ),
    'section' => 'header',
    'transport' => 'auto',
    'default' => '#000000',
    'output' => array(
        array(
            'element' => '.site-title a',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.site-title a',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'header_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Site Title Hover Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'site_title_hover_color',
    'label' => __( 'Site Title Hover', 'alexandrie' ),
    'section' => 'header',
    'transport' => 'auto',
    'default' => '#000000',
    'output' => array(
        array(
            'element' => '.site-title a:hover',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.site-title a:hover',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'header_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Site Title Typography
Kirki::add_field( 'alexandrie', array(
    'type' => 'typography',
    'settings' => 'header_site_title',
    'section' => 'header',
    'transport' => 'auto',
    'default' => array(
        'font-family' => '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif',
        'variant' => '700',
        'font-size' => '45px',
        'line-height' => '1.5',
        'letter-spacing' => '0',
        'text-transform' => 'none',
    ),
    'output' => array(
        array(
            'element' => '.site-title a',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.site-title a',
            'function' => 'css',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'header_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Site Description Styles
alexandrie_headlines( 'site_desc_styles', 'header', 'header_setting', 'style', 'Site Description', '35px' );

// Site Description Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'site_description_color',
    'label' => __( 'Description', 'alexandrie' ),
    'section' => 'header',
    'transport' => 'auto',
    'default' => '#000000',
    'output' => array(
        array(
            'element' => '.site-description',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.site-description',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'header_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Site Description Typography
Kirki::add_field( 'alexandrie', array(
    'type' => 'typography',
    'settings' => 'header_site_description',
    'section' => 'header',
    'transport' => 'auto',
    'default' => array(
        'font-family' => '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif',
        'font-size' => '14px',
        'line-height' => '1.5',
        'letter-spacing' => '0',
        'text-transform' => 'none',
    ),
    'output' => array(
        array(
            'element' => '.site-description',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.site-description',
            'function' => 'css',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'header_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Header Backgrounds
alexandrie_headlines( 'header_backgrounds', 'header', 'header_setting', 'style', 'Backgrounds', '35px' );


//Kirki::add_field( 'alexandrie', array(
//    'type' => 'color',
//    'settings' => 'header_bg',
//    'label' => __( 'Header', 'alexandrie' ),
//    'section' => 'header',
//    'transport' => 'auto',
//    'default' => '#fff',
//    'choices' => array(
//        'alpha' => true,
//    ),
//    'output' => array(
//        array(
//            'element' => '.site-header',
//            'property' => 'background-color',
//        ),
//    ),
//    'js_vars' => array(
//        array(
//            'element' => '.site-header',
//            'function' => 'css',
//            'property' => 'background-color',
//        ),
//    ),
//    'active_callback' => array(
//        array(
//            'setting' => 'header_setting',
//            'operator' => '==',
//            'value' => 'style',
//        ),
//    ),
//) );
Kirki::add_field( 'alexandrie', array(
	'type'        => 'background',
	'settings'    => 'header_bg_',
	'label'       => esc_html__( 'Background Control', 'alexandrie' ),
//	'description' => esc_html__( 'Background conrols are pretty complex - but extremely useful if properly used.', 'alexandrie' ),
	'section'     => 'header',
	'default'     => [
		'background-color'      => '#fff',
		'background-image'      => '',
		'background-repeat'     => 'repeat',
		'background-position'   => 'center center',
		'background-size'       => 'cover',
		'background-attachment' => 'scroll',
	],
	'transport'   => 'auto',
	'output'      => [
		[
			'element' => '.site-header',
		],
	],
    'active_callback' => array(
        array(
            'setting' => 'header_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'type' => 'custom',
    'settings' => 'top_header_section',
    'section' => 'header',
    'default' => '<div style="border-bottom: 1px solid #000; line-height: 2em; font-size: 16px; background: #000; color: #fff; padding: 0 10px; cursor: auto; margin-top: 20px">' . esc_html__( 'Top Header ', 'alexandrie' ) . '</div>',
    'active_callback' => array(
        array(
            'setting' => 'header_setting',
            'operator' => '==',
            'value' => 'style',
        ),
        array(
            'setting' => 'show_top_header',
            'operator' => '==',
            'value' => '1',
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'header_top_bg',
    'label' => __( 'Top Header Background', 'alexandrie' ),
    'section' => 'header',
    'transport' => 'auto',
    'default' => '#f8f9fa',
    'choices' => array(
        'alpha' => true,
    ),
    'output' => array(
        array(
            'element' => '.top-header',
            'property' => 'background-color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.top-header',
            'function' => 'css',
            'property' => 'background-color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'header_setting',
            'operator' => '==',
            'value' => 'style',
        ),
        array(
            'setting' => 'show_top_header',
            'operator' => '==',
            'value' => '1',
        ),
    ),
) );

Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'header_top_link_color',
    'label' => __( 'Top Header link color', 'alexandrie' ),
    'section' => 'header',
    'transport' => 'auto',
    'default' => '#000',
    'choices' => array(
        'alpha' => true,
    ),
    'output' => array(
        array(
            'element' => '.top-header a',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.top-header a',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'header_setting',
            'operator' => '==',
            'value' => 'style',
        ),
        array(
            'setting' => 'show_top_header',
            'operator' => '==',
            'value' => '1',
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'header_top_link_color_hover',
    'label' => __( 'Top Header link hover color', 'alexandrie' ),
    'section' => 'header',
    'transport' => 'auto',
    'default' => '#ddd',
    'choices' => array(
        'alpha' => true,
    ),
    'output' => array(
        array(
            'element' => '.top-header a:hover',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.top-header a',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'header_setting',
            'operator' => '==',
            'value' => 'style',
        ),
        array(
            'setting' => 'show_top_header',
            'operator' => '==',
            'value' => '1',
        ),
    ),
) );

//header spacing
Kirki::add_field( 'alexandrie', array(
    'settings' => 'header_spacing',
    'section' => 'header',
    'label' => esc_html__( 'Header Padding', 'alexandrie' ),
    'type' => 'spacing',
    'default' => array(
        'top' => '5rem',
        'right' => '15px',
        'bottom' => '5rem',
        'left' => '15px',
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.header-content',
            'property' => 'padding',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.header-content',
             'function'=>'css',
            'property' => 'padding',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'header_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'type' => 'select',
    'settings' => 'site_header_shadow',
    'label' => __( 'Header Shadow', 'alexandrie' ),
    'section' => 'header',
    'default' => 'shadow-sm',
    'priority' => 10,
    'multiple' => 1,
    'choices' => array(
        'shadow-none' => esc_attr__( 'None', 'alexandrie' ),
        'shadow-sm' => esc_attr__( 'Small Shadow', 'alexandrie' ),
        'shadow' => esc_attr__( 'Shadow', 'alexandrie' ),
        'shadow-lg' => esc_attr__( 'large Shadow', 'alexandrie' ),
        'shadow-custom' => esc_attr__( '3D shadow', 'alexandrie' ),
    ),
    'output' => array(
        array(
            'element' => '.site-header',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'header_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
    ),
) );


Kirki::add_field( 'alexandrie', array(
    'type' => 'radio-buttonset',
    'settings' => 'header_content_border_style',
    'label' => __( 'Header Content Border Style', 'alexandrie' ),
    'section' => 'header',
    'transport' => 'auto',
    'default' => 'solid',
    'priority' => 10,
    'multiple' => 1,
    'choices' => array(
        'none' => esc_attr__( 'None', 'alexandrie' ),
        'solid' => esc_attr__( 'solid', 'alexandrie' ),
        'dotted' => esc_attr__( 'dotted', 'alexandrie' ),
        'dashed' => esc_attr__( 'dashed', 'alexandrie' ),
    ),
    'output' => array(
        array(
            'element' => '.header.site-header',
            'property' => 'border-style',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.header.site-header',
            'function' => 'css',
            'property' => 'border-style',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'header_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
) ) );

Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'header_content_border_color',
    'label' => esc_attr__( 'Header content Border color', 'alexandrie' ),
    'section' => 'header',
    'priority' => 10,
    'default' => '#f8f9fa',
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.header.site-header',
            'property' => 'border-color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.header.site-header',
            'function'=>'css',
            'property' => 'border-color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'header_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
        array(
            'setting' => 'header_content_border_style',
            'value' => 'none',
            'operator' => '!='
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'settings' => 'header_content_border_thickness',
    'section' => 'header',
    'label' => esc_html__( 'Header Content Border Thickness', 'alexandrie' ),
    'type' => 'dimensions',
    'default' => array(
        'top-width' => '0',
        'right-width' => '0',
        'bottom-width' => '1px',
        'left-width' => '0',
    ),
    'choices' => array(
        'top-width' => esc_attr__( 'Top', 'alexandrie' ),
        'right-width' => esc_attr__( 'Right', 'alexandrie' ),
        'bottom-width' => esc_attr__( 'Bottom', 'alexandrie' ),
        'left-width' => esc_attr__( 'Left', 'alexandrie' ),
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.header.site-header',
            'property' => 'border',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.header.site-header',
            'function' => 'css',
            'property' => 'border',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'header_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
        array(
            'setting' => 'header_content_border_style',
            'value' => 'none',
            'operator' => '!='
        ),
    ),
) );



// Sidebar title Styles
alexandrie_headlines( 'header_title_styles', 'header', 'header_setting', 'style', 'Widget Title', '10px' );

// Sidebar Title Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'header_title_color',
    'label' => __( 'widget Title', 'alexandrie' ),
    'section' => 'header',
    'transport' => 'auto',
    'default' => '#000000',
    'output' => array(
        array(
            'element' => '.header-widget-area .widget-title',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.header-widget-area .widget-title',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'header_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Sidebar Widgets Titles
Kirki::add_field( 'alexandrie', array(
    'type' => 'typography',
    'settings' => 'header_widgets_title_typography',
    'section' => 'header',
    'transport' => 'auto',
    'default' => array(
        'font-family' => '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif',
        'variant' => 'regular',
        'font-size' => '30px',
        'line-height' => '1.5',
        'letter-spacing' => '0',
        'text-transform' => 'none',
        'text-align' => 'left',
    ),
    'output' => array(
        array(
            'element' => '.header-widget-area .widget-title',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.header-widget-area .widget-title',
            'function' => 'css',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'header_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );



// Sidebar Text Styles
alexandrie_headlines( 'header_text_styles', 'header', 'header_setting', 'style', 'Widget Text', '35px' );

// Sidebar Text Color
Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'header_text_color',
    'label' => __( 'Text', 'alexandrie' ),
    'section' => 'header',
    'transport' => 'auto',
    'default' => '#3a3a3a',
    'output' => array(
        array(
            'element' => '.header-widget-area .widget',
            'property' => 'color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.header-widget-area .widget',
            'function' => 'css',
            'property' => 'color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'header_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );

// Sidebar Widgets Text 
Kirki::add_field( 'alexandrie', array(
    'type' => 'typography',
    'settings' => 'header_widgets_text_typography',
    'section' => 'header',
    'transport' => 'auto',
    'default' => array(
        'font-family' => '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif',
        'variant' => 'regular',
        'font-size' => '17px',
        'line-height' => '1.5',
        'letter-spacing' => '0',
        'text-transform' => 'none',
        'text-align' => 'left',
    ),
    'output' => array(
        array(
            'element' => '.header-widget-area .widget',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.header-widget-area .widget',
            'function'=>'css',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'header_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );


// Sidebar Bakcground Styles
alexandrie_headlines( 'header_widgets_backgrounds', 'header', 'header_setting', 'style', 'Widget Backgrounds', '35px' );


Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'header_wid_bg',
    'label' => __( 'Backround', 'alexandrie' ),
    'section' => 'header',
    'transport' => 'auto',
    'default' => 'transparent',
    'choices' => array(
        'alpha' => true,
    ),
    'output' => array(
        array(
            'element' => '.header-widget-area .widget',
            'property' => 'background-color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.header-widget-area .widget',
            'function' => 'css',
            'property' => 'background-color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'header_setting',
            'operator' => '==',
            'value' => 'style',
        ),
    ),
) );


alexandrie_headlines( 'header_title_styles_adv', 'header', 'header_setting', 'advanced', 'Header Widget Advanced Styling', '10px' );
/* Advanced Field */
Kirki::add_field( 'alexandrie', array(
    'type' => 'select',
    'settings' => 'header_widget_shadow',
    'label' => __( 'widget Shadow', 'alexandrie' ),
    'section' => 'header',
    'default' => 'none',
    'priority' => 10,
    'multiple' => 1,
    'choices' => array(
        'shadow-none' => esc_attr__( 'None', 'alexandrie' ),
        'shadow-sm' => esc_attr__( 'Small Shadow', 'alexandrie' ),
        'shadow' => esc_attr__( 'Shadow', 'alexandrie' ),
        'shadow-lg' => esc_attr__( 'large Shadow', 'alexandrie' ),
        'shadow-custom' => esc_attr__( '3D shadow', 'alexandrie' ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'header_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'settings' => 'header_border_radius',
    'section' => 'header',
    'label' => esc_html__( 'widget Border radius', 'alexandrie' ),
    'type' => 'dimensions',
    'default' => array(
        'top-left-radius' => '1rem',
        'top-right-radius' => '1rem',
        'bottom-left-radius' => '1rem',
        'bottom-right-radius' => '1rem',
    ),
    'choices' => array(
        'top-left-radius' => esc_attr__( 'Top-Left radius', 'alexandrie' ),
        'top-right-radius' => esc_attr__( 'Top-Right-radius:', 'alexandrie' ),
        'bottom-left-radius' => esc_attr__( 'Bottom-Left radius', 'alexandrie' ),
        'bottom-right-radius' => esc_attr__( 'Bottom-Right-radius', 'alexandrie' ),
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.header-widget-area .widget',
            'property' => 'border',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.header-widget-area .widget',
            'function' => 'css',
            'property' => 'border',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'header_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'type' => 'radio-buttonset',
    'settings' => 'header_border_style',
    'label' => __( 'widget Border Style', 'alexandrie' ),
    'section' => 'header',
    'default' => 'solid',
    'priority' => 10,
    'multiple' => 1,
    'transport' => 'auto',
    'choices' => array(
        'none' => esc_attr__( 'None', 'alexandrie' ),
        'solid' => esc_attr__( 'solid', 'alexandrie' ),
        'dotted' => esc_attr__( 'dotted', 'alexandrie' ),
        'dashed' => esc_attr__( 'dashed', 'alexandrie' ),
    ),
    'output' => array(
        array(
            'element' => '.header-widget-area .widget',
            'property' => 'border-style',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.header-widget-area .widget',
            'function'=>'css',
            'property' => 'border-style',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'header_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
) ) );

Kirki::add_field( 'alexandrie', array(
    'type' => 'color',
    'settings' => 'header_widget_border_color',
    'label' => esc_attr__( 'widget Border color', 'alexandrie' ),
    'section' => 'header',
    'priority' => 10,
    'default' => '#f8f9fa',
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.header-widget-area .widget',
            'property' => 'border-color',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.header-widget-area .widget',
             'function'=>'css',
            'property' => 'border-color',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'header_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
        array(
            'setting' => 'header_border_style',
            'value' => 'none',
            'operator' => '!='
        ),
    ),
) );
Kirki::add_field( 'alexandrie', array(
    'settings' => 'header_border',
    'section' => 'header',
    'label' => esc_html__( 'widget Border Thickness', 'alexandrie' ),
    'type' => 'dimensions',
    'default' => array(
        'top-width' => '1px',
        'right-width' => '1px',
        'bottom-width' => '1px',
        'left-width' => '1px',
    ),
    'choices' => array(
        'top-width' => esc_attr__( 'Top', 'alexandrie' ),
        'right-width' => esc_attr__( 'Right', 'alexandrie' ),
        'bottom-width' => esc_attr__( 'Bottom', 'alexandrie' ),
        'left-width' => esc_attr__( 'Left', 'alexandrie' ),
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.header-widget-area .widget',
            'property' => 'border',
        ),
    ),
    'js_vars' => array(
        array(
            'element' => '.header-widget-area .widget',
            'function' => 'css',
            'property' => 'border',
        ),
    ),
    'active_callback' => array(
        array(
            'setting' => 'header_setting',
            'operator' => '==',
            'value' => 'advanced',
        ),
        array(
            'setting' => 'header_border_style',
            'value' => 'none',
            'operator' => '!='
        ),
    ),
) );
