//load plugins
var gulp = require('gulp'),
    //        compass = require('gulp-compass'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    cleancss = require('gulp-clean-css'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    livereload = require('gulp-livereload'),
    plumber = require('gulp-plumber'),
    path = require('path'),
    cmq = require('gulp-group-css-media-queries'),
    bundle = require('gulp-bundle-assets'),
    imagemin = require('gulp-imagemin'),
    imageminPngcrush = require('imagemin-pngcrush'),
    deleteUnusedImages = require('gulp-delete-unused-images'),
    htmlmin = require('gulp-htmlmin'),
    changed = require('gulp-changed'),
    cache = require('gulp-cache'),
    njk = require('gulp-nunjucks-render'),
    sourcemaps = require('gulp-sourcemaps'),
    gulpif = require('gulp-if'),
    browserSync = require('browser-sync').create(),
    cssmin = require('gulp-cssmin'),
    babel = require("gulp-babel");

var env,
    jsSources,
    sassSources,
    htmlSources,
    jsonSources,
    imgSources,
    fontSources,
    outputDir,
    sassStyle;

env = process.env.NODE_ENV || 'development';
// env='production';

outputDir = "assets";
//htmlSources = "dev/html/**/*";
sassSources = ["assets/src/scss/**/**/*.scss","assets/src/scss/*.scss"];
jsSources = ['assets/src/js/**/*'];
//imgSources = "img/**/*";
fontSources = 'assets/fonts/**/*';
videoSources = 'video/*';

if (env === 'development') {
    outputDir = 'assets/';
    sassStyle = 'expanded';
} else {
    outputDir = 'assets/';
    sassStyle = 'compressed';
}


//the title and icon that will be used for the Grunt notifications
var notifyInfo = {
    title: 'Gulp',
    icon: path.join(__dirname, 'gulp.png')
};

gulp.task('clear', function (done) {
    return cache.clearAll(done);
});

//error notification settings for plumber
var plumberErrorHandler = {
    errorHandler: notify.onError({
        title: notifyInfo.title,
        icon: notifyInfo.icon,
        message: "Error: <%= error.message %>"
    })
};
gulp.task('default', ['styles', 'html', 'js']);


var importoptions = {
    filter: /^http:\/\//gi // process only http urls 
};

gulp.task('browser-sync', function () {
    browserSync.init({
        server: {
            baseDir: ["./", 'start'],
            index: "index.php",
            startUrl: "/"
        }

    });
});


//gulp.task('serve', ['browser-sync'], function () {
//    gulp.watch("assets/src/scss/**/*.scss", {interval: 500}, ['styles']);
//    gulp.watch("assets/src/js/**/*.js", {interval: 500}, ['js']);
//    gulp.watch("html/html-src/**/*.*", {interval: 500}, ['html'], function () {
//        browserSync.reload;
//    });
////    gulp.watch(["html/*.html","!html/html-src/**"]).on('change', browserSync.reload);
//});

gulp.task('watch', function () {
    gulp.watch(jsSources, {
        interval: 1000, // default 100
        debounceDelay: 1000, // default 500
        mode: 'poll'
    }, ['js']);
    gulp.watch(sassSources, {
        interval: 1000, // default 100
        debounceDelay: 1000, // default 500
        mode: 'poll'
    }, ['styles']);
//    gulp.watch(htmlSources, {
//        interval: 1000, // default 100
//        debounceDelay: 1000, // default 500
//        mode: 'poll'
//    }, ['html'], function (done) {
//        browserSync.reload();
//        done();
//    });
    //    gulp.watch(imgSources, ['images']);
    //    gulp.watch(fontSources, ['fonts']);
    //    gulp.watch(videoSources, ['video']);
});

//styles
gulp.task('styles', function () {
    var options = {
        extensions: ["css"] // process only css 
    };
    return gulp.src(sassSources)
        .pipe(plumber(plumberErrorHandler))
        .pipe(sass())
        //            .pipe(compass({
        //                config_file: 'config.rb',
        //                css: 'src/css',
        //                sass: 'src/scss',
        //                image: 'img',
        //                logging: true
        //            }))
        .pipe(autoprefixer({
            browsers: ['last 4 version', 'safari 5', 'ie 7', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'],
            cascade: false
        }))
        .pipe(cmq({
            //                log: true
        }))
        .pipe(gulp.dest('assets/src/css'))
        .pipe(gulpif(env === 'production', cssmin({
            processImport: false
        })))
        .pipe(gulp.dest(outputDir + '/css'))
        .pipe(browserSync.reload({
            stream: true
        }));
});

//gulp.task('images', function () {
//    gulp.src(imgSources)
//            .pipe(plumber(plumberErrorHandler))
//            .pipe(imagemin({verbose: true,
//                progressive: true,
//                interlaced: true,
//                arithmetic: true,
//                plugins: [imagemin.gifsicle(), imagemin.jpegtran(), imagemin.optipng(), imagemin.svgo()]}
//            ))
//            .pipe(gulp.dest(outputDir + 'assets/img'))
//            .pipe(browserSync.reload({stream: true}));
//});

//gulp.task('html', function () {
//    var locals = {};
//    return gulp.src(htmlSources)
//        .pipe(plumber())
//        .pipe(njk({
//            path: ['dev/html']
//        }))
//        .pipe(gulpif(env === 'production', htmlmin({
//            collapseWhitespace: true
//        })))
//        .pipe(gulp.dest(outputDir))
//        .pipe(browserSync.reload({
//            stream: true
//        }));
//
//
//});



//gulp.task('fonts', function () {
//    return gulp.src(fontSources)
//            .pipe(gulp.dest(outputDir + 'assets/fonts'));
//});
//gulp.task('video', function () {
//    return gulp.src(videoSources)
//            .pipe(gulp.dest(outputDir + 'assets/video'));
//});


//javascript
gulp.task('js', function () {

    return gulp.src('bundle.config.js')
        .pipe(sourcemaps.init())
        .pipe(plumber(plumberErrorHandler))
        .pipe(babel())
        .pipe(bundle())
        .pipe(gulpif(env === 'production', uglify().on('error', function (e) {
            console.log(e);
        })))
        .pipe(gulp.dest(outputDir + 'js'))
        .pipe(browserSync.reload({
            stream: true
        }));

});