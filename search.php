<?php
/**
 * The template for displaying search results pages.
 *
 * @package alexandrie
 */
get_header();
?>

<section id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <?php do_action( 'alexandrie_inside_content_container_before' ); ?><!-- Content Container Before Hook -->
        <?php if ( have_posts() ) : ?>

            <header class="search-page-header">
                <h2 class="page-title search_title"><?php printf( esc_html__( get_theme_mod( 'search_headline', 'Search Results for' ) . ': %s', 'alexandrie' ), '<span>' . get_search_query() . '</span>' ); ?></h2>
            </header><!-- .page-header -->

            <?php
            $shadow_class = get_theme_mod( 'search_archive_post_shadow', 'shadow-none' );
            while ( have_posts() ) : the_post();
                ?>

                <article id="post-<?php the_ID(); ?>" <?php post_class( [ 'search_blog', $shadow_class ] ); ?>>	
                    <?php do_action( 'alexandrie_before_content' ); ?>
                    <?php
                    $search_template_parts = get_theme_mod( 'search_posts_layout', array( 'thumbnail', 'title', 'meta', 'content' ) );

                    if ( !empty( $search_template_parts ) && is_array( $search_template_parts ) ) {
                        foreach ( $search_template_parts as $search_part ) {
                            get_template_part( 'template-parts/search-archive/search-' . $search_part );
                        }
                    }
                    ?>
        <?php do_action( 'alexandrie_after_content' ); ?>					
                </article>

            <?php endwhile; ?>

            <?php alexandrie_the_posts_navigation(); ?>

        <?php else : ?>

            <?php get_template_part( 'template-parts/content', 'none' ); ?>

        <?php endif; ?>
<?php do_action( 'alexandrie_inside_content_container_after' ); ?><!-- Content Container After Hook -->
    </main><!-- #main -->
</section><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>