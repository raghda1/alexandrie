<form id="searchform" class="form-inline-custom" method="get" action="<?php echo home_url( '/' ); ?>">
    <input type="text" class="search-field form-control form-control-left-oval form-control-sm" name="s" placeholder="Search" value="<?php the_search_query(); ?>">
    <!--<input type="submit" value="" class="btn btn-right-oval">-->
    <button type="submit" class="btn btn-light btn-sm  btn-right-oval">
        <i class='ti-search'></i>
    </button>
</form>