wp.blocks.registerBlockStyle('core/image', [
    {
        name: "default",
        label: "no style image",
        isDefault: true
    },
    {
        name: "shadow",
        label: "put shadow"
    },
    {
        name: "shadow-sm",
        label: "put small shadow"
    },
    {
        name: "shadow-lg",
        label: "put large shadow"
    },
    {
        name: "shadow-custom",
        label: "put 3D shadow"
    }, 
    {
        name: "rounded",
        label: "Rounded corner image",
        isDefault: true
    },
    {
        name: "rounded-shadow",
        label: "rounded w shadow"
    },
    {
        name: "rounded-shadow-sm",
        label: "rounded w small shadow"
    },
    {
        name: "rounded-shadow-lg",
        label: "rounded w large shadow"
    },
    {
        name: "rounded-shadow-custom",
        label: "rounded w 3D shadow"
    }
    
]);
wp.blocks.registerBlockStyle('core/cover', [
    {
        name: "default",
        label: "no style image",
        isDefault: true
    },
    {
        name: "shadow",
        label: "put shadow"
    },
    {
        name: "shadow-sm",
        label: "put small shadow"
    },
    {
        name: "shadow-lg",
        label: "put large shadow"
    },
    {
        name: "shadow-custom",
        label: "put 3D shadow"
    },
    {
        name: "shadow-hover",
        label: "put shadow on hover"
    },
    {
        name: "rounded",
        label: "Rounded corner image",
        isDefault: true
    },
    {
        name: "rounded-shadow",
        label: "rounde w shadow"
    },
    {
        name: "rounded-shadow-sm",
        label: "rounde w small shadow"
    },
    {
        name: "rounded-shadow-lg",
        label: "rounde w large shadow"
    },
    {
        name: "rounded-shadow-custom",
        label: "rounde w 3D shadow"
    },
    {
        name: "rounded-shadow-hover",
        label: "rounde w shadow on hover"
    }
    
]);
wp.blocks.registerBlockStyle('core/video', [
    {
        name: "default",
        label: "no style image",
        isDefault: true
    },
    {
        name: "shadow",
        label: "put shadow"
    },
    {
        name: "shadow-sm",
        label: "put small shadow"
    },
    {
        name: "shadow-lg",
        label: "put large shadow"
    },
    {
        name: "shadow-custom",
        label: "put 3D shadow"
    },
    {
        name: "rounded",
        label: "Rounded corner image",
        isDefault: true
    },
    {
        name: "rounded-shadow",
        label: "rounde w shadow"
    },
    {
        name: "rounded-shadow-sm",
        label: "rounde w small shadow"
    },
    {
        name: "rounded-shadow-lg",
        label: "rounde w large shadow"
    },
    {
        name: "rounded-shadow-custom",
        label: "rounde w 3D shadow"
    }
    
]);

wp.blocks.registerBlockStyle('core/gallery', [
    {
        name: "default",
        label: "Default (this works with all options in gallery settings)",
    },
    {
        name: "default-shadow-img",
        label: "Default with shadow",
    },
    {
        name: "default-rounded-img",
        label: "Default with rounded images",
    },
    {
        name: "default-rounded-shadow-img",
        label: "Default with rounded and shadow images",
    },
    {
        name: "lightbox",
        label: "show images in lightbox(only works with 'link to media file' in gallery settings)",
    },
    {
        name: "lightbox-shadow-img",
        label: "show images in lightbox",
    },
    {
        name: "lightbox-rounded-img",
        label: "show rounded courners images and opens in lightbox",
    },
    {
        name: "lightbox-rounded-shadow-img",
        label: "show rounded shadow images and opens in lightbox",
    }

]);
wp.blocks.registerBlockStyle('core/table', [
    {
        name: "vertical-stripes",
        label: "column stripes",
    },
//    {
//        name: "owl-carousel",
//        label:"show images in carousel",
//    }

]);

