"use strict";
jQuery(document).ready(function ($) {
    function makeFullWidth()
    {
        if ($(".no-sidebar")[0]) {
            var fullWidth = $("body").prop("clientWidth");
//        console.log(fullWidth);
            var containerWidth = $(".site-content").width();
//        console.log(containerWidth);
            var negMargin = (-fullWidth + containerWidth) / 2;
//        console.log(negMargin);
            $('.alignfull').css({"width": fullWidth, "margin-left": negMargin, "margin-right": negMargin});

        } else {
            $('.alignfull').css({"width": '100%', "margin-left": '0', "margin-right": '0'});
            $('.alignwide').css({"width": '100%', "margin-left": '0', "margin-right": '0'});
        }

    }
    makeFullWidth();
    $(window).resize(makeFullWidth);

    $('body').append('<div id="toTop" class="btn btn-2-top"><span class="ti-angle-up"></span></div>');
    $(window).scroll(function () {
        if ($(this).scrollTop() != 0) {
            $('#toTop').fadeIn();
        } else {
            $('#toTop').fadeOut();
        }
    });
    $('#toTop').click(function () {
        $("html, body").animate({scrollTop: 0}, 600);
        return false;
    });
   $('.grid').imagesLoaded(function () {
        $('.grid').masonry({
            itemSelector: '.grid-item', // use a separate class for itemSelector, other than .col-
            columnWidth: '.grid-sizer',
            percentPosition: true
        });
    });
//    is-style-lightbox
    //maginfic pop up code
    $('.is-style-lightbox').magnificPopup({
        delegate: 'a', // child items selector, by clicking on it popup will open
        type: 'image',
        gallery: {
            enabled: true
        },
        // Delay in milliseconds before popup is removed
        removalDelay: 300,
        // Class that is added to popup wrapper and background
        // make it unique to apply your CSS animations just to this exact popup
        mainClass: 'mfp-fade',
        image: {
            titleSrc: function (item) {
//                console.log(item);
                if (item.el.next('figcaption').length !== 0) {
                    return item.el.next('figcaption').html();
                }
                return "";
            }

        },
        retina: {
            ratio: 1, // Increase this number to enable retina image support.
            // Image in popup will be scaled down by this number.
            // Option can also be a function which should return a number (in case you support multiple ratios). For example:
            // ratio: function() { return window.devicePixelRatio === 1.5 ? 1.5 : 2  }


            replaceSrc: function (item, ratio) {
                return item.src.replace(/\.\w+$/, function (m) {
                    return '@2x' + m;
                });
            } // function that changes image source
        }
    });
});

