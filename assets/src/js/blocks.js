wp.blocks.registerBlockStyle('core/image', [
    {
        name: "default",
        label: "no style image",
        isDefault: true
    },
    {
        name: "shadow",
        label: "put shadow"
    },
    {
        name: "shadow-sm",
        label: "put small shadow"
    },
    {
        name: "shadow-lg",
        label: "put large shadow"
    },
    {
        name: "shadow-custom",
        label: "put 3D shadow"
    },
    {
        name: "rounded",
        label: "Rounded corner image",
        isDefault: true
    },
    {
        name: "rounded-shadow",
        label: "put shadow"
    },
    {
        name: "rounded-shadow-sm",
        label: "put small shadow"
    },
    {
        name: "rounded-shadow-lg",
        label: "put large shadow"
    },
    {
        name: "rounded-shadow-custom",
        label: "put 3D shadow"
    }
    
]);
wp.blocks.registerBlockStyle('core/video', [
    {
        name: "default",
        label: "no style image",
        isDefault: true
    },
    {
        name: "shadow",
        label: "put shadow"
    },
    {
        name: "shadow-sm",
        label: "put small shadow"
    },
    {
        name: "shadow-lg",
        label: "put large shadow"
    },
    {
        name: "shadow-custom",
        label: "put 3D shadow"
    },
    {
        name: "rounded",
        label: "Rounded corner image",
        isDefault: true
    },
    {
        name: "rounded-shadow",
        label: "put shadow"
    },
    {
        name: "rounded-shadow-sm",
        label: "put small shadow"
    },
    {
        name: "rounded-shadow-lg",
        label: "put large shadow"
    },
    {
        name: "rounded-shadow-custom",
        label: "put 3D shadow"
    }
    
]);
//wp.blocks.registerBlockStyle('core/quote', [
//    {
//        name: "solid-color",
//        label: "Rounded corner image",
//    }
//
//]);
//wp.blocks.registerBlockStyle('core/shortcode', [
//    {
//        name: "alignwide",
//        label: "Alignwide",
//    },
//    {
//        name: "alignfull",
//        label: "Alignfull",
//    }
//
//]);
wp.blocks.registerBlockStyle('core/gallery', [
    {
        name: "default",
        label: "Default (this works with all options in gallery settings)",
    },
    {
        name: "default-shadow-img",
        label: "Default with shadow",
    },
    {
        name: "default-rounded-img",
        label: "Default with rounded images",
    },
    {
        name: "default-rounded-shadow-img",
        label: "Default with rounded and shadow images",
    },
    {
        name: "lightbox",
        label: "show images in lightbox(only works with 'link to media file' in gallery settings)",
    },
    {
        name: "lightbox-shadow-img",
        label: "show images in lightbox",
    },
    {
        name: "lightbox-rounded-img",
        label: "show rounded courners images and opens in lightbox",
    },
    {
        name: "lightbox-rounded-shadow-img",
        label: "show rounded shadow images and opens in lightbox",
    }

]);
wp.blocks.registerBlockStyle('core/table', [
    {
        name: "vertical-stripes",
        label: "column stripes",
    },
//    {
//        name: "owl-carousel",
//        label:"show images in carousel",
//    }

]);
//code to get blocks names
function getBlockSettings(settings, blockName) {
    console.log(blockName);
    console.log(settings);

    return settings;
}

wp.hooks.addFilter(
        'blocks.registerBlockType',
        'post-taxonomy-filter/test-function',
        getBlockSettings
        );

